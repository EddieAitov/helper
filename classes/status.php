<?php
class HStatus extends HBase {
	const StatusActive = 1;
	const StatusFinished = 2;

	static function get_list(){
		return [HStatus::StatusActive, HStatus::StatusFinished];
	}
}