<?php
class HNodeChat extends HListnode {
	function set_list($list){
		parent::set_list($list);

		$not_viewed = [];
		foreach ($list as $message){
			// список может быть составлен и для оператора и для пользователя
			if ((!$message['operator_id'] ^ $this->attr('user_mode')) && !$message['viewed'])
				$not_viewed[] = intval($message['message_id']);
		}
		$this->attr('not_viewed', $not_viewed);
	}

	function api_data(){
		$out = parent::api_data();
		$out['data']['active'] = intval($this->attr('active'));
		$out['data']['not_viewed'] = $this->attr('not_viewed');
		return $out;
	}

	function actions(){
		if ($this->attr('operator_id') == $this->attr('current_operator_id') || $this->attr('admin')){
			// скрыть кнопки для дальнейшего показа из js (после начала чата)
			$change_operator_class = $this->attr('operator_id') ? '' : ' hidden2';
			$change_status_class = $this->attr('operator_id') ? '' : ' hidden2';

			// правая колонка
			$out = HNode::standart('div', ['class' => 'col-sm-6 right'])
				// сменить статус чата
				->add(
					HNode::standart('a', [
						'class' => 'change_status btn btn-primary'.$change_status_class,
						'data-popup' => 'change_status',
						'data-params' => 'chat_id='.$this->attr('chat_id')
					])->addText(w('change-chat-status'))
				)
				->addSpace()
				// сменить оператора
				->add(
					HNode::standart('a', [
						'class' => 'change_operator btn btn-default'.$change_operator_class,
						'data-popup' => 'change_operator'
					])->addText(w('change-operator'))
				)
				->show();
		} else $out = '';

		return $out;
	}

	// заголовок в теле страницы
	function header(){
		$name = '';

		$classes = ['glyphicon', 'va', 'status-icon'];
		$classes[] = $this->attr('active') ? 'green' : 'gray';

		// feedback / chat / mobile
		switch ($this->attr('messenger_type_id')){
			case HChat::MessengerFeedback:
				$classes[] = 'glyphicon-envelope';
				break;
			case HChat::MessengerChat:
				$classes[] = 'glyphicon-comment';
				break;
			case HChat::MessengerMobile:
				$classes[] = 'glyphicon-phone';
				break;
		}


		// заголовка, иконка чата, имя чата
		$name .= HNode::standart('div', ['class' => 'col-sm-6'])
				->add(
					HNode::standart('i', [
						'class' => implode(' ', $classes),
						'title' => w('messenger-type-'.$this->attr('messenger_type_id'))
					])
				)
				->addSpace()
				->add(
					HNode::standart('span', [
						'class' => 'chat-name'
					])->addText($this->name()))
				->show();
		$name .= $this->actions();

		return $name;
	}

	// переписывается стандартная функция
	function name(){
		$name = $this->attr('activity').' ';
		$name .= ($this->attr('user_name') ? $this->attr('user_name') : '('.w('unnamed').')');
		// дополнительная информация в названии
		$e_name = [];
		if ($this->attr('user_email')) $e_name[] = $this->attr('user_email');
		if ($this->attr('user_phone')) $e_name[] = $this->attr('user_phone');
		if (count($e_name)) $name .= ' ['.implode(', ', $e_name).']';

		return $name;
	}

	function extra_name(){
		$name = ' ('.w('operator').' ';
		if ($this->attr('operator_id')) $name .= '#'.$this->attr('operator_id');
		else $name .= w('not-set');
		$name .= ')';

		if (!$this->attr('operator_id')) $name = "<b>{$name}</b>";

		return $name;
	}

	function answer(){
		ob_start();
		$can_post = $this->attr('status_id') != HStatus::StatusFinished && (
				!$this->attr('operator_id') ||
				$this->attr('operator_id') == $this->attr('current_operator_id') ||
				$this->attr('admin')
			);

		// есть возможность отвечать в данном чате
		if ($can_post){
			// чат еще не был начат - вывод временной плашки "начать чат". убирается из js.
			if (!$this->attr('operator_id')){ ?>
				<div class="answer_overlay">
					<a class="btn btn-lg btn-primary" data-click="jhelper.chat.start"><?php echo w('start-chat')?></a>
				</div>
			<?php } ?>
			<form enctype="multipart/form-data" class="answer-form">
				<div class="col-lg-12 on-answer">
					<div class="col-sm-3">
						<select class="form-control answer-select" data-change="jhelper.fast_answer.select">
							<option value="0"><?php echo w('fast-answer')?></option>
							<?php
								foreach ($this->attr('answers') as $answer){
									echo HNode::standart('option', ['value' => $answer['answer_id']])
										->addText($answer['short_text'] ? $answer['short_text'] : $answer['text'])
										->show();
								}
							?>
						</select>
					</div>
					<div class="col-sm-4">
						<div class="tbl tbl-0">
							<div class="tbl-cell tbl-cell-minimum">
								<a class="btn btn-default fileinput-button">
									<?php echo w('upload-files')?>
									<input class="file" type="file" name="upload_file[]" multiple data-form-data='{"chat_id": <?php echo
										$this->attr('chat_id')?>}'/>
								</a>
							</div>
							<div class="tbl-cell">
								<div class="progress progress-wide">
									<div class="progress-bar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 tbl">
					<div class="tbl-cell">
						<textarea class="form-control" rows="2" name="text" data-ctrlenter="jhelper.chat.send"
								  placeholder="Ctrl+Enter"></textarea>
					</div>
					<div class="tbl-cell send-wrapper">
						<a class="btn btn-lg btn-success" data-click="jhelper.chat.send"><?php echo w('send')?></a>
					</div>
				</div>
				<ul class="answer-files"></ul>
			</form>
			<?php
		// нет возможности отвечать
		} else {
			if ($this->attr('status_id') == HStatus::StatusFinished){ // чат завершен ?>
				<div class="answer_overlay">
					<h3><span class="label label-default"><?php echo w('chat-is-finished')?></span></h3>
				</div>
			<?php } elseif ($this->attr('operator_id') != $this->attr('current_operator_id')){ // чат сфорваржен ?>
			<div class="answer_overlay">
				<h3><span class="label label-default"><?php echo w('chat-was-forwarded')?></span></h3>
			</div>
		<?php }
		}

		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	// показать целиком блок чата
	function show(){
		ob_start(); ?>
		<div class="row"
			<?php echo $this->show_object_attr('chat')?>
			data-name="<?php echo str_replace('"', '&quot;', $this->name())?>"
			>
			<div class="col-lg-12">
				<h3 class="page-header chat-page-header row"><?php echo $this->header()?></h3>
				<div class="dynamic">
					<?php echo $this->show_list()?>
				</div>
				<div class="row answer">
					<?php echo $this->answer()?>
				</div>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	// только список сообщений для возможности отрисовки из апи
	function show_list($full = true){
		// pdo-mysql types bug
		foreach ($this->list as &$message){
			foreach ($message as $key => &$value){
				if (in_array($key, ['message_id', 'chat_id', 'viewed', 'delivered']) ||
					$key == 'operator_id' && !is_null($value)) $value = (int)$value;
			}
		}

		// для режима пользователя не рендерим полную картину
		if ($this->attr('user_mode')){
			foreach ($this->list as &$message){
				// удаляем параметры, которые не нужно передавать в api
				unset($message['end_time'], $message['date']);
			}
			return $this->list;
		}

	//	print_r($this->list);exit;

	//	ob_start();
		$out = '';
		foreach ($this->list as &$message){
//			print_r($message);continue;
			$class = $message['operator_id'] ? 'alert-success' : 'alert-info';
			$class_outer = $message['operator_id'] ? 'col-md-offset-4' : '';

			if ($message['operator_id']){
				$name = $message['operator_name'];
			} else {
				$name = $message['user_name'];
			}

			if (!$name) $name = '('.w('unnamed').')';

			if ($message['user_email']){
				$pre_icon_title = str_replace('%user_email%', $message['user_email'], w('message-was-sent-to-email'));
				$pre_icon = '<i title="'.$pre_icon_title.'" class="fa fa-envelope"></i>';
			} else {
				$pre_icon = '';
			}

			if ($full) $out .= $this->full($message, $pre_icon, $class, $class_outer, $name);
			else $out .= $this->tiny($message, $name);
		}

		return $out;
	}

	function files($list, $ul_attrs = []){
		if (!count($list)) return '';
		$ul = HNode::standart('ul', $ul_attrs);
		foreach ($list as $file){
		//	$size = HFile::_size($file['file_id'], $file['file_name']);
			$path = HFile::_path($file['file_id'], $file['file_name']);

			$ul->add(
				HNode::standart('li')
					->add(
						HNode::standart('a', ['href' => $path])->addText($file['file_name'].' ('.$file['file_size'].')')
					)
			);
		}
		return $ul->show();
	}

	// вывод краткого сообщения для e-mail
	function tiny($message, $name){
		ob_start(); ?>
		<span style="color: #999"> (<?php echo $message['date']?>) <b><?php echo $name?></b>: </span>
		<?php echo nl2br(htmlspecialchars($message['text']))?>
		<?php echo $this->files($message['files'])?>
		<br/>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	// вывод полного сообщения для оператора
	function full($message, $pre_icon, $class, $class_outer, $name){
		ob_start(); ?>
		<div class="row" data-message_id="<?php echo $message['message_id']?>">
			<div class="col-md-8 <?php echo $class_outer?>">
				<div class="alert <?php echo $class?>">
					<span class="grey"><?php echo $pre_icon?> (<?php echo $message['date']?>) <b><?php echo $name?></b>: </span>
					<?php echo nl2br(htmlspecialchars($message['text']))?>
					<?php echo $this->files($message['files'], ['class' => 'message-files'])?>
				</div>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}