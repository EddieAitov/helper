<?php
class HNodeForm extends HNode {
	const TypeStandart = 100;
	// const TypeSuperCustom - для возможности, например, сделать файловую форму верхнего уровня

	const ButtonSave = 1;
	const ButtonApply = 2;
	const ButtonSaveAndAdd = 3;
	const ButtonClone = 4;
	const ButtonRemove = 5;

	// default button set
	protected $buttons = [self::ButtonSave, self::ButtonApply, self::ButtonSaveAndAdd, self::ButtonClone, self::ButtonRemove];

	function title_extra($separator = ' / '){
		switch ($this->attr('action_type')){
			case HQuery::TypeInsert:
				return $separator.w('create-title');
			case HQuery::TypeUpdate:
				return $separator.w('update-title');
			default:
				return '';
		}
	}

	function setButtons($buttons){
		$this->buttons = $buttons;
	}

	function toggleButton($button, $state){
		if ($state && !in_array($button, $this->buttons)) $this->buttons[] = $button;
		if (!$state && in_array($button, $this->buttons)) $this->buttons = array_diff($this->buttons, [$button]);
	}

	function title(){
		return $this->name().$this->title_extra();
	}

	function show_message($check_attr, $message){
		ob_start();
		if ($this->attr($check_attr)){ ?>
			<div class="alert alert-success entry-updated">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo w($message)?>
			</div>
		<?php }
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	// редактирование объекта из раздела
	function show(){
		// this list is also buttons order
		/** @var HNode[] $button_nodes */
		$button_nodes = [
			self::ButtonSave => HNode::standart('a', [
				'class' => 'btn btn-lg btn-primary',
				'data-return' => 'list',
				'data-click' => 'jhelper.manager.save'
			])->addText('<i></i> '.w('save')),
			self::ButtonApply => HNode::standart('a', [
				'class' => 'btn btn-lg btn-default',
				'data-return' => 'item',
				'data-click' => 'jhelper.manager.save'
			])->addText('<i></i> '.w('apply')),
			self::ButtonSaveAndAdd => HNode::standart('a', [
				'class' => 'btn btn-lg btn-default',
				'data-return' => 'new',
				'data-click' => 'jhelper.manager.save'
			])->addText('<i></i> '.w('save-and-add')),
			self::ButtonRemove => HNode::standart('a', [
				'class' => 'btn btn-lg btn-danger float-right',
				'data-return' => 'new',
				'data-click' => 'jhelper.manager.remove_entry'
			])->addText(w('remove')),
			self::ButtonClone => HNode::standart('a', [
				'class' => 'btn btn-lg btn-default float-right',
				'data-click' => 'jhelper.manager.create_clone'
			])->addText(w('clone'))
		];

		ob_start(); ?>
		<form enctype="multipart/form-data" method="post" class="manager-form" data-row_id="<?php echo $this->attr('row_id')?>">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header"><?php echo $this->title()?></h1>
				</div>
			</div>
			<?php echo $this->show_message('entry_updated', 'entry-updated')?>
			<?php echo $this->show_message('entry_cloned', 'entry-cloned')?>
			<div class="row">
				<?php echo $this->show_children()?>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<hr/>
					<fieldset class="btn-toolbar">
						<?php
						foreach ($button_nodes as $key => $button_node){
							if (in_array($key, $this->buttons)) echo $button_node->show();
						}
						?>
					</fieldset>
					<hr/>
				</div>
			</div>
		</form>
		<?php
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}
}