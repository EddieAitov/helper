<?php
/*
 * view for order table
 */
class HNodeOrder extends HNode {
	/** @var HModule $module */
	protected $module;
	/** @var array */
	protected $rows;

	/**
	 * @param HModule $module
	 * @param array $rows
	 */
	function __construct($module, $rows){
		$this->module = $module;
		$this->rows = $rows;
	}

	/**
	 * @return string
	 */
	function show(){
		$columns = $this->module->columns();

		ob_start(); ?>
		<table class="table table-bordered order-table">
			<thead>
			<tr>
				<?php
				foreach ($columns as $column){
					echo '<th>'.$column->name().'</th>';
				}
				?>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($this->rows as $row){
				$tr = HNode::standart('tr');
				foreach ($row as $value) $tr->add(HNode::standart('td')->addText($value));
				$tr->child(0)->add(HNodeField::hidden('ids[]', $tr->child(0)->show_children()));
				echo $tr->show();
			}
			?>
			</tbody>
		</table>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}