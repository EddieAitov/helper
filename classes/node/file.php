<?php
class HNodeFile extends HNodeAbstract {
	function show_wrapper(){
		//!$data['enabled'] && !$this->property('unblockable');

		ob_start(); ?>
		<div class="col-lg-6">
			<div class="panel panel-default" id="wrapper-<?php echo $this->module->id()?>" data-module_id="<?php echo $this->module->id()?>">
				<div class="panel-heading">
					<i class="fa fa-file fa-fw"></i>
					<?php echo w($this->module->name())?>
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default btn-xs" data-panel_toggle><i class="fa fa-chevron-up"></i></button>
					</div>
				</div>
				<ol class="panel-body wrapper-content row<?php echo ($this->enabled ? ' sortable' : '')?>">
					<?php echo $this->show_children()?>
				</ol>
				<?php if ($this->enabled){ ?>
					<div class="panel-footer padding0">
						<div class="tbl">
							<div class="tbl-cell small-cell">
								<button class="btn btn-sm btn-success file-upload" type="button"
										data-module_id="<?php echo $this->module->id()?>"><?php echo w('upload')?></button>
							</div>
							<div class="progress active tbl-cell" id="progress-<?php echo $this->module->id()?>">
								<div class="progress-bar progress-bar-info" role="progressbar"></div>
							</div>
							<div class="tbl-cell small-cell">
								<button class="btn btn-sm btn-default select-all" type="button"><?php echo w('select-all')?></button>
							</div>
							<div class="tbl-cell small-cell">
								<button class="btn btn-sm btn-danger remove-selected" disabled type="button"><?php echo w('remove-selected')?></button>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	function show_tablecell(){
		$id_field = $this->module->field_id();
		$this->add(HNodeField::hidden([$this->module->id(), $this->attr($id_field), $id_field], $this->attr($id_field)));

		ob_start();
		?>
		<div><small><a href="<?php echo $this->attr('src')?>"><?php echo $this->attr('filename')?></a></small></div>
		<?php echo $this->show_children()?>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	protected function path($file_id, $filename){
		$path = HELPER_UPLOAD_FOLDER;
		if ($this->module->data('folder')) $path .= '/'.$this->module->data('folder');
		$path .= '/'.floor($file_id / 1000).'/'.$file_id.'/'.$filename;

		return $path;
	}

	function show_listitem(){
		$path = $this->path($this->attr('file_id'), $this->attr('filename'));

		$this->child(0)->attr('class', 'col-lg-12');
		foreach ($this->child(0)->children() as &$child) $child->attr('form_horizontal', 1);

		ob_start(); ?>
		<li class="col-lg-12 file-element-wrapper element-wrapper form-horizontal">
			<div class="thumbnail">
				<?php if ($this->enabled){ ?>
					<input type="checkbox" class="selected"/>
					<button type="button" class="btn btn-danger file-remove"
							data-click="jhelper.file.remove_dom"><i class="fa fa-times"></i>
					</button>
				<?php } ?>
				<a href="<?php echo $path?>"><button class="btn btn-primary file-save" type="button"><i class="fa fa-save"></i></button></a>
				<div class="row form-group-sm">
					<?php echo $this->show_children()?>
				</div>
			</div>
		</li>
		<?php
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}
}