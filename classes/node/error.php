<?php
class HNodeError extends HNode {
	static function error($text){
		return new self(0, $text);
	}

	function show(){
		ob_start(); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="login-panel panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo w('error-occured')?></h3>
						</div>
						<div class="panel-body">
							<?php echo $this->name()?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}