<?php
// обертка и общие элементы списка
class HNodeItemlist extends HNode {
	/** @var HModule $module */
	protected $module;

	/**
	 * @param HModule $module
	 */
	function __construct($module){
		$this->module = $module;
	}

	/**
	 * @return string
	 */
	function show(){
		$last_th = '<th class="nosort th-editable">&nbsp;</th>';
		$th_class = $this->module->isSortable() ? '' : 'nosort';
		$columns = $this->module->columns();
		$data_attributes = [];
		if ($this->module->data('missing_lang'))
			$data_attributes[] = 'data-missing_lang="'.$this->module->data('missing_lang').'"';
		$data_attributes[] = 'id="table-'.$this->module->id().'"';

		if (count($data_attributes)) $data_attributes = ' '.implode(' ', $data_attributes);
		else $data_attributes = '';

//		print_r($columns);

		ob_start(); ?>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->module->name()?></h1>
			</div>
			<?php echo $this->actionButtons()?>
			<div class="col-lg-12">
				<div class="dynamic">
					<table class="table table-striped table-bordered table-hover data-table"<?php echo $data_attributes?>>
						<thead>
						<tr>
							<th class="nosort"><input type="checkbox"/></th>
							<?php
							foreach ($columns as $column){
								echo '<th class="'.($column->isField() ? $th_class : 'nosort').'">'.$column->name().'</th>';
							}
							echo $last_th;
							?>
						</tr>
						</thead>
						<?php if ($this->module->isFilterable()){ ?>
							<tfoot>
							<tr>
								<td>&nbsp;</td>
								<?php // for filter
								foreach ($columns as $column){
									$th = HNode::standart('th', ['class' => $column->isField() ? $th_class : 'nosort']);
									if ($column->isField()){
										$node = HNodeField::c($column->type(), '', $column->data());
										$th->add($node->filter());
									} else {
										$th->addText('&nbsp;');
									}
									echo $th->show();
								}
								echo $last_th;
								?>
							</tr>
							</tfoot>
						<?php } ?>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<?php echo $this->actionButtons()?>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	const ListActionAdd = 1;
	const ListActionOrder = 2;
	const ListActionDelete = 3;
	const ListActionClone = 4;

	protected function actionButtons(){
		$buttons = [
			self::ListActionAdd => '<a href="'.HRouter::link('manage/'.$this->module->id()).'" class="btn btn-lg btn-primary">'.w('create').'</a>',
			self::ListActionOrder => '<a data-click="jhelper.order.toggle" class="btn btn-lg btn-default order-toggle">'.w('order').'</a>',
			self::ListActionDelete => '<a data-click="jhelper.manager.remove_entries" class="btn btn-lg btn-danger float-right checkbox-dependent" disabled="disabled">'.w('delete-selected').'</a>',
			self::ListActionClone => '<a data-click="jhelper.manager.create_clones" class="btn btn-lg btn-default float-right checkbox-dependent" disabled="disabled">'.w('clone-selected').'</a>'
		];

		$actions = [];
		if ($this->module->canCreate()) $actions[] = self::ListActionAdd;
		if ($this->module->canUpdate() && $this->attr('order_enabled')) $actions[] = self::ListActionOrder;
		if ($this->module->canDelete()) $actions[] = self::ListActionDelete;
		if ($this->module->canCreate()) $actions[] = self::ListActionClone;

		$buttons_out = '';
		foreach ($actions as $action) $buttons_out .= $buttons[$action];
		if (!$buttons_out) return '';

		ob_start(); ?>
		<div class="col-lg-12">
			<fieldset class="btn-toolbar">
				<?php echo $buttons_out?>
			</fieldset>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	const ActionEdit = 4;
	const ActionSave = 1;
	const ActionClone = 2;
	const ActionDelete = 3;

	static function firstTd(){
		ob_start(); ?>
		<input type="checkbox"/>
		<?php
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	/**
	 * @param HModule $module
	 * @param [] $row
	 * @param array $actions
	 * @param int $row_id
	 * @return string
	 */
	static function lastTd($module, $actions, $row_id){
		$buttons = [
			self::ActionEdit => '<a class="btn btn-default btn-info" data-action="edit" title="'.w('edit').'"
				href="'.HRouter::link('manage/'.$module->id().'/'.$row_id).'"><i class="fa fa-edit fa-fw"></i></a>',
			self::ActionSave => '<a class="btn btn-default" data-action="save" title="'.w('save').'"
				data-click="jhelper.manager.save_tr"><i class="fa fa-save fa-fw"></i></a>',
			self::ActionClone => '<a class="btn btn-default" data-action="clone" title="'.w('clone').'"
				data-click="jhelper.manager.create_clone"><i class="fa fa-copy fa-fw"></i></a>',
			self::ActionDelete => '<a class="btn btn-default" data-action="delete" title="'.w('delete').'"
				data-click="jhelper.manager.remove_entry"><i class="fa fa-times fa-fw"></i></a>'
		];

		$buttons_out = '';
		foreach ($actions as $action) $buttons_out .= $buttons[$action];
		if (!$buttons_out) $buttons_out = '&nbsp;';
		ob_start();
		?>
		<?php echo $buttons_out?>
		<input type="hidden" name="action_type" value="<?php echo HQuery::TypeUpdate?>"/>
		<input type="hidden" name="module_id" value="<?php echo $module->id()?>"/>
		<input type="hidden" name="list_mode" value="1"/>
		<input type="hidden" name="row_id" value="<?php echo $row_id?>"/>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

}