<?php
class HNodeChats extends HListnode {
	// показать целиком блок чата
	function show(){
		ob_start(); ?>
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header row"><?php echo $this->name()?></h3>
				<div class="list-wrapper">
					<?php echo $this->show_list()?>
				</div>
				<?php if (count($this->list) == HELPER_CHATLIST_COUNT) echo $this->show_more_button($this->attr('method')); ?>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	function show_list(){
		ob_start();
		foreach ($this->list as $item){
		//Array ( [chat_id] => 14 [messenger_type_id] => 3 [user_name] => [user_email] => [user_phone] => [operator_id] => 1
		//	[ip] => 93.191.20.78 [status_id] => 2 [start] => 12.08.2014 16:15 [activity] => 12.08.2014 16:18 [date] => 12.08.2014
		//	[time] => 16:18 [today] => 0 [active] => 0 )
			//print_r(HNodeChat::standart('', $item));
			// стандартный метод формирования имени чата. отличие есть только при выводе в меню.
			$chat = HNodeChat::standart('', $item);
			$name = $chat->name().' '.$chat->extra_name();

			echo HNode::standart('p')
				->add(
					HNode::standart('a', ['href' => HRouter::link('chat/'.$item['chat_id'])])->addText($name)
				)
				->show();
		}
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}