<?php
class HNodeWrapper extends HNode {
	const TypeFieldWrapper = 100;

	static function field($name, $attributes = []){
		return new self(self::TypeFieldWrapper, $name, $attributes);
	}

	function show(){
		ob_start();

		switch ($this->type()){
			case self::TypeFieldWrapper: ?>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading"><i class="fa fa-file-text-o fa-fw"></i> <?php echo $this->name()?></div>
						<div class="panel-body">
							<?php echo $this->show_children()?>
						</div>
					</div>
				</div>
				<?php break;
		}

		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}