<?php
class HNodeAuth extends HNode {
	function show(){
		ob_start(); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-panel panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo w('authorization')?></h3>
						</div>
						<div class="panel-body">
							<form role="form" method="post" id="auth-form" action="">
								<input type="hidden" name="signin" value="1"/>
								<fieldset>
									<div class="form-group">
										<input class="form-control" placeholder="<?php echo w('login')?>" id="login" type="text"
											   autofocus data-enter="jhelper.auth.signin">
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="<?php echo w('password')?>" id="password" type="password"
											   value="" data-enter="jhelper.auth.signin">
									</div>
									<a onclick="jhelper.auth.signin()" class="btn btn-lg btn-success btn-block"><?php echo w('enter')?></a>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}