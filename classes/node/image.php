<?php
class HNodeImage extends HNodeAbstract {
	function show_wrapper(){
		//!$data['enabled'] && !$this->property('unblockable');

		ob_start(); ?>
		<div class="col-lg-6">
			<div class="panel panel-default" id="wrapper-<?php echo $this->module->id()?>" data-module_id="<?php echo $this->module->id()?>">
				<div class="panel-heading">
					<i class="fa fa-camera fa-fw"></i>
					<?php echo w($this->module->name())?>
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default btn-xs" data-panel_toggle><i class="fa fa-chevron-up"></i></button>
					</div>
				</div>
				<ol class="panel-body wrapper-content row<?php echo ($this->enabled ? ' sortable' : '')?>">
					<?php echo $this->show_children()?>
				</ol>
				<?php if ($this->enabled){ ?>
					<div class="panel-footer padding0">
						<div class="tbl">
							<div class="tbl-cell small-cell">
								<button class="btn btn-sm btn-success file-upload" type="button"
										data-module_id="<?php echo $this->module->id()?>"><?php echo w('upload')?></button>
							</div>
							<div class="progress active tbl-cell" id="progress-<?php echo $this->module->id()?>">
								<div class="progress-bar progress-bar-info" role="progressbar"></div>
							</div>
							<div class="tbl-cell small-cell">
								<button class="btn btn-sm btn-default select-all" type="button"><?php echo w('select-all')?></button>
							</div>
							<div class="tbl-cell small-cell">
								<button class="btn btn-sm btn-danger remove-selected" disabled type="button"><?php echo w('remove-selected')?></button>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	function show_tablecell(){
		$id_field = $this->module->findByType(HNodeField::TypeId)->field();
		$this->add(HNodeField::hidden([$this->module->id(), $this->attr($id_field), $id_field], $this->attr($id_field)));

		ob_start();
		?>
		<img src="<?php echo $this->attr('src')?>" onclick="$.fancybox('<?php echo $this->attr('big')?>')"/>
		<?php echo $this->show_children()?>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	protected function path($image_id, $filename, $index){
		$path = HELPER_UPLOAD_FOLDER;
		if ($this->module->data('folder')) $path .= '/'.$this->module->data('folder');
		$path .= '/'.floor($image_id / 1000).'/'.$image_id.'/'.$index.'/'.$filename;

		return $path;
	}

	function show_listitem(){
		$preview_index = (int)$this->module->data('preview_index');

		$small = $this->path($this->attr('image_id'), $this->attr('filename'), $preview_index);
		$big = $this->path($this->attr('image_id'), $this->attr('filename'), count($this->module->data('sizes')) - 1);

		ob_start(); ?>
		<li class="col-lg-12 image-element-wrapper element-wrapper form-horizontal">
			<div class="thumbnail">
				<?php if ($this->enabled){ ?>
					<input type="checkbox" class="selected"/>
					<button type="button" class="btn btn-danger image-remove"
							data-click="jhelper.file.remove_dom"><i class="fa fa-times"></i>
					</button>
				<?php } ?>
				<div class="row form-group-sm">
					<div class="col-lg-4">
						<a class="">
							<div class="image-element" style="background-image:url('<?php echo $small?>')" onclick="$.fancybox('<?php echo $big?>')"></div>
						</a>
					</div>
					<?php echo $this->show_children()?>
				</div>
			</div>
		</li>
		<?php
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}
}