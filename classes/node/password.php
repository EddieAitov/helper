<?php
class HNodePassword extends HNodeAbstract {
	function show_listitem(){
		ob_start(); ?>
		<div class="col-lg-12">
			<?php
			$notification = $this->attr('notification');
//			print_r($notification);
			if (!$notification){
				echo w('password-was-not-generated');
			} else {

				$link_text = $notification['user_name'].' (#'.$notification['initiator_id'].')';

				if (isset($notification['link'])){
					$initiator = HNode::standart('a', ['href' => $notification['link']])->addText($link_text)->show();
				} else {
					$initiator = $link_text;
				}

				echo str_replace(['%date%', '%user%'], [$notification['create_time'], $initiator], w('last-password-reset'));
			}
			?>
			<hr/>
			<?php if ($this->attr('action_type') == HQuery::TypeInsert){ ?>
				<?php echo w('pwd-info-create')?>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="field[<?php echo $this->module->id()?>][by_email]" value="1" checked="checked"/> <?php echo w('by-email')?>
					</label>
				</div>
			<?php } elseif ($this->attr('action_type') == HQuery::TypeUpdate){ ?>
				<?php echo w('pwd-info-update')?>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="field[<?php echo $this->module->id()?>][by_email]" value="1" checked="checked"/> <?php echo w('by-email')?>
					</label>
				</div>
				<a class="btn btn-default" data-click="jhelper.password.reset"><?php echo w('pwd-info-update-do')?></a>
			<?php } ?>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}