<?php
abstract class HNodeAbstract extends HNode {
	// wrapper for list inside object
	const TypeWrapper = 100;
	// list inside object
	const TypeListitem = 101;
	// main list cell
	const TypeTableCell = 102;

	/** @var HModule */
	protected $module;

	/**
	 * @param HModule $module
	 */
	function setModule($module){
		$this->module = $module;
	}

	/**
	 * @descr abstract factory for wrapper node
	 * @param HModule $module
	 * @return HNodeAbstract
	 */
	static function wrapper($module){
		$object = new static(self::TypeWrapper);
		$object->setModule($module);
		return $object;
	}

	/**
	 * @descr abstract factory for list item node
	 * @param HModule $module
	 * @param array $row
	 * @return HNodeAbstract
	 */
	static function listitem($module, $row){
		$object = new static(self::TypeListitem, '', $row);
		$object->setModule($module);
		return $object;
	}

	/**
	 * @descr abstract factory for table cells
	 * @param HModule $module
	 * @param array $row
	 * @return HNodeAbstract
	 */
	static function tableCell($module, $row){
		$object = new static(self::TypeTableCell, '', $row);
		$object->setModule($module);
		return $object;
	}

	/**
	 * @descr abstract factory for table cells with multi object
	 * @param HModule $module
	 * @param array $rows
	 * @return HNodeAbstract
	 */
	static function tableCellMulti($module, $rows){
		$out = new HNode(HNode::TypeEmpty);
		foreach ($rows as $row){
			$object = new static(self::TypeTableCell, '', $row);
			$object->setModule($module);
			$out->add($object);
		}
		return $out;
	}


	function show(){
		switch ($this->type()){
			case self::TypeWrapper:
				return $this->show_wrapper();
			case self::TypeListitem:
				return $this->show_listitem();
			case self::TypeTableCell:
				return $this->show_tablecell();
		}

		return '';
	}

	/* custom module view methods */
	function show_wrapper(){
		$icon = $this->module->data('icon') ? $this->module->data('icon') : 'fa fa-file-text-o fa-fw';
		ob_start(); ?>
		<div class="col-lg-6">
			<div class="panel panel-default" data-module_id="<?php echo $this->module->id()?>">
				<div class="panel-heading"><i class="<?php echo $icon?>"></i> <?php echo $this->module->name()?></div>
				<div class="panel-body row">
					<?php echo $this->show_children()?>
				</div>
			</div>
		</div>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
	function show_listitem(){ return ''; }
	function show_tablecell(){ return ''; }
}