<?php
class HNodeField extends HNode {
	/**
	 * field's type list.
	 * Example config:
	 * ['type' => 'textarea', 'field' => 'my_textarea', 'name' => 'locale-key-for-textarea'],
	 * ['type' => 'checkbox', 'field' => 'my_hidden_field'],
	 * ['type' => 'multi', 'query' => ['table' => 'tbl1', 'id' => 't_id', 'name' => 't_name'], 'name' => 'multi-field', 'field' => 'multi_t']
	 */

	// default name is taken from these values. leave these field names in DB as default (recommended)
	const TypeParentId = 'parent_id';
	const TypeModuleId = 'module_id';
	const TypeOrderby = 'orderby';

	// fixed field names - do not create these types in config.modules.php
	const TypeStartTime = 'start_time';
	const TypeEndTime = 'end_time';
	const TypeUpdateTime = 'update_time';
	const TypeEditorId = 'editor_id';

	// config field names
	const TypeHidden = 'hidden';
	const TypeId = 'id';
	const TypeUserId = 'user_id';
	const TypeText = 'text';
	const TypeTextarea = 'textarea';
	const TypeRedactor = 'redactor';
	const TypeCheckbox = 'checkbox';
	const TypeDate = 'date';
	const TypeDatetime = 'datetime';

	// use this type with "linked" attr
	const TypeLinked = 'linked';
	// use these types with "query" attr
	const TypeSelect = 'select';
	const TypeLink = 'link';
	const TypeMultiSelect = 'multi';

	// for information which is stored as text (ru, en, ...) -> creating select
	const TypeLanguageSelector = 'language';

	static function hidden($name, $value){
		return new self(self::TypeHidden, $name, ['value' => $value]);
	}

	// field's name. by default field's name is field[module_id][data_id][field_name]
	function name($extra_key = null){
		if (is_array($this->name)){
			$ar = $this->name;
			if (!is_null($extra_key)) $ar[] = $extra_key;
			$name = 'field';
			foreach ($ar as $el) $name .= "[{$el}]";
			return $name;
		} else {
			return $this->name;
		}
	}

	// return's value, or value's array element by its key (value[key])
	function value($extra_key = null){
		if (is_null($extra_key)) return $this->attr('value');
		if (is_array($this->attr('value')) && array_key_exists($extra_key, $this->attr('value')))
			return $this->attr('value')[$extra_key];
		return '';
	}

	// check if value is null or empty string
	function isEmpty(){
		return is_null($this->value()) || $this->value() === '';
	}

	function label(){
		return $this->attr('label');
	}

	function id(){
		return 'field-'.$this->attr('id');
	}

	/**
	 * @descr editing in list
	 * @return string
	 */
	function show_manage_itemlist(){
		// setting option for list
		$this->attr('list_mode', 1);
		// standart call
		return $this->show();
	}

	/**
	 * @descr standart node method for view-edit
	 * @return string
	 */
	function show(){
		// do not show empty disabled entries
//		if (!$this->enabled && $this->isEmpty() && !in_array($this->type(), [
//				self::TypeCheckbox
//			])) return '';

		// input wrapper
		$wrapper = HNode::standart('div', ['class' => 'form-group']);

		$form_horizontal = false;
	//	print_r($this->attributes);

		// add label (if conditions)
		if (
			!$this->attr('list_mode') &&
			!$this->attr('locale_mode') &&
			!in_array($this->type(), [self::TypeCheckbox, self::TypeHidden])
		){
			$attrs = [];
			if ($this->attr('form_horizontal')){
				$form_horizontal = true;
				$attrs['class'] = 'col-sm-4 control-label';
				// changing wrapper class!
				$wrapper->attr('class', 'row');
			}
			$wrapper->add(HNode::standart('label', $attrs)->addText($this->label()));
		}

		// multi languages hstore->locale
		if ($this->attr('locale')) $input = $this->locale();
		// standart input
		else $input = $this->input();

		// wrap input in div
		if ($form_horizontal) $input = HNode::standart('div', ['class' => 'col-sm-8'])->add($input);

		$wrapper->add($input);
	//	$wrapper->addText('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>');

		if ($this->attr('locale_mode') || in_array($this->type(), [self::TypeHidden])){
			$out = $wrapper->show_children();
		} else {
			$out = $wrapper->show();
		}

		return $out;
	}

	const YmdHis = '/^(\d{4})\-(\d{2})\-(\d{2})\s(\d{2}):(\d{2}):(\d{2})$/';
	const YmdHisms = '/^(\d{4})\-(\d{2})\-(\d{2})\s(\d{2}):(\d{2}):(\d{2})\.?(\d{0,6})$/';
	const dmYHi = '/^(\d{2})\.(\d{2})\.(\d{4})\s(\d{2}):(\d{2})$/';
	const dmYHis = '/^(\d{2})\.(\d{2})\.(\d{4})\s(\d{2}):(\d{2}):(\d{2})$/';
	const Ymd = '/^(\d{4})\-(\d{2})\-(\d{2})$/';
	const dmY = '/^(\d{2})\.(\d{2})\.(\d{4})$/';

	protected function convertForManager($type, $value){
		switch ($type){
			case self::TypeDatetime:
				$value = preg_replace(self::YmdHisms, '$3.$2.$1 $4:$5', $value);
				break;
			case self::TypeDate:
				$value = preg_replace(self::Ymd, '$3.$2.$1', $value);
				break;
		}

		return $value;
	}

	// just field
	protected function input($type = null){
		if (is_null($type)) $type = $this->type();
		// name, class, disabled, id -> standart attributes
		$attrs = [
			'name' => $this->name(),
			'class' => 'form-control',
			'id' => $this->id()
		];
		if (!$this->enabled) $attrs['disabled'] = 'disabled';
		if ($this->attr('inputmask')) $attrs['data-inputmask'] = 1;
		if ($this->attr('class')) $attrs['class'] .= ' '.$this->attr('class');

		switch ($type){
			case self::TypeId:
				$input = HNode::standart('div', ['class' => 'form-control-static'])
					->addText($this->value())
					->add(HNodeField::hidden($this->name(), $this->value()));
				break;
			case self::TypeCheckbox:
				// totally rewrite $attrs for checkbox mode
				$attrs = [
					'type' => 'checkbox',
					'name' => $this->name(),
					'value' => 1
				];
				if ($this->value()) $attrs['checked'] = 'checked';
				if (!$this->enabled) $attrs['disabled'] = 'disabled';

				$object = HNode::standart('label')->add(HNode::single('input', $attrs));
				if (!$this->attr('list_mode')) $object->addText($this->label());

				$input = HNode::standart('div', ['class' => 'checkbox'])->add($object);
				break;
			case self::TypeLink:
				$value = '<a href="'.str_replace('%value%', $this->value(), $this->attr('link')).'">Перейти</a>';
				$input = HNode::standart('div', ['class' => 'link'])->addText($value);
				break;
			case self::TypeDate:
			case self::TypeDatetime:
			case self::TypeText:
				$attrs['type'] = 'text';
				$attrs['value'] = $this->convertForManager($type, $this->value());
				$attrs['placeholder'] = $this->attr('placeholder');
				if ($this->attr('alias')) $attrs['data-alias'] = $this->attr('alias');
				// dom differs with data attribute
				if ($type != self::TypeText) $attrs['data-'.$type] = 1;
				$input = HNode::single('input', $attrs);
				break;
			case self::TypeLinked:
				$input = HNode::standart('select', $attrs);
				// zero value option
				$input->add(HNode::standart('option', ['value' => 0])->addText(w('not-selected')));
				// options list
//				echo $this->value().print_r($this->attr('linked_rows'),1);
				foreach ($this->attr('rows') as $row){
					$option_attrs = ['value' => $row['id']];
					if ($this->value() == $row['id']) $option_attrs['selected'] = 'selected';
					$input->add(HNode::standart('option', $option_attrs)->addText($row['name']));
				}
				break;
			case self::TypeSelect:
				$input = HNode::standart('select', $attrs);
				$input->add(HNode::standart('option', ['value' => 0])->addText(w('not-selected')));
				foreach ($this->attr('rows') as $row){
					$option_attrs = ['value' => $row['id']];
					if ($this->value() == $row['id']) $option_attrs['selected'] = 'selected';
					$input->add(HNode::standart('option', $option_attrs)->addText($row['name']));
				}
				break;
			case self::TypeMultiSelect:
				$attrs['multiple'] = 'multiple';
				$attrs['name'] = $this->name('');	// extra brackets for field's name (for multiple values support)
				$value = [];
				if ($this->value()) $value = explode(',', $this->value());

				$input = HNode::standart('select', $attrs);
				foreach ($this->attr('rows') as $row){
					$option_attrs = ['value' => $row['id']];
					if (in_array($row['id'], $value)) $option_attrs['selected'] = 'selected';

					$input->add(HNode::standart('option', $option_attrs)->addText($row['name']));
				}
				break;
			case self::TypeTextarea:
				$attrs['rows'] = 5;
				if ($this->attr('maxlength')) $attrs['maxlength'] = $this->attr('maxlength');
				$input = HNode::standart('textarea', $attrs)->addText($this->value());
				break;
			case self::TypeRedactor:
				$attrs['class'] .= ' ckeditor';
				$input = HNode::standart('textarea', $attrs)->addText($this->value());
				break;
			case self::TypeLanguageSelector:
				$input = HNode::standart('select', $attrs);
				foreach (HLang::get_list() as $language){
					$option_attrs = ['value' => $language];
					if ($language == $this->value()) $option_attrs['selected'] = 'selected';
					$input->add(HNode::standart('option', $option_attrs)->addText(strtoupper($language)));
				}
				break;
			case self::TypeHidden:
				$input = HNode::single('input', ['type' => 'hidden', 'name' => $this->name(), 'value' => $this->value()]);
				break;
			default:	// error
				$input = HNode::text('Unexpected field type ('.$type.')!');
		}

		return $input;
	}

	/**
	 * @descr packed locale array (ex-hstore())
	 * @return HNode
	 */
	protected function locale(){
		$wrapper = HNode::standart('div', ['class' => 'locale-container']);

		$button_wrapper = HNode::standart('div', ['class' => 'btn-group language-toggle', 'data-toggle' => 'buttons']);
		$field_array = [];

		$languages = $this->attr('allowed_languages');
		foreach ($languages as $language){
			// active language or first language if no active
			$active = $language == $this->attr('current_language') ||
				(!in_array($this->attr('current_language'), $languages) && $language == reset($languages));

			$i_name = $this->name($language);
			$i_value = $this->value($language);
			$attrs = ['value' => $i_value, 'id' => $this->attr('id').'-'.$language, 'locale_mode' => 1];
			if (!$active) $attrs['class'] = 'hidden2';
			// new field view
			$field = new HNodeField($this->type(), $i_name, $attrs);
			if (!$this->enabled) $field->disable();
			$field_array[] = $field;

			switch (true){
				case $active:
					$class = 'btn btn-xs btn-primary active';
					break;
				case $i_value:
					$class = 'btn btn-xs btn-gray';
					break;
				default:
					$class = 'btn btn-xs btn-default';
			}

			$button = HNode::standart('div', [
				'class' => $class,
				'data-click' => 'jhelper.locale.toggle_field_language',
				'data-field' => $this->name(),
				'data-language' => $language
			])
				->add(HNode::single('input', ['type' => 'radio', 'name' => 'options']))
				->addText(strtoupper($language));
			$button_wrapper->add($button);
		}

		$language = HLang::helper_language();
		if ($this->value($language)){
			$original_text = HNode::standart('div', ['class' => 'original'])
				->add(HNode::standart('span', ['class' => 'gray'])
					->addText(w('source').' ('.strtoupper($language).'): '))
				->addText($this->value($language));
		} else {
			$original_text = HNode::text('');
		}

		$wrapper->add($button_wrapper);
		$wrapper->add($original_text);
		$wrapper->addChildren($field_array);

		return $wrapper;
	}

	function filter_select(){
		$out = $this->input(self::TypeSelect);
		$out->removeAttr('name')->removeAttr('class')->removeAttr('id');
		/** @noinspection PhpUndefinedMethodInspection */
		$out->child(0)->clear()->addText('-')->attr('value', '');
		return $out;
	}

	function filter(){
		switch ($this->type()){
			case self::TypeCheckbox:
				$this->attr('rows', [
					['id' => 1, 'name' => w('yes')],
					['id' => 0, 'name' => w('no')]
				]);
				$out = $this->filter_select();
				break;
			case self::TypeMultiSelect:
				$out = $this->filter_select();
				break;
			default:
				$out = HNode::standart('input', [
					'type' => 'search',
					'placeholder' => w('search').' '.w($this->attr('name')),
					'required' => 'required'
				]);
		}

		return $out;
	}

	// list output without editing
	function show_itemlist(){
		$value = $this->value();

		// conversion from database
		switch ($this->type()){
			case self::TypeCheckbox:
				$value = $value ? '<i class="fa fa-check fa-fw"></i>' : '<i class="fa fa-times fa-fw"></i>';
				break;
			case self::TypeDate:
			case self::TypeDatetime:
				$value = preg_replace('/^(\d{4})\-(\d{2})\-(\d{2})\s((\d{2}):(\d{2}):(\d{2})).*/', '$3.$2.$1 $5:$6', $value);
				break;
			case self::TypeMultiSelect:
			case self::TypeLinked:
				$ids = explode(',', $value);
				$value = [];
				foreach ($ids as $id){
					foreach ($this->attr('rows') as $row){
						if ($row['id'] == $id){
							$value[] = $row['name'];
							break;
						}
					}
				}

				if (!count($value)) $value[] = '&nbsp;';
				// visual output -> extra space
				$value = implode(', ', $value);
				break;
		}

		if ($this->attr('locale')){
			$language = HLang::helper_language();

			$translations = [];
			foreach (HLang::get_list() as $l){
				if (array_key_exists($l, $value) && $value[$l]) $translations[] = strtoupper($l);
			}

			if (array_key_exists($language, $value)) $value = $value[$language];
			else $value = '';

			// show filled translations for the record
			$value = '<span class="label label-default">'.implode(', ', $translations).'</span> '.$value;
		}

		return $value;
	}

}