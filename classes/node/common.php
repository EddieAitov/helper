<?php
// стандартные элементы интерфейса (чтобы не плодить классы)
class HNodeCommon extends HNode {
	// 100+ - типы в классах, которые наследуют HNode
	const TypeFormBlock = 100;
	const TypeModal = 101;
	const TypeHeader = 102;
	const TypeLeft = 103;
	const TypeMenuChatElement = 104;
	const TypeMenuElement = 105;

	protected $configuration, $active;
	function set_configuration($c){
		$this->configuration = $c;
	}

	// конструктор для передачи конфига
	static function header($user_data, $configuration){
		$node = new self(self::TypeHeader, '', $user_data);
		$node->set_configuration($configuration);
		return $node;
	}

	static function form_block($name, $icon){
		return new self(self::TypeFormBlock, $name, ['icon' => $icon]);
	}

	function setActive(){
		$this->active = true;
	}

	function isActive(){
		return $this->active || ($this->attr('link') && HRouter::compare($this->attr('link')));
	}

	function show(){
		switch ($this->type()){
			case self::TypeFormBlock:
				ob_start(); ?>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading"><i class="<?php echo $this->attr('icon')?>"></i> <?php echo $this->name()?></div>
						<div class="panel-body">
							<?php echo $this->show_children()?>
						</div>
					</div>
				</div>
				<?php
				$out = ob_get_contents();
				ob_end_clean();
				break;
			case self::TypeModal:
				ob_start(); ?>
				<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="modal-title"></h4>
							</div>
							<div class="modal-body" id="modal-body"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary hidden2"
										id="modal-accept"><?php echo w('do-confirm')?></button>
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo w('close')?></button>
							</div>
						</div>
					</div>
				</div>
				<?php
				$out = ob_get_contents();
				ob_end_clean();
				break;
			case self::TypeHeader:
				ob_start(); ?>
				<nav class="navbar navbar-default navbar-static-top margin-bottom-0" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="<?php echo HELPER_PREFIX?>" class="logo"><img src="<?php echo HELPER_PREFIX?>/images/logo.png"/></a>
					</div>
					<!-- /.navbar-header -->
					<ul class="nav navbar-top-links navbar-right">
						<li class="header-user"><?php echo $this->attr('first_name')?> <?php echo $this->attr('last_name')?></li>
						<?php if (HELPER_CHAT_ENABLED && $this->attr('chat_type_ids')){ ?>
						<li>
							<a href="<?php echo HRouter::link('all')?>" title="<?php echo w('chats')?>">
								<i class="fa fa-comments-o fa-fw"></i>
							</a>
						</li>
						<?php } ?>
						<?php foreach ($this->configuration['groups'] as $group){ ?>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#" title="<?php echo w($group['name'])?>">
									<i class="<?php echo $group['icon']?>"></i>  <i class="fa fa-caret-down"></i>
								</a>
								<ul class="dropdown-menu dropdown-user">
								<?php
								foreach ($this->configuration['modules'] as $module){
									if (!isset($module['group']) || $module['group'] != $group['class']) continue;
									if (!isset($module['icon'])) $module['icon'] = '';
									?>
									<li>
										<a href="<?php echo HRouter::link('itemlist/'.$module['id'])?>">
											<i class="<?php echo $module['icon']?>"></i> <?php echo w($module['name'])?>
										</a>
									</li>
									<?php
								}
								foreach ($this->configuration['tools'] as $tool){
									if ($tool['group'] != $group['class']) continue;
									if (!isset($tool['icon'])) $tool['icon'] = '';
									if (isset($tool['disabled']) && $tool['disabled']){ ?>
										<li>
											<a class="disabled-link">
												<i class="<?php echo $tool['icon']?>"></i> <?php echo w($tool['name'])?>
											</a>
										</li>
									<?php } else { ?>
										<li>
											<a onclick="jhelper.ajax('tool/<?php echo $tool['id']?>')">
												<i class="<?php echo $tool['icon']?>"></i> <?php echo w($tool['name'])?>
											</a>
										</li>
									<?php }
								}
								?>
								</ul>
								<!-- /.dropdown-messages -->
							</li>
						<?php } ?>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="<?php echo HRouter::link('profile')?>"><i class="fa fa-user fa-fw"></i> <?php echo w('profile')?></a></li>
								<li><a data-click="jhelper.auth.signout"><i class="fa fa-sign-out fa-fw"></i> <?php echo w('logout')?></a></li>
							</ul>
							<!-- /.dropdown-user -->
						</li>
						<!-- /.dropdown -->
					</ul>
					<!-- /.navbar-top-links -->
				</nav>
				<!-- /.navbar-static-top -->
				<?php
				$out = ob_get_contents();
				ob_end_clean();
				break;
			case self::TypeLeft:
				ob_start(); ?>
				<nav class="navbar-default navbar-static-side" role="navigation" id="<?php echo $this->attr('id')?>">
					<div class="sidebar-collapse">
						<ul class="nav" id="side-menu"<?php echo ($this->update ? ' data-update="left_menu"' : '')?>>
							<?php
								foreach ($this->children() as $child) echo $child->show();
							?>
						</ul>
						<!-- /#side-menu -->
					</div>
					<!-- /.sidebar-collapse -->
				</nav>
				<?php
				$out = ob_get_contents();
				ob_end_clean();
				break;
			case self::TypeMenuChatElement:
				$class = '';

				if ($this->attr('status_id') == HStatus::StatusFinished){
					$icon = 'glyphicon glyphicon-stop';
				// чат не начат оператором
				} elseif (!$this->attr('operator_id')){
					$class = 'bg-success';
					$icon = 'glyphicon glyphicon-fire';
				// пользователь чата активен
				} elseif ($this->attr('active')){
					$icon = 'glyphicon glyphicon-eye-open';
				// пользователь чата неактивен
				} else {
					$icon = 'glyphicon glyphicon-eye-close';
				}

				if ($this->attr('active_menu')) $class = 'bg-primary';
				// название
				$this->name = ($this->attr('user_name') ? $this->attr('user_name') : '('.w('unnamed').')').'
					('.($this->attr('today') ? $this->attr('time') : $this->attr('date')).')';

				$out = $this->li('chat/'.$this->attr('chat_id'), $this->name(), $class, $icon);
				break;
			case self::TypeMenuElement:
				$out = $this->li($this->attr('link'), $this->name(), $this->attr('class'), $this->attr('icon'));
				break;
			default:
				$out = '';
		}

		return $out;
	}

	function li($link, $name, $class = '', $icon = ''){
		// активный пункт меню, если совпадает ссылка
		if ($this->isActive()) $class = 'bg-primary';
		$has_children = (bool)count($this->children());

		$url = $link ? HRouter::link($link) : '#';

		ob_start(); ?>
			<li<?php echo ($this->isActive($link) ? ' class="active"' : '')?>>
				<a href="<?php echo $url?>"<?php echo ($class ? ' class="'.$class.'"' : '')?>>
					<?php if ($icon){ ?>
						<i class="<?php echo $icon?>"></i>
					<?php } ?>
					<?php echo $name?>
					<?php if ($has_children){ ?>
						<span class="fa arrow"></span>
					<?php } ?>
				</a>
				<?php if ($has_children){ ?>
					<ul class="nav nav-second-level">
						<?php echo $this->show_children()?>
					</ul>
				<?php } ?>
			</li>
		<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}