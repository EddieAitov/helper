<?php
class HAnswer extends HBase {
	const api = 1;

	function inc_fast($params){
		$this->parent()->check()->numeric(['answer_id'], $params);
		HQuery::c($this)
			->update('helper_fast_answers')
			->set('use_count', HQuery::Inc)
			->where_equal('answer_id', $params['answer_id'])
			->ex();
	}
}