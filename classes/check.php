<?php
class HCheck extends HBase {
	function exist($keys, $params, $ex = 'not-all-parameters-passed'){
		foreach ($keys as &$key)
			if (!isset($params[$key]))
				throw new HException($ex);
	}

	function is_not_empty($keys, $params, $ex = 'parameter-can-not-be-empty'){
		$this->exist($keys, $params, $ex);
		foreach ($keys as &$key)
			if (!$params[$key])
				throw new HException($ex);
	}

	function numeric($keys, $params){
		$this->exist($keys, $params);
		foreach ($keys as &$key)
			if (!is_numeric($params[$key]))
				throw new HException('parameter-must-be-an-integer');
	}

	function date($keys, $params){
		$this->exist($keys, $params);
		foreach ($keys as &$key)
			if (!preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $params[$key]))
				throw new HException('wrong-date-format');
	}

	function in_array($keys, $params, $array, $ex = 'parameter-value-is-not-allowed'){
		$this->exist($keys, $params, $ex);
		foreach ($keys as &$key)
			if (!in_array($params[$key], $array))
				throw new HException($ex);
	}

	function is_array($keys, $params, $ex = 'parameter-must-be-array'){
		$this->exist($keys, $params, $ex);
		foreach ($keys as &$key)
			if (!is_array($params[$key]))
				throw new HException($ex);
	}

	function float($keys, &$params){
		$this->exist($keys, $params);
		foreach ($keys as &$key)
			if (!is_float($params[$key]) && !is_numeric($params[$key]))
				throw new HException('parameter-must-be-a-float');
	}
}