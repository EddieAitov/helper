<?php
class HQuery extends HBase {
	const TypeSelect = 1;
	const TypeUpdate = 2;
	const TypeInsert = 3;
	const TypeDelete = 4;

	protected $fields = [],
		$packed_fields = [],
		$table,
		$alias,
		$where = [],
		$join = [],
		$group,
		$order,
		$limit,
		$offset,
		$returning,
		$fields_values = [],
		$where_values = [],
		$join_values = [],
		$type;

//	const Now = 'now()';
	const Now = 'date_trunc(\'second\', now())';
	const Inc = '%increment%';

	protected $constants = [HQuery::Now, HQuery::Inc];

	protected function is_constant($value){
		return in_array($value, $this->constants);
	}

	/**
	 * @param HController|HBase|MBase|CBase $obj
	 * @return HQuery
	 */
	static function c($obj){
		if (get_class($obj) != 'HController'){
			if (method_exists($obj, 'helper')) $obj = $obj->helper();
			elseif (method_exists($obj, 'controller')) $obj = $obj->controller()->helper();
			else $obj = $obj->parent();
		}
		return new self($obj);
	}

	protected function q(){
		$q = [
			DB_MYSQL => '`',
			DB_POSTGRE => ''
		];
		return $q[DB_TYPE];
	}

	function select($array){
		if (count($this->fields)) return $this->add_select($array);

		$this->type = $this::TypeSelect;
		if (!is_array($array)) $array = [$array];
		$this->fields = $array;
		return $this;
	}

	protected function add_select($array){
		if (!is_array($array)) $array = [$array];
		$this->fields = array_merge($this->fields, $array);
		return $this;
	}

	function clear_select(){
		$this->fields = [];
		return $this;
	}

	const FormatDate = 'DD.MM.YYYY';
	const FormatTime = 'HH24:MI';
	const FormatDateTime = 'DD.MM.YYYY HH24:MI';

	function select_time($field, $format, $alias){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->select("to_char({$field}, '{$format}') as {$alias}");
			case DB_MYSQL:
				$format = str_replace(
					['DD', 'MM', 'YYYY', 'HH24', 'MI', 'SS'],
					['%d', '%m', '%Y', '%H', '%i', '%s'],
					$format
				);
				return $this->select("date_format({$field}, '{$format}') as `{$alias}`");
			default:
				throw new HException('wrong-db-type');
		}
	}

	function select_today($field, $alias){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->select("case when {$field}::date = now()::date then 1 else 0 end as {$alias}");
			case DB_MYSQL://select date(now()) = curdate()
				return $this->select("if (date({$field}) = curdate(), 1, 0) as `{$alias}`");
			default:
				throw new HException('wrong-db-type');
		}
	}

	function select_active($field, $alias, $extra_condition = false){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->select("case when {$field} + interval '".HELPER_ACTIVITY_INTERVAL." second' > now() ".
					($extra_condition ? 'and '.$extra_condition : '').
					" then 1 else 0 end as {$alias}");
			case DB_MYSQL:
				return $this->select("if ({$field} + interval ".HELPER_ACTIVITY_INTERVAL." second > now() ".
					($extra_condition ? 'and '.$extra_condition : '').", 1, 0) as `{$alias}`");
			default:
				throw new HException('wrong-db-type');
		}
	}

	function update($table){
		$this->type = self::TypeUpdate;
		$this->table = $table;
		return $this;
	}

	function insert_into($table){
		$this->type = self::TypeInsert;
		$this->table = $table;
		return $this;
	}

	function delete_from($table){
		$this->type = self::TypeDelete;
		$this->table = $table;
		return $this;
	}

	function set($field, $value, $packed = false){
		$this->fields[] = $field;
		$this->fields_values[] = $value;
		// packed works for postgre (adding data). for mysql its just overwriting data. TODO: mysql solution
		if ($packed) $this->packed_fields[] = $field;
		return $this;
	}

	function from($table, $alias = null){
		$this->table = $table;
		$this->alias = $alias;
		return $this;
	}

	/**
	 * @param $text
	 * @param array|string|int $values
	 * @return $this
	 */
	function where($text, $values = []){
		$this->where[] = "({$text})";
		if (!is_array($values)) $values = [$values];
		$this->where_values = array_merge($this->where_values, $values);
		return $this;
	}

	function where_or($array, $values = []){
		return $this->where(implode(' or ', $array), $values);
	}

	function where_and($array, $values = []){
		return $this->where(implode(' and ', $array), $values);
	}

	function where_ilike($field, $value){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->where("{$field} ilike ?", $value);
			case DB_MYSQL:
				return $this->where("{$field} like ?", $value);
			default:
				throw new HException('wrong-db-type');
		}
	}

	function where_equal($field, $value){
		return $this->where("{$field} = ?", $value);
	}

	function where_not_equal($field, $value){
		return $this->where("{$field} <> ?", $value);
	}

	function where_null($field){
		return $this->where("{$field} is null");
	}

	function where_not_null($field){
		return $this->where("{$field} is not null");
	}

	/* special conditions */
	function where_not_deleted($table = null){
		if (!$table) $table = $this->alias ? $this->alias : $this->table;
		$extra = DB_TYPE == DB_MYSQL ? "or {$table}.end_time = '0000-00-00 00:00:00' or {$table}.end_time = ''" : "";
		$this->where("{$table}.end_time is null {$extra} or {$table}.end_time > now()");
		return $this;
	}

	function where_null_or_any($field, $ids_field){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->where("{$field} is null or {$field} = any(('{' || {$ids_field} || '}')::int[])");
			case DB_MYSQL:
				return $this->where("{$field} is null or find_in_set({$field}, {$ids_field})");
			default:
				throw new HException('wrong-db-type');
		}
	}

	function where_any($field, $ids_field){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->where("{$field} = any(('{' || {$ids_field} || '}')::int[])");
			case DB_MYSQL:
				if (preg_match('/,/', $ids_field)) $ids_field = "'{$ids_field}'";
				return $this->where("find_in_set({$field}, {$ids_field})");
			default:
				throw new HException('wrong-db-type');
		}
	}

	function where_null_or_equal($field, $value){
		$this->where("{$field} = ? or {$field} is null", $value);
		return $this;
	}

	function where_not_older($field, $count, $measure = 'day'){
		switch (DB_TYPE){
			case DB_POSTGRE:
				return $this->where("{$field} + interval '{$count} {$measure}' >= now()");
			case DB_MYSQL:
				return $this->where("{$field} + interval {$count} {$measure} >= now()");
			default:
				throw new HException('wrong-db-type');
		}
	}

	function where_in($field, $values){
		return $this->where("{$field} in ({$this->question_signs($values)})", $values);
	}

	/**
	 * @param $text
	 * @param array|int|string $values
	 * @return $this
	 */
	function join($text, $values = []){
		$this->join[] = $text;
		if (!is_array($values)) $values = [$values];
		$this->join_values = array_merge($this->join_values, $values);
		return $this;
	}

	const LeftOuterJoin = 1;
	const InnerJoin = 2;

	function join_using($table, $column, $join_type = self::LeftOuterJoin, $join_table_column = false, $tableSuffix = ''){
		switch ($join_type){
			case self::LeftOuterJoin:
				$join = 'left outer join';
				break;
			case self::InnerJoin:
				$join = 'inner join';
				break;
			default:
				throw new HException('wrong-join-type');
		}

		$current_table = $this->alias ? $this->alias : $this->table;
		if (!$join_table_column) $join_table_column = $column;
		$alias = $tableSuffix ? $table.$tableSuffix : '';

		return $this->join("{$join} {$table} {$alias} on ".($alias ? $alias : $table).".{$join_table_column} = {$current_table}.{$column}");
	}

	function group($text){
		$this->group = $text;
		return $this;
	}

	function order($text, $direction = ''){
		if ($direction) $direction = ' '.$direction;
		$this->order = $text.$direction;
		return $this;
	}

	function limit($num){
		$this->limit = (int)$num;
		return $this;
	}

	function offset($num){
		$this->offset = (int)$num;
		return $this;
	}

	function returning($text){
		$this->returning = $text;
		return $this;
	}

	static function question_signs($array){
		return implode(',', array_fill(0, count($array), '?'));
	}

	protected function q_fields($quote = false){
		// for mysql insert queries
		if ($quote) return $this->q().implode("{$this->q()},{$this->q()}", $this->fields).$this->q();
		return implode(",", $this->fields);
	}

	protected function q_update_fields(){
		$out = [];
		$i = 0;
		$values = $this->values(false);
		foreach ($this->fields as $field){
			switch (true){
				case $values[$i] === HQuery::Now:
					$sign = HQuery::Now;
					break;
				case $values[$i] === HQuery::Inc:
					$sign = "{$this->q()}{$field}{$this->q()} + 1";
					break;
				// TODO: update packed in mysql
				case in_array($field, $this->packed_fields) && DB_TYPE == DB_POSTGRE:
					$sign = "{$field} || ?";
					break;
				default:
					$sign = '?';

			}
			$i++;

			$out[] = "{$this->q()}{$field}{$this->q()} = {$sign}";
		}
		return implode(',', $out);
	}

	// insert only
	protected function q_question_signs(){
		$out = [];
		$values = $this->values(false);
		foreach ($values as $value){
			$out[] = $value === HQuery::Now ? HQuery::Now : '?';
		}
		return implode(',', $out);
	}

	protected function q_table(){
		return $this->table.($this->alias ? ' as '.$this->alias : '');
	}

	protected function q_join(){
		return implode(" ", $this->join);
	}

	protected function q_where(){
		if (count($this->where)) return " where ".implode(" and ", $this->where);
		return '';
	}

	protected function q_group(){
		if ($this->group) return " group by {$this->group} ";
		return '';
	}

	protected function q_order(){
		if ($this->order) return " order by {$this->order} ";
		return '';
	}

	protected function q_limit_offset(){
		if (!$this->limit) return '';

		if (DB_TYPE == DB_POSTGRE){
			$out = " limit {$this->limit} ";
			if ($this->offset) $out .= "offset {$this->offset} ";
		} else {
			$out = " limit ";
			if ($this->offset) $out .= $this->offset.',';
			$out .= $this->limit;
		}

		return $out;
	}

	protected function q_returning(){
		if ($this->returning && DB_TYPE == DB_POSTGRE) return " returning {$this->returning} ";
		return '';
	}

	// TODO: optimize
	function num_rows(){
		$fields = $this->fields;
		$this->fields = ['count(*)'];
		$count = $this->fetchColumn();
		$this->fields = $fields;
		return $count;
	}

	protected function query_select(){
		$sql = "select {$this->q_fields()} from {$this->q_table()} "
			.$this->q_join()
			.$this->q_where()
			.$this->q_group()
			.$this->q_order()
			.$this->q_limit_offset();

		return $sql;
	}

	protected function query_update(){
		$sql = "update {$this->table} set {$this->q_update_fields()} "
			.$this->q_where();

		return $sql;
	}

	protected function query_insert(){
		$sql = "insert into {$this->table} ({$this->q_fields(true)}) values ({$this->q_question_signs()})"
			.$this->q_returning();

		return $sql;
	}

	protected function query_delete(){
		$sql = "delete from {$this->table} "
			.$this->q_where();

		return $sql;
	}

	function sql(){
		switch ($this->type){
			case self::TypeSelect: return $this->query_select();
			case self::TypeUpdate: return $this->query_update();
			case self::TypeInsert: return $this->query_insert();
			case self::TypeDelete: return $this->query_delete();
			default: throw new HException('q-error');
		}
	}

	function values($exclude_constants = true){
		$values = array_merge($this->fields_values, $this->join_values, $this->where_values);
		// now() не работает в prepared statements (используем только в случае update и insert)
		if ($exclude_constants && in_array($this->type, [self::TypeInsert, self::TypeUpdate]))
			$values = array_values(array_diff($values, $this->constants));
		return $values;
	}

	function fetchAll($type = PDO::FETCH_ASSOC){
		if ($this->returning && DB_TYPE == DB_MYSQL) return [$this->fetch($type)];
		return $this->db()->fetchAll($this->sql(), $this->values(), $type);
	}

	function fetchAllColumns(){
		$rows = $this->fetchAll(PDO::FETCH_NUM);
		$out = [];
		foreach ($rows as $row) $out[] = $row[0];

		return $out;
	}

	function fetch($type = PDO::FETCH_ASSOC){
		if ($this->returning && DB_TYPE == DB_MYSQL){
			$this->ex();
			return [$this->returning => $this->db()->lastInsertId($this->returning)];
		}
		return $this->db()->fetch($this->sql(), $this->values(), $type);
	}

	function fetchColumn(){
		if ($this->returning && DB_TYPE == DB_MYSQL){
			$this->ex();
			return $this->db()->lastInsertId($this->returning);
		}
		return $this->db()->fetchColumn($this->sql(), $this->values());
	}

	function ex(){
		$this->db()->ex($this->sql(), $this->values());
	}

	function debug(){
		if ($this->parent()->router()->request() == HRouter::RequestApi){
			$out = [
				'error' => HELPER_DEBUG ? $this->sql() : 's-error',
				'error_code' => 's-error'
			];
			if (HELPER_DEBUG) $out['values'] = $this->values();
			$out = json_encode($out);
		} else {
			if (HELPER_DEBUG) $out = '<code>'.$this->sql().'<br/><br/>'.print_r($this->values(), true).'</code>';
			else $out = '<code>Query error</code>';
		}
		exit($out);
	}
}