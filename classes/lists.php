<?php

/**
 * @descr билдер (модель) для списков (frontend & api mode)
 * Class HLists
 */
class HLists extends HBase {
	function api(){
		return [
			'operator_chat',
			'user_chat',
			'forwarded',
			'finished',
			'all',
			'itemlist',
		];
	}

	function free_api(){
		return ['user_chat'];
	}

	function fieldExists($table, $field){
		return HQuery::c($this)
			->select('data_type')
			->from('information_schema.columns')
			->where_equal('table_name', $table)
			->where_equal('column_name', $field)
			->fetchColumn();
	}

	/**
	 * @descr separate search conditions for each row
	 * @param HModule[] $columns
	 * @param $table
	 * @param $params
	 * @param HQuery &$query
	 * @return string
	 */
	function rowSearchCondition($columns, $table, $params, &$query){
		$conditions = [];
		$values = [];

		for ($i = 0; $i < count($columns); $i++){
			if (!isset($params['columns'][$i + 1]['search']['value'])) continue;
			$this->searchColumn($columns[$i], $table, $params['columns'][$i + 1]['search']['value'], $conditions, $values);
		}

		if (count($conditions)) $query->where_and($conditions, $values);
	}

	/**
	 * @descr common search over all rows
	 * @param HModule[] $columns
	 * @param string $search
	 * @param string $table
	 * @param HQuery $query
	 * @return string
	 */
	function searchCondition($columns, $table, $search, &$query){
		$conditions = [];
		$values = [];

		foreach ($columns as $column){
			// OR condition and to boolean type casting - no checkboxes allowed (checkbox search only for rowSearchCondition)
			if (!$column->isField() || in_array($column->type(), [HNodeField::TypeCheckbox, HNodeField::TypeMultiSelect])) continue;
			$this->searchColumn($column, $table, $search, $conditions, $values);
		}
		if (count($conditions)) $query->where_or($conditions, $values);
	}

	/**
	 * @descr search logic
	 * @param HModule $column
	 * @param $table
	 * @param $search
	 * @param $conditions
	 * @param $values
	 */
	protected function searchColumn($column, $table, $search, &$conditions, &$values){
		// for checkboxes there are 3 options: "", "1", "0". first is ignored. multi will return array.
		if ($search === '' || (is_array($search) && empty($search))) return;



		// searching for
		switch ($column->type()){
			// very custom
			case HNodeField::TypeMultiSelect:
				$search = (int)$search;
				if (DB_TYPE == DB_POSTGRE){
					$conditions[] = "{$search} = any(('{' || {$table}.{$column->field()} || '}')::int[])";
				} else {
					$conditions[] = "find_in_set({$search}, {$table}.{$column->field()})";
				}
				return;	// !
			// custom
			case HNodeField::TypeCheckbox:
				if (DB_TYPE == DB_POSTGRE) $search = $search ? 'true' : 'false';	// "true" || "false"
				else $search = (int)(bool)$search; // 1 || 0
				break;
			// standart
			default:
				$search = str_replace('%', '\%', $search);
				$search = "%{$search}%";
		}

		$values[] = $search;

		// linked
		if ($column->isLinkedStandart()) $condition = $column->data('linked').$column->data('suffix').'.name';
		else $condition = $table.'.'.$column->field();

		// TODO: hstore / json search: SELECT * FROM nw_object_detail WHERE ((details like '%"screen_type":"%LED%"%'))
		// postgre: hstore->text
		if (DB_TYPE == DB_POSTGRE) $condition .= '::text ilike ?';
		else $condition .= ' like ?';

		// add to fields array
		$conditions[] = $condition;
	}

	/**
	 * @param HModule $module
	 * @param $params
	 * @param HQuery $query
	 */
	function missingLangCondition($module, $params, &$query){
		if (!$module->data('missing_lang')) return;
		// no filter is set
		if (!array_key_exists('missing_lang', $params) ||
			!in_array($params['missing_lang'], HLang::get_list())) return;

		if (DB_TYPE == DB_POSTGRE){
			$query->where("exist({$module->data('table')}.{$module->data('missing_lang')}, '{$params['missing_lang']}') = false or
				{$module->data('table')}.{$module->data('missing_lang')}->'{$params['missing_lang']}' = ''");
		} else {
			// TODO: mysql json search for empty value / no key

		}
	}

	/**
	 * @descr условие для неадмина - только свои записи. следует ставить в начало, т.к. модифицирует values
	 * @param HModule $module
	 * @param HQuery $query
	 * @return string
	 */
	function ownerCondition($module, &$query){
		if ($this->parent()->auth()->admin()) return;
		$user_id_field = $module->find(['type' => HNodeField::TypeUserId]);
		if (is_null($user_id_field)) return;

		$query->where_equal($module->table().'.'.$user_id_field->field(), $this->parent()->auth()->id());
	}

	/**
	 * @param HModule $module
	 * @param HQuery $query
	 * @param int $id
	 */
	function idCondition($module, &$query, $id){
		$query->where_equal($module->findByType(HNodeField::TypeId, true)->field(), $id);
	}

	/**
	 * @param HModule $module
	 * @param HQuery $query
	 * @param int $parent_id
	 */
	function parentCondition($module, &$query, $parent_id){
		$query->where_equal($module->findByType(HNodeField::TypeParentId, true)->field(), $parent_id);
	}

	/**
	 * @param HModule $module
	 * @param HQuery $query
	 */
	function moduleCondition($module, &$query){
		$query->where_equal($module->findByType(HNodeField::TypeModuleId, true)->field(), $module->id());
	}

	/**
	 * @param HModule $module
	 * @param HQuery $query
	 */
	function timeCondition($module, &$query){
		// not logically deleted
		if ($this->fieldExists($module->data('table'), 'start_time') &&
			$this->fieldExists($module->data('table'), 'end_time'))
			$query->where_not_deleted();
	}

	/**
	 * @param HModule $module
	 * @param HQuery $query
	 */
	function staticConditions($module, &$query){
		// extra static conditions
		if ($module->data('itemlist_conditions')){
			foreach ($module->data('itemlist_conditions') as $condition) $query->where($condition);
		}
	}

	/**
	 * @descr standart list method (for dataTables). api only.
	 * @param [] $params
	 * @return array
	 * @throws HException
	 * @throws Exception
	 */
	function itemlist($params){
		$this->parent()->manager()->setActionType(HQuery::TypeUpdate);

		// config array
		$array = $this->parent()->configuration()->module((int)$this->parent()->router()->data(2));
		if (!$array) throw new HException('module-not-found');

		// module (model)
		$module = HModule::c($array);
		if (!$module->canRead()) throw new HException('permission-denied');

		// adapt vars
		$offset = $params['start'];
		$limit = $params['length'];
		$order = $params['order'][0]['column'] - 1;	// fix for checkboxes column
		$order_direction = $params['order'][0]['dir'];
		if ($order_direction != 'desc' && $order_direction) $order_direction = 'asc';
		$search = $params['search']['value'];
		$echo = $params['draw'];
		// hack for HOrder
		$order_mode = isset($params['order_mode']) && $params['order_mode'];
		if ($order_mode) $module->data('editable_list', 0);

		// result
		$out = [
			"sEcho" => $echo,
			"aaData" => []
		];

		$columns = $module->columns();
		$table = $module->table();

//		print_r($columns);
		$index = 1;

		$fields_array = [];
		foreach ($columns as $column){
			if (!$column->isField()) continue;
		//	$fields_array[] = $column->itemlist_field($table);
			if ($column->data('as')) $fields_array[] = $column->request_field_as($table, $column->data('as'));
			else $fields_array[] = $column->request_field($table);
			if ($column->linked()){
				$column->data('suffix', $index++);
				$fields_array[] = $column->request_field_linked('_linked', $column->data('suffix'));
			}
		}

		// building query
		$query = HQuery::c($this)
			->select($fields_array)
			->from($table);

//		$query->debug();

		// join
		foreach ($columns as $column){
			if ($column->isLinkedStandart()){
				$query->join_using($column->data('linked'), $column->field(), HQuery::LeftOuterJoin, $column->data('linked_field'), $column->data('suffix'));
			}
		}

		// condition for absent language
		$this->missingLangCondition($module, $params, $query);
		// search conditions
		$this->rowSearchCondition($columns, $table, $params, $query);
		$this->searchCondition($columns, $table, $search, $query);
		// owner condition for non-admins
		$this->ownerCondition($module, $query);
		// not logically deleted
		$this->timeCondition($module, $query);
		// extra static conditions
		$this->staticConditions($module, $query);

//		$query->debug();
		if (!$order_mode) $out['iTotalDisplayRecords'] = $query->num_rows();

		// order
		if ($order_mode){
			$query->order("{$table}.orderby asc, {$table}.{$module->field_id()} desc");
		} elseif (isset($columns[$order])){
			$query->order($table.'.'.$columns[$order]->field(), $order_direction);
		}

		// limit & offset
		$query->limit($limit)->offset($offset);

//		$query->debug();

		// sql select
		$rows = $query->fetchAll();

		foreach ($rows as &$row){
			// storing int id value
			$row_id = $row[$module->field_id()];
			// check if row enabled, or not by its column values
			$row_enabled = $this->parent()->manager()->checkRowEnabled($module, $row);
			// post processing
			$this->parent()->manager()->unpackLocale($columns, $row);
			// creating view array from values array
			$row = $this->rowListView($module, $columns, $row, $row_enabled);
			// assoc->num (after post processing! only for dataTables.js)
			$row = array_values($row);
			// null values look bad in dataTables.js
			foreach ($row as &$v){
				if (is_null($v)) $v = '';
			}
			// add extra info for editable content
			//$module->isEditableList();

			// last TD in row is shown when not in order mode. in order mode dataTables is also disabled.
			if (!$order_mode){
				array_unshift($row, HNodeItemlist::firstTd());

				$actions = [];
				$actions[] = HNodeItemlist::ActionEdit;
				if ($module->isEditableList()) $actions[] = HNodeItemlist::ActionSave;
				if ($module->canCreate()) $actions[] = HNodeItemlist::ActionClone;
				if ($module->canDelete()) $actions[] = HNodeItemlist::ActionDelete;

				$row[] = HNodeItemlist::lastTd($module, $actions, $row_id);
			}
		}
		$out['aaData'] = $rows;

		return $out;
	}


	/**
	 * @param HModule $module
	 * @param HModule[] $columns
	 * @param array $row
	 * @param bool $row_enabled
	 * @return array
	 */
	function rowListView($module, $columns, $row, $row_enabled){
		$out = [];

		// creating view for all columns in one row
		foreach ($columns as $column){
			// non-custom module - standart field
			if ($column->isField()){
				$field = $column->data('as') ?: $column->field();
				if (array_key_exists($field, $row)){
					// create view
					$node = $this->parent()->manager()->nodeField($module->id(),
						$row[$module->findByType(HNodeField::TypeId)->field()], $column, $row);
					// different behavior
					if ($module->isEditableList()){
						// manage in list
						if (!$this->parent()->manager()->checkCellEnabled($column, $row_enabled)) $node->disable();
						$out[$field] = $node->show_manage_itemlist();
					} else {
						// passive in list
						$out[$field] = $node->show_itemlist();
					}
				} else {
					$out[$field] = '&nbsp;';
				}
			} else {
				$column->setDelegate($this);
				$node = $column->tableCell($row[$module->field_id()]);
				$out[] = $node->show();
			}
		}

		return $out;
	}

	/*
	 * custom lists
	 */

	function user_chat($params){
		return $this->_chat($params, HLists::ModeUser);
	}

	function operator_chat($params){
		return $this->_chat($params, HLists::ModeOperator);
	}

	const ModeUser = 1;
	const ModeOperator = 2;

	function _chat($params, $mode){
		$this->parent()->check()->numeric(['chat_id'], $params);
		if ($mode == HLists::ModeUser) $params['user_mode'] = 1;
		// collect chat data
		$chat_data = $this->parent()->chat()->_data($params);
		if (!$chat_data) throw new HException('chat-not-found');
		// collect actual messages
		$list = $this->parent()->message()->_list($params);
		// сортировка в списке обратная
		$list = array_reverse($list);
		// обновление статуса сообщений на полученные и сбор непросмотренных сообщений
		if ($mode == HLists::ModeOperator){
			$this->parent()->message()->_operator_delivered($list);
			// префикс current_ необходим, чтобы не переписалось поле operator_id, полученное из базы
			$chat_data['current_operator_id'] = $this->parent()->auth()->id();
			$chat_data['admin'] = $this->parent()->auth()->admin();
			$chat_data['answers'] = HQuery::c($this)
				->select('*')
				->from('helper_fast_answers')
				->where_not_deleted()
				->order('use_count desc')
				->fetchAll();
		} else {
			$this->parent()->message()->_user_delivered($list);
		}
		// создание элемента чата
		$chat = new HNodeChat(0, '', $chat_data);
		// все необходимые данные нужно передать в Node, так как из Node в контроллер запрос невозможен
		$chat->set_id_field('message_id');
		$chat->set_list($list);
		return $this->out($chat);
	}

	// список завершенных чатов
	function finished($params = []){
		return $this->_chats($params, ['status_id' => HStatus::StatusFinished], __FUNCTION__);
	}

	// список перенаправленных чатов
	function forwarded($params = []){
		return $this->_chats($params, ['forwarded' => 1], __FUNCTION__);
	}

	// список всех чатов (для админа)
	function all($params = []){
		return $this->_chats($params, ['all' => 1], __FUNCTION__);
	}

	function _chats($params, $conditions, $type){
		$conditions['limit'] = HELPER_CHATLIST_COUNT;
		$list = $this->parent()->chat()->_list($params, $conditions);

		$chats = new HNodeChats(0, w($type.'-chats'), ['method' => $type]);
		$chats->set_id_field('chat_id');
		$chats->set_list($list);
		return $this->out($chats);
	}

	/**
	 * @param HListnode $obj
	 * @return array|HNode
	 */
	function out($obj){
		// для фронтэнда просто возращаем узел чата, чтобы использовать его в дереве
		if ($this->parent()->router()->request() == HRouter::RequestFrontend) return $obj;
		// объект с данными для работы из api
		return $obj->api_data();
	}
}