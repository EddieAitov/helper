<?php
class HMessage extends HBase {
	const api = 1;

	function free_api(){
		return ['send', 'user_viewed'];
	}

	function operator_viewed($params){
		$this->parent()->check()->exist(['message_ids'], $params);
		$params['message_ids'] = json_decode($params['message_ids'], true);
		$this->parent()->check()->is_array(['message_ids'], $params);
		$this->parent()->chat()->check_for_operator($params);

		HQuery::c($this)
			->update('helper_messages')
			->set('viewed', true)
			->where_in('message_id', $params['message_ids'])
			->where_equal('chat_id', $params['chat_id'])
			->ex();
	}

	function user_viewed($params){
		$this->parent()->check()->exist(['message_ids'], $params);
		$params['message_ids'] = json_decode($params['message_ids'], true);
		$this->parent()->check()->is_array(['message_ids'], $params);
		$this->parent()->chat()->check_for_user($params);

		HQuery::c($this)
			->update('helper_messages')
			->set('viewed', true)
			->where_in('message_id', $params['message_ids'])
			->where_not_null('operator_id')
			->where_equal('chat_id', $params['chat_id'])
			->ex();
	}

	function _operator_delivered($message_list){
		foreach ($message_list as $message){
			if (!$message['operator_id'] && !$message['delivered']){
				HQuery::c($this)
					->update('helper_messages')
					->set('delivered', true)
					->where_equal('message_id', $message['message_id'])
					->ex();
			}
		}
	}

	function _user_delivered($message_list){
		foreach ($message_list as $message){
			if ($message['operator_id'] && !$message['delivered']){
				HQuery::c($this)
					->update('helper_messages')
					->set('delivered', true)
					->where_equal('message_id', $message['message_id'])
					->ex();
			}
		}
	}

	// отправка сообщения пользователем
	function send($params){
		$this->parent()->check()->exist(['text'], $params);
		$params['user_mode'] = 1;

		// создание чата, если не передан chat_id
		if (!isset($params['chat_id']) || !$params['chat_id']){
			$chat_id = $this->parent()->chat()->create($params)['chat_id'];
		// проверка чата
		} else {
			// обновление данных о чате, если чат найден
			if ($this->parent()->chat()->check($params)){
				$chat_id = $params['chat_id'];
				$this->parent()->chat()->update($params);
			// создание нового чата, если указанный чат не найден
			} else {
				$chat_id = $this->parent()->chat()->create($params)['chat_id'];
			}
		}

		$message_id = HQuery::c($this)
			->insert_into('helper_messages')
			->set('chat_id', $chat_id)
			->set('text', $params['text'])
			->set('start_time', HQuery::Now)
			->returning('message_id')
			->fetchColumn();

		$this->_link_files($chat_id, $message_id, isset($params['file_ids']) ? $params['file_ids'] : []);

		return [
			'chat_id' => (int)$chat_id,
			'message_id' => $message_id
		];
	}

	// отправка сообщения оператором
	function operator_send($params){
		$this->parent()->check()->numeric(['chat_id'], $params);
		$this->parent()->check()->exist(['text'], $params);

		$chat_operator_id = HQuery::c($this)
			->select('operator_id')
			->from('helper_chats')
			->where_equal('chat_id', $params['chat_id'])
			->fetchColumn();

		// проверка, если уже задан оператор
		if ($chat_operator_id){
			if ($chat_operator_id != $this->parent()->auth()->id()) throw new Exception('user-is-taken-by-another-operator');
		// установка оператора для чата
		} else {
			$this->parent()->chat()->set_operator($params);
		}

		$out = HQuery::c($this)
			->insert_into('helper_messages')
			->set('operator_id', $this->parent()->auth()->id())
			->set('text', $params['text'])
			->set('chat_id', $params['chat_id'])
			->set('start_time', HQuery::Now)
			->returning('message_id')
			->fetch();

		$this->_link_files($params['chat_id'], $out['message_id'], isset($params['file_ids']) ? $params['file_ids'] : []);

		return $out;
	}

	// линковка загруженных файлов
	function _link_files($chat_id, $message_id, $file_ids){
		foreach ($file_ids as $file_id){
			$this->parent()->file()->_link($file_id, $chat_id, $message_id);
		}
	}

	function operator_send_email($params){
		// получаем e-mail адрес из чата
		$chat_data = $this->parent()->chat()->_data($params);
		// на всякий случай, проверка e-mail
		if (!$chat_data['user_email']) throw new Exception('email-is-set-incorrect');
		// стандартная отправка сообщения
		$message_id = $this->operator_send($params)['message_id'];

		// апдейт поля e-mail в сообщении, чтобы осталась отметка о том, что оно было отправлено на e-mail
		HQuery::c($this)
			->update('helper_messages')
			->set('user_email', $chat_data['user_email'])
			->where_equal('message_id', $message_id)
			->ex();

		// отправка e-mail пользователю
		$subject = w('helper-email-subject');
		$body = str_replace([
				'%user_name%',
				'%operator_name%',
				'%text%'
			], [
				htmlspecialchars($chat_data['user_name']),
				$this->parent()->auth()->data('first_name'),
				// текст пользователя
				htmlspecialchars($params['text'])
			],
			w('helper-email-body')
		);

		// поиск файлов, прикрепленных к сообщению
		$files = $this->parent()->file()->_list(['message_id' => $message_id]);
		$body .= HNodeChat::standart()->files($files);

		$this->parent()->model()->mail($chat_data['user_email'], $subject, $body);
	}

	function _list($params, $conditions = []){
		$this->parent()->check()->numeric(['chat_id'], $params);

		$query = new HQuery($this->parent());
		$query
			->select([
				'h_m.*',
				'h_c.user_name',
				'h_o.first_name as operator_name'
			])
			->select_time('h_m.start_time', HQuery::FormatDateTime, 'date')
			->from('helper_messages', 'h_m')
			->join('inner join helper_chats as h_c on h_c.chat_id = h_m.chat_id')
			// требуется для вывода имени оператора юзеру
			->join('left outer join helper_operators as h_o on h_o.operator_id = h_m.operator_id')
			->where_equal('h_m.chat_id', $params['chat_id'])
			->order('h_m.message_id desc');
		if (!isset($conditions['no_limit'])) $query->limit(HELPER_MESSAGES_COUNT);

		// посетитель
		if (isset($params['user_mode']) && $params['user_mode']){
		// оператор
		} else {
			// текущий владелец чата
			$operator_id = HQuery::c($this)->select('h_c.operator_id')->from('helper_chats', 'h_c')
				->where_equal('h_c.chat_id', $params['chat_id'])
				->fetchColumn();

			// владелец чата другой оператор, текущий пользователь не админ
			if (!$this->parent()->auth()->admin() && $operator_id != $this->parent()->auth()->id()){
				// проверим, не было ли форварда данного чата от текущего оператора
				$forward_time = HQuery::c($this)->select('h_f.time')->from('helper_forwards', 'h_f')
					->where_equal('from_operator_id', $this->parent()->auth()->id())
					->where_equal('chat_id', $params['chat_id'])
					->order('h_f.time desc')
					->fetchColumn();

				// чат был отфорваржен - добавляем вывод сообщений
				if ($forward_time){
					$query->where('h_m.start_time < ?', $forward_time);
				} else {
					$query->where_null_or_equal('h_c.operator_id', $this->parent()->auth()->id());
				}
			}
		}

		// передан может быть или last_id или first_id (если оба, берем только last_id, условие сразу для обоих вернет 0 строк)
		if (isset($params['last_id']) && is_numeric($params['last_id'])){
			$query->where('message_id > ?', $params['last_id']);
		} elseif (isset($params['first_id']) && is_numeric($params['first_id'])){
			$query->where('message_id < ?', $params['first_id']);
		}

//		$query->debug();
		// список сообщений
		$data = $query->fetchAll();

		// добавим файлы
		foreach ($data as &$node){
			$node['files'] = $this->parent()->file()->_list($node);
		}

		return $data;
	}
}