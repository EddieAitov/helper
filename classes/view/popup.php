<?php
class HViewPopup extends HBase {
	const api = 1;

	function change_operator(){
		$out = [
			'title' => w('change-operator-for-current-chat'),
			'callback' => 'jhelper.chat.change_operator'
		];
		ob_start(); ?>
		<div class="form-group">
			<label for="new_operator_id"><?php echo w('choose-operator')?>:</label>
			<select class="form-control" id="new_operator_id">
				<option value="0">-</option>
				<?php
				$operators = HQuery::c($this)
					->select(['operator_id', 'first_name', 'last_name'])
					->select_active('activity_time', 'active')
					->from('helper_operators')
					->where_not_equal('operator_id', $this->parent()->auth()->id())
					->fetchAll();

				foreach ($operators as $operator){
					$attrs = ['value' => $operator['operator_id']];
					if (!$operator['active']) $attrs['disabled'] = 'disabled';
					$name = $operator['operator_id'].'). '.$operator['first_name'].' '.$operator['last_name'].' '
						.($operator['active'] ? '[online]' : '[offline]');

					echo HNode::standart('option', $attrs)->addText($name)->show();
				}
				?>
			</select>
		</div>
		<?php
		$out['text'] = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	function change_status($params){
		$out = [
			'title' => w('change-status-for-current-chat'),
			'callback' => 'jhelper.chat.change_status'
		];
		$data = $this->parent()->chat()->_data(['chat_id' => $params['chat_id']]);
		if (!$data) throw new HException('chat-not-found');
		ob_start(); ?>
		<div class="form-group">
			<label for="new_status_id"><?php echo w('choose-status')?>:</label>
			<select class="form-control" id="new_status_id">
				<option value="0">-</option>
				<?php
				$statuses = HStatus::get_list();

				foreach ($statuses as $status){
					$attrs = ['value' => $status];
					if ($data['status_id'] == $status) $attrs['selected'] = 'selected';
					echo HNode::standart('option', $attrs)->addText($status.'). '.w('chat-status-'.$status))->show();
				}
				?>
			</select>
		</div>
		<?php
		$out['text'] = ob_get_contents();
		ob_end_clean();

		return $out;
	}
}