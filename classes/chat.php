<?php
class HChat extends HBase {
	function api(){
		return ['activity', 'set_operator', 'change_operator', 'change_status', 'send_history'];
	}

	function free_api(){
		return ['activity', 'send_history'];
	}

	const MessengerFeedback = 1;// [off]
	const MessengerChat = 2; 	// [on][off] default
	const MessengerMobile = 3;	// [on]

	// отправка истории сообщений пользователю на e-mail
	function send_history($params){
		$this->check_for_user($params);

		// попытаемся получить e-mail из чата, если он не был передан
		if (!isset($params['user_email']) || !$params['user_email']){
			$params['user_email'] = HQuery::c($this)->select('user_email')
				->from('helper_chats')
				->where_equal('chat_id', $params['chat_id'])->fetchColumn();
			if (!$params['user_email']) throw new Exception('email-is-not-set');
		}

		// тема письма
		$subject = w('mail-subject-message-history');
		$chat_node = new HNodeChat();
		$chat_node->set_id_field('message_id');
		$messages = $this->parent()->message()->_list([
			'user_mode' => 1,
			'chat_id' => $params['chat_id']
		], ['no_limit' => 1]);
		$messages = array_reverse($messages);
		$chat_node->set_list($messages);

		$body = $chat_node->show_list(false);

		$params['success'] = $this->parent()->model()->mail($params['user_email'], $subject, $body);

		// логируем
		$this->parent()->log()->insert(HLog::TypeSendConversation, $params);

		return $params['success'];
	}

	// функция резервации чата за оператором
	function set_operator($params){
		$this->parent()->check()->numeric(['chat_id'], $params);
		(new HQuery($this->parent()))
			->update('helper_chats')
			->set('operator_id', $this->parent()->auth()->id())
			->set('status_id', HStatus::StatusActive)
			->where_equal('chat_id', $params['chat_id'])
			->where_null('operator_id')
			->ex();
	}

	// функция смены оператора доступная владельцу чата
	function change_operator($params){
		$this->parent()->check()->numeric(['chat_id', 'operator_id'], $params);
		if (!$this->check_for_operator($params)) return 0;

		HQuery::c($this)
			->update('helper_chats')
			->set('operator_id', $params['operator_id'])
			->where_equal('operator_id', $this->parent()->auth()->id())
			->where_equal('chat_id', $params['chat_id'])
			->ex();

		// логируем
		HQuery::c($this)
			->insert_into('helper_forwards')
			->set('chat_id', $params['chat_id'])
			->set('from_operator_id', $this->parent()->auth()->id())
			->set('operator_id', $params['operator_id'])
			->set('time', HQuery::Now)
			->ex();

		return 1;
	}

	function change_status($params){
		$this->parent()->check()->in_array(['status_id'], $params, HStatus::get_list());
		if (!$this->check_for_operator($params)) return 0;

		HQuery::c($this)
			->update('helper_chats')
			->set('status_id', $params['status_id'])
			->where_equal('chat_id', $params['chat_id'])
			->where_equal('operator_id', $this->parent()->auth()->id())
			->ex();
		return 1;
	}

	// пользователь проявил активность в чате
	function activity($params){
		// некорректный чат - молча выходим
		if (!$this->check_for_user($params)) return 0;

		(new HQuery($this->parent()))
			->update('helper_chats')
			->set('activity_time', HQuery::Now)
			->where_equal('chat_id', $params['chat_id'])
			->ex();

		return 1;
	}

	/**
	 * @descr подготовка данных пользователя для внесения в базу
	 * @param HQuery $query
	 * @param $params
	 */
	protected function prepare_data($query, $params){
		$query
			->set('ip', $_SERVER['REMOTE_ADDR'])
			->set('session_id', session_id());

		// опциональный параметр пользовательский unid, генерируемый на клиенте
		if (isset($params['user_unid'])) $query->set('user_unid', $params['user_unid']);
		if (isset($params['user_name'])) $query->set('user_name', $params['user_name']);
		if (isset($params['user_email'])) $query->set('user_email', $params['user_email']);
		if (isset($params['user_phone'])) $query->set('user_phone', $params['user_phone']);
		// тип чата
		if (isset($params['type_id']) && is_numeric($params['type_id'])) $query->set('type_id', $params['type_id']);
	}

	// создание нового чата
	function create($params){
		// допустимо начинать чат без имени
//		$this->parent()->check()->is_not_empty(['user_name'], $params, 'please-introduce');
		$this->parent()->check()->in_array(['messenger_type_id'], $params, [1, 2, 3], 'incorrect-messenger-type');

	$query = new HQuery($this->parent());
		$this->prepare_data($query, $params);
		$out['chat_id'] = $query
			->insert_into('helper_chats')
			// в зависимости от этого типа определяем возможность отправки сообщения в on и off
			->set('messenger_type_id', $params['messenger_type_id'])
			->set('start_time', HQuery::Now)
			->set('activity_time', HQuery::Now)
//			->set('status_id', HStatus::StatusActive)	// null->active в set_operator, finished->active в update
			->returning('chat_id')
			->fetchColumn();

		return $out;
	}

	// обновление контактных данных пользователя при его активности
	function update($params){
		if (!$this->check_for_user($params)) return;

		$query = new HQuery($this->parent());
		$this->prepare_data($query, $params);

		// если был установлен статус "завершен" для разговора, открываем его снова
		if (HQuery::c($this)
			->select('status_id')
			->from('helper_chats')
			->where_equal('chat_id', $params['chat_id'])
			->fetchColumn() == HStatus::StatusFinished)
			$query->set('status_id', HStatus::StatusActive);

		$query
			->update('helper_chats')
			->set('chat_id', $params['chat_id'])
			->set('activity_time', HQuery::Now)
			->where_equal('chat_id', $params['chat_id'])
			->ex();
	}

	private $checked_id = 0;

	// проверка указанного чата (что он принадлежит текущему пользователю)
	function check_for_user($params){
		$this->parent()->check()->numeric(['chat_id'], $params);
		if ($this->checked_id == $params['chat_id']) return true;

		$query = new HQuery($this->parent());
		$query->select(1)
			->from('helper_chats')
			->where_equal('chat_id', $params['chat_id']);

		// передан уникальный идентификатор пользователя
		if (isset($params['user_unid']) && $params['user_unid']){
			$query->where_equal('user_unid', $params['user_unid']);
			// проверка по стандартному session_id
		} else {
			$query->where_equal('session_id', session_id());
		}

		$ex = $query->fetchColumn();

		// cache
		if ($ex) $this->checked_id = $params['chat_id'];

		return $ex ? 1 : 0;
	}

	// проверка доступности чата для оператора
	function check_for_operator($params){
		$this->parent()->check()->numeric(['chat_id'], $params);
		if ($this->checked_id == $params['chat_id']) return true;

		$ex = $this->parent()->auth()->admin() ||
			(new HQuery($this->parent()))
			->select(1)
			->from('helper_chats')
			->where_equal('chat_id', $params['chat_id'])
			->where_null_or_equal('operator_id', $this->parent()->auth()->id())
			->fetchColumn();

		// cache
		if ($ex) $this->checked_id = $params['chat_id'];

		return $ex ? 1 : 0;
	}

	// универсальная проверка доступности чата
	function check($params){
		if (isset($params['user_mode']) && $params['user_mode']) return $this->check_for_user($params);
		return $this->check_for_operator($params);
	}

	// вывод списка чатов для оператора
	function _list($params = [], $conditions = []){
		$query = new HQuery($this->parent());

		$query
			->select([
				'h_c.chat_id',
				'h_c.messenger_type_id',
				'h_c.user_name',
				'h_c.user_email',
				'h_c.user_phone',
				'h_c.operator_id',
				'h_c.ip',
				'h_c.status_id'
			])
			->select_time('h_c.start_time', HQuery::FormatDateTime, 'start')
			->select_time('h_c.activity_time', HQuery::FormatDateTime, 'activity')
			->select_time('h_c.activity_time', HQuery::FormatDate, 'date')
			->select_time('h_c.activity_time', HQuery::FormatTime, 'time')
			->select_today('h_c.activity_time', 'today')
			->from('helper_chats', 'h_c')
			->where_not_deleted()
	//		->group('h_c.chat_id')
			->order('h_c.chat_id desc');

		// требуется загрузка сообщений для посетителя
		if (isset($params['user_mode']) && $params['user_mode']){
			$this->parent()->check()->numeric(['chat_id'], $params);
			$query
				->select_active('h_o.activity_time', 'active', 'h_o.operator_id is not null')
				->select('h_o.first_name')
				->join('left outer join helper_operators as h_o on h_o.operator_id = h_c.operator_id')
				->where_equal('chat_id', $params['chat_id']);

			if (isset($params['user_unid']) && $params['user_unid']){
				$query->where_equal('h_c.user_unid', $params['user_unid']);
			} else {
				$query->where_equal('h_c.session_id', session_id());
			}
		// требуется загрузка сообщений для оператора
		} else {
			// активность пользователя
			$query->select_active('h_c.activity_time', 'active', 'h_c.messenger_type_id != 1');
			// дополнительные условия для оператора (если не запрошен "админский" список)
			if (!isset($conditions['all']) || !$this->parent()->auth()->admin()){
				$query
					// оператор должен состоять в группе
					->join('inner join helper_groups as h_g on h_g.group_id = ?', $this->parent()->auth()->data('group_id', 0))
					// к просмотру доступны либо чаты без типа, либо чаты с допустимым для оператора типом
					->where_null_or_any('h_c.type_id', 'h_g.chat_type_ids');

				// запрос на вывод перенаправленных чатов (только)
				if (isset($conditions['forwarded'])){
					$query->join('inner join helper_forwards as h_f on h_f.chat_id = h_c.chat_id and h_f.from_operator_id = ?',
							$this->parent()->auth()->id());
				} else {
					// иначе: не занятые другим оператором
					$query->where_null_or_equal('h_c.operator_id', $this->parent()->auth()->id());
				}
			}

			// при операторском режиме возможна подгрузка списка чатов без передачи chat_id
			if (isset($params['chat_id'])) $query->where_equal('h_c.chat_id', $params['chat_id']);
		}

		// за последние N дней
		if (isset($conditions['not_older'])){
			$query->where_not_older('h_c.activity_time', $conditions['not_older']);
		}

		// указано количество записей за одну выгрузку
		if (isset($conditions['limit'])){
			$query->limit($conditions['limit']);
		}

		// вывод с определенным статусом
		if (isset($conditions['status_id'])){
			$query->where_equal('status_id', $conditions['status_id']);
		}

		// активные и новые
		if (isset($conditions['active_and_new'])){
			$query->where_null_or_equal('status_id', HStatus::StatusActive);
		}

		// first or last id
		if (isset($params['first_id']) && $params['first_id'] > 0){
			$query->where('chat_id < ?', $params['first_id']);
		} elseif (isset($params['last_id']) && $params['last_id'] > 0){
			$query->where('chat_id > ?', $params['last_id']);
		}

//		$query->debug();
		$data = $query->fetchAll();
//		if (count($data)) throw new HException(['error'=>print_r($data[0],1)]);

		return $data;
	}

	/**
	 * @descr информация об указанном чате
	 * @param $params
	 * @return bool|array
	 */
	function _data($params){
		$this->parent()->check()->numeric(['chat_id'], $params);
		$data = $this->_list($params, ['all' => 1]);
		if (!count($data)){
			// поищем среди форвардов - они также доступны в контекте метода _data
			$data = $this->_list($params, ['forwarded' => 1]);
			if (!count($data)) return false;
		}

		$row = $data[0];

		// для передачи в HNodeChat как аттрибута
		if (isset($params['user_mode'])) $row['user_mode'] = $params['user_mode'];
		return $row;
	}
}