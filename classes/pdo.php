<?php
// wrapper for PDO
class HPDO extends PDO {
	protected function st($sql, $values){
		$st = $this->prepare($sql);
		$st->execute($values);
		return $st;
	}

	function fetch($sql, $values = [], $type = PDO::FETCH_ASSOC){
		return $this->st($sql, $values)->fetch($type);
	}

	function fetchAll($sql, $values = [], $type = PDO::FETCH_ASSOC){
		return $this->st($sql, $values)->fetchAll($type);
	}

	function fetchColumn($sql, $values = []){
		return $this->st($sql, $values)->fetchColumn();
	}

	function ex($sql, $values){
		$this->st($sql, $values);
	}

	function unpack($string){
		if (DB_TYPE == DB_POSTGRE){
			$rows = $this->fetchAll("select (each(h)).key, (each(h)).value from (select ?::hstore as h) as s", [$string], PDO::FETCH_KEY_PAIR);
		} else {
			$rows = json_decode($string, true);
		}
		return $rows;
	}

	function pack($array){
		if (DB_TYPE == DB_POSTGRE){
			$pg_hstore = array();

			foreach ($array as $key => $val) {
				$search = array('"');
				$replace = array('\"');

				$key = str_replace($search, $replace, $key);
				$val = $val === NULL
					? 'NULL'
					: '"' . str_replace($search, $replace, $val) . '"';

				$pg_hstore[] = sprintf('"%s"=>%s', $key, $val);
			}

			$data = implode(',', $pg_hstore);
		} else {
			$data = json_encode($array);
		}

		return $data;
	}

	function isInt($type){	// TODO: mysql types
		return in_array($type, ['integer', 'smallint', 'bigint']);
	}

	function isFloat($type){
		return in_array($type, ['numeric', 'double precision']);
	}
}