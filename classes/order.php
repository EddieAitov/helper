<?php
/*
 * list ordering api
 */
class HOrder extends HBase {
	const api = 1;

	function get(){
		$rows = $this->parent()->lists()->itemlist([
			'start' => 0,
			'length' => 1000,
			'order' => [['column' => 0, 'dir' => 0]],
			'search' => ['value' => ''],
			'order_mode' => 1,
			'draw' => 0
		])['aaData'];

		$module = $this->parent()->manager()->getModule((int)$this->parent()->router()->data(2));
		$node = new HNodeOrder($module, $rows);

		return $node->show();
	}

	function save($params){
		if (!isset($params['ids']) || !is_array($params['ids'])) return;

		$module = $this->parent()->manager()->getModule((int)$this->parent()->router()->data(2));
		$orderby = 0;
		foreach ($params['ids'] as $id){
			HQuery::c($this)
				->update($module->table())
				->set(HNodeField::TypeOrderby, $orderby++)
				->where_equal($module->field_id(), $id)
				->ex();
		}
	}
}