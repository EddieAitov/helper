<?php
class HLang extends HBase {
	const LanguageRussian	= 'ru';
	const LanguageEnglish	= 'en';
	const LanguageSpanish	= 'es';
	const LanguageGerman	= 'de';
	const LanguageItalian	= 'it';
	const LanguageFrench	= 'fr';

	static function get_list(){
		return ['en', 'ru', 'es', 'de', 'it', 'fr'];
	}

	static function helper_language(){
		return HLang::LanguageRussian;
	}
}