<?php
// helper only required files
// db & site config
require_once(__DIR__ . '/../config.php');
// custom exception class
require_once(__DIR__.'/exception.php');
// base class for all dynamic included classes
require_once(__DIR__.'/base.php');
// main model class
require_once(__DIR__.'/model.php');
// locale function
require_once(__DIR__.'/locale.php');
// pdo wrapper
require_once(__DIR__.'/pdo.php');

class HController {
	/** @var HPDO */
	private $db;
	/** @var bool */
	private $error;

	// инициализация контроллера
	function __construct(){
		try {
			$this->model = new HModel($this);
			$this->model()->headers();
			$this->model()->autoload();
			$this->model()->define_language();
		} catch (HException $e){
			$this->error = $e->message()['error'];
		} catch (PDOException $e){
			$this->error = 's-error';
		}
	}

	function language(){
		return $this->auth()->language();
	}

	// запуск обработки запроса
	function process(){
		switch ($this->router()->request()){
			case HRouter::RequestApi:
				$out = $this->api();
				break;
			case HRouter::RequestFrontend:
				$out = $this->frontend();
				$this->model()->clear_disposable_session();
				break;
			default:
				$out = null;
		}

		return $out;
	}

	function api(){
		try {
			// до основной загрузки возникла ошибка
			if (!is_null($this->error)) throw new HException($this->error);

			ob_start();
			// получение объекта для вызова
			list($object, $method) = $this->model()->api_object();
			// вызов
			$out = $object->$method($_POST);
			// подхватываем вывод - если не пустой, значит была ошибка
			$output = ob_get_contents();
			ob_end_clean();
			if ($output) throw new HException($output);
		} catch (HException $e){
			$out = $e->message();
		} catch (PDOException $e){
			$out = HELPER_DEBUG ?
				['error' => $e->getMessage(), 'error_code' => 's-error'] :
				['error' => w('s-error'), 'error_code' => 's-error'];
		}

		if (is_null($out)) $out = [];
		if (!is_array($out)) $out = ['out' => $out];

		return json_encode($out);
	}

	function frontend(){
		try {
			// до основной загрузки возникла ошибка
			if (!is_null($this->error)) throw new HException($this->error);

			// пользователь авторизован
			if ($this->auth()->check()){
				// каркас стандартной страницы (верх, левое меню, контент)
				$this->builder()->page_standart();
				switch ($this->router()->data(0)){
					/* STANDART ACTIONS */
					case 'manage':
						/** @var HNodeForm $node */
						$node = $this->manager()->create();
						// node -> DOM
						$this->builder()->append($node);
						// title
						$this->builder()->set_title($node->title());
						break;
					// user's profile
					case 'profile':
						$node = $this->manager()->create_profile();
						$this->builder()->append($node);
						$this->builder()->set_title($node->title());
						break;
					case 'itemlist':
						// array (data)
						$array = $this->configuration()->module((int)$this->router()->data(1));
						if (is_null($array)) throw new HException('module-not-found');
						// module (model from data)
						$module = HModule::c($array);
						$this->manager()->prepareModuleFields($module);
						// node (view from model) - only list wrapper without data
						$itemlist_node = new HNodeItemlist($module);
						$itemlist_node->attr('order_enabled', $this->lists()->fieldExists($module->table(), HNodeField::TypeOrderby));
						// node -> DOM
						$this->builder()->append($itemlist_node);
						// title
						$this->builder()->set_title($module->name());
						break;
					/* CUSTOM RESERVED ALIASES */
					case 'chat':
						/** @var HNodeChat $chat */
						$chat = $this->lists()->operator_chat(['chat_id' => $this->router()->data(1)]);
						// добавляем в стандартное дерево
						$this->builder()->append($chat);
						// page title
						$this->builder()->set_title($chat->name());
						break;
					case 'finished':
					case 'forwarded':
					case 'all':
						$chats = $this->lists()->{$this->router()->data(0)}();
						$this->builder()->append($chats);
						$this->builder()->set_title(w($this->router()->data(0).'-chats'));
						break;
					/* MAIN PAGE (by default) */
					default:
						$this->builder()->set_title('site.helper');
						$this->builder()->append(HNode::text('&nbsp;'));
				}
			// пользователь не авторизован
			} else {
				$this->builder()->page_authorization();
			}
		// при исключении строится страница для исключения
		} catch (HException $e){
			$this->builder()->page_exception($e->message()['error']);
		}
		// визуальное отображение дерева
		return $this->builder()->root()->show();
	}

	/* getters */

	function dynamic_module($module){
		if (!isset($this->$module) || is_null($this->$module)){
			$class = 'H'.ucfirst($module);
			$this->$module = new $class($this);
		}
		return $this->$module;
	}

	/** @return HPDO */
	function db(){
		if (is_null($this->db)) $this->db = $this->model->define_db();
		return $this->db;
	}
	/** @return HModel */
	function model(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HRouter */
	function router(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HChat */
	function chat(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HMessage */
	function message(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HCheck */
	function check(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HAuth */
	function auth(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HView */
	function view(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HBuilder */
	function builder(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HLists */
	function lists(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HManager */
	function manager(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HOperator */
	function operator(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HAnswer */
	function answer(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HLog */
	function log(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HFile */
	function file(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HConfiguration */
	function configuration(){ return $this->dynamic_module(__FUNCTION__); }
}