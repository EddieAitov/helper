<?php
class HRouter {
	const RequestApi = 1;
	const RequestFrontend = 2;

	/** @var array */
	protected $data;
	/** @var int */
	protected $request;
	/** @var bool */
	protected $core = false;

	static function link($url){
		if (HELPER_USER_FRIENDLY_URLS) return HELPER_PREFIX.'/'.$url;
		return HELPER_PREFIX.'/index.php?r='.$url;
	}

	static function request_uri(){
		// user-friendly urls
		if (HELPER_USER_FRIENDLY_URLS){
			$request_uri = $_SERVER['REQUEST_URI'];
			$request_uri = preg_replace('/^'.preg_quote(HELPER_PREFIX, '/').'/', '', $request_uri);
			if (mb_strlen($request_uri) > 0) $request_uri = mb_substr($request_uri, 1);
			// standart urls index.php?routes=...
		} else {
			if (isset($_GET['r'])) $request_uri = $_GET['r'];
			else $request_uri = '';
		}

		return $request_uri;
	}

	static function compare($uri){
		$request_uri = HRouter::request_uri();
		$array = explode('/', $request_uri);
		if ($array[0] == 'api') $request_uri = implode('/', $_POST['routes']);

		$request_uri = preg_replace('/manage\/(\d+)\/?\d*/', 'itemlist/$1', $request_uri);

		return $request_uri == $uri;
	}

	function __construct(){
		$data = explode('/', HRouter::request_uri());


		if ($data[0] == 'core'){
			$this->request = $this::RequestApi;
			$this->core = true;
			array_shift($data);
		} elseif ($data[0] == 'api'){
			$this->request = $this::RequestApi;
			array_shift($data);
		} else {
			$this->request = $this::RequestFrontend;
		}

		$this->data = $data;
	}

	function isCore(){
		return $this->core;
	}

	function request(){
		return $this->request;
	}

	function data($index = -1){
		if ($index == -1) return $this->data;
		if (isset($this->data[$index])) return $this->data[$index];
		return false;
	}
}