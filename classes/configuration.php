<?php
class HConfiguration extends HBase {
	protected $data, $last_id = 1, $ids = [];

	// pre-defined modules
	const ProfileModuleId = -1;
	const ProfilePasswordModuleId = -2;

	// проверка на дубликаты id
	function collectExistingIds($array){
		foreach ($array as $node){
			if (array_key_exists('id', $node)){
				if (in_array($node['id'], $this->ids)) throw new HException('config-id-dublicate');
				$this->ids[] = $node['id'];
			}
			if (array_key_exists('children', $node)) $this->collectExistingIds($node['children']);
		}
	}

	// простановка id всем узлам (нумерация с 1)
	function set_id(&$node){
		if (!array_key_exists('id', $node)){
			while (in_array($this->last_id, $this->ids)) $this->last_id++;
			$this->ids[] = $this->last_id;
			$node['id'] = $this->last_id;
		}

		foreach ($node['children'] as &$child) $this->set_id($child);
	}

	// простановка прав всем узлам
	function set_permissions(&$node, $parent_permissions = ''){
		// TODO: temporary (custom)
		if (!isset($node['id']) || $node['id'] == HConfiguration::ProfileModuleId){
			$node['permissions'] = 'cruda';
			foreach ($node['children'] as &$child) $this->set_permissions($child, $node['permissions']);
			return true;
		}
		$permissions = $this->parent()->auth()->permission($node['id']);
		if (!$permissions){
			if (!$parent_permissions) return false;
			$permissions = $parent_permissions;
		}

		$node['permissions'] = $permissions;
		foreach ($node['children'] as &$child) $this->set_permissions($child, $permissions);

		return true;
	}

	// простановка типов полям
	function set_types(&$node){
//		print_r($node);
		if (isset($node['field']) && !isset($node['type'])){
			if (isset($node['linked']) && $node['linked']) $node['type'] = HNodeField::TypeLinked;
			else $node['type'] = HNodeField::TypeText;
		}

		foreach ($node['children'] as &$child) $this->set_types($child);
	}

	// простановка обязательных параметров
	function set_defaults(&$node){
		if (!isset($node['children'])) $node['children'] = [];

		foreach ($node['children'] as &$child) $this->set_defaults($child);
	}

	function setup(&$node, $set_id = true){
		// массив потомков должен быть инициализирован
		$this->set_defaults($node);
		// простановка идентификаторов
		if ($set_id) $this->set_id($node);
		// простановка прав
		if (!$this->set_permissions($node)) return false;
		// простановка типов
		$this->set_types($node);
//		print_r($node);
//		print_r($_SESSION['operator']['permissions']);
		// node - ok
		return true;
	}

	function init(){
		$this->data = require(__DIR__.'/../config.modules.php');
		if (!array_key_exists('groups', $this->data)) $this->data['groups'] = [];
		if (!array_key_exists('modules', $this->data)) $this->data['modules'] = [];

		// убираем недоступные для неадминов группы
		$available_groups = [];
		for ($i = 0, $n = count($this->data['groups']); $i < $n; $i++){
			$group = $this->data['groups'][$i];
			if (isset($group['admin']) && $group['admin'] && !$this->parent()->auth()->data('admin'))
				unset($this->data['groups'][$i]);
			else $available_groups[] = $group['class'];
		}
		$this->data['groups'] = array_values($this->data['groups']);

		if (!isset($this->data['modules'])) $this->data['modules'] = [];
		if (!isset($this->data['tools'])) $this->data['tools'] = [];


		// сбор уже имеющихся идентификаторов для избежания повторов
		$this->collectExistingIds($this->data['modules']);
		$this->collectExistingIds($this->data['tools']);
		$this->setupArray($this->data['modules'], $available_groups);
		$this->setupArray($this->data['tools'], $available_groups);
	}

	function setupArray(&$array, $available_groups){
		for ($i = 0, $n = count($array); $i < $n; $i++){
			$module = $array[$i];
			// убираем недоступные для неадминов разделы
			if ((isset($module['group']) && !in_array($module['group'], $available_groups)) ||
				(isset($module['admin']) && $module['admin'] && !$this->parent()->auth()->data('admin')) ||
				// try to setup node params
				!$this->setup($array[$i])
			){
				unset($array[$i]);
			}
		}

		$array = array_values($array);
	}

	// TODO: проверка прав
	function init_profile(){
		$this->data['profile'] = [
			'id' => self::ProfileModuleId,
			'table' => 'helper_operators',
			'name' => 'profile',
			'children' => [
				['field' => 'operator_id', 'type' => 'id'],
				['field' => 'login', 'name' => 'login', 'readonly' => 1],
				['field' => 'first_name', 'name' => 'first-name'],
				['field' => 'last_name', 'name' => 'last-name'],
				['field' => 'email', 'name' => 'email'],
				[
					'id' => self::ProfilePasswordModuleId,
					'class' => 'password',
					'name' => 'password'
				]
			]
		];
//		print_r($this->data['profile']);
		$this->setup($this->data['profile'], false);
//		print_r($this->data['profile']);
	}

	// lazy load данных
	function data(){
		// формирование конфигурации
		if (is_null($this->data)) $this->init();
		return $this->data;
	}

	function modules(){
		return $this->data()['modules'];
	}

	function groups(){
		return $this->data()['groups'];
	}

	function tools(){
		return $this->data()['tools'];
	}

	function profile(){
		if (!isset($this->data()['profile'])) $this->init_profile();
		return $this->data()['profile'];
	}

	// getting tool is the same as getting module
	function tool($params){
		return $this->module($params, $this->tools());
	}

	// search by id (int), table (string) or anything else ([key=>value,...])
	function module($params, $modules = null, $process_tree = true){
		if (is_numeric($params)) $params = ['id' => $params];
		if (is_string($params)) $params = ['table' => $params];

		if (is_null($modules)){
			if (isset($params['id']) && $params['id'] < 0) $modules = [$this->profile()];
			else $modules = $this->modules();
		}
		foreach ($modules as $module){
			$match = true;
			foreach ($params as $key => $value){
				if (!isset($module[$key]) || $module[$key] != $value){
					$match = false;
					break;
				}
			}
			if ($match) return $module;
		}

		// possible switch off
		if ($process_tree){
			foreach ($modules as $module){
				if (array_key_exists('children', $module)){
					$child_result = $this->module($params, $module['children']);
					if ($child_result) return $child_result;
				}
			}
		}

		return null;
	}

	// false -> parent not found, null - module is top
	function find_parent($params, $parent = null){
		if ($params === self::ProfilePasswordModuleId) return $this->profile();

		if (is_null($parent)) $modules = $this->modules();
		elseif (array_key_exists('children', $parent)) $modules = $parent['children'];
		else return false;

		// found module in nearest children
		if ($this->module($params, $modules, false)) return $parent;

		foreach ($modules as $module){
			$parent = $this->find_parent($params, $module);
			if ($parent) return $parent;
		}

		return false;
	}
}