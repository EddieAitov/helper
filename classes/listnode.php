<?php
// wrapper для списков
class HListnode extends HNode {
	protected $id_field;

	function set_id_field($id_field){
		$this->id_field = $id_field;
	}

	function set_list($list){
		$this->list = $list;

		if (!is_null($this->id_field)){
			// first_id и last_id в chat data
			if (count($list)){
				$a = reset($list)[$this->id_field];
				$b = end($list)[$this->id_field];
				$this->attr('first_id', min($a, $b));
				$this->attr('last_id', max($a, $b));
			} else {
				$this->attr('first_id', 0);
				$this->attr('last_id', 0);
			}
		}
	}

	function show_list(){
		return '';
	}

	function api_data(){
		$out = [
			// вывод сообщений (верстка)
			'out' => HModel::pack($this->show_list()),
			// заголовок <title>
			'name' => $this->name(),
			// данные, синхронизируемые через data аттрибуты
			'data' => [
				'first_id' => intval($this->attr('first_id')),
				'last_id' => intval($this->attr('last_id'))
			]
		];

		return $out;
	}

	function show_more_button($list, $first_id = true, $last_id = false){
		$data = [
			'class' => 'btn btn-primary',
			'data-click' => 'jhelper.list.more',
			'data-list' => $list
		];

		if ($first_id) $data['data-first_id'] = $this->attr('first_id');
		if ($last_id) $data['data-last_id'] = $this->attr('last_id');

		$out = HNode::standart('a', $data)->addText(w('load-more'));

		return $out->show();
	}
}