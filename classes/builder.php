<?php
class HBuilder extends HBase {
	/** @var HNode */
	protected $root;

	function api(){
		return ['update'];
	}

	// !no external!
	// подключаемые скрипты и стили (список может быть динамическим)
	protected $js_list = [
		'js/jquery-1.11.1.min.js',
		'js/jquery-sortable.js',
	//	'/js/jquery.inputmask-multi.js',

		'js/bootstrap.min.js',
		'js/plugins/metisMenu/jquery.metisMenu.js',
		'js/plugins/dataTables/jquery.dataTables.js',
		'js/plugins/dataTables/dataTables.bootstrap.js',
		'js/sb-admin.js',
		'js/jquery.ui.widget.js',
		'js/jquery.fileupload.js',
		'js/jquery.iframe-transport.js',
		'ckeditor/ckeditor.js',
		'fancybox/jquery.fancybox.pack.js',
		'chosen/chosen.jquery.min.js',
		'datetimepicker/moment.js',
		'datetimepicker/bootstrap-datetimepicker.js',
		'datetimepicker/ru.js',
		'js/jquery.tablednd.js',
		// main module
		'js/lib.js',
		// modules
		'js/modules/misc.js',
		'js/modules/md5.js',
		'js/modules/locale.js',
		'js/modules/listeners.js',
		'js/modules/sortable.js',
		'js/modules/manager.js',
		'js/modules/table.js',
		'js/modules/navigation.js',
		'js/modules/file.js',
		'js/modules/list.js',
		'js/modules/fast_answer.js',
		'js/modules/operator.js',
		'js/modules/resize.js',
		'js/modules/updater.js',
		'js/modules/chat.js',
		'js/modules/blink_title.js',
		'js/modules/popup.js',
		'js/modules/auth.js',
		'js/modules/ajax.js',
		'js/modules/redactor.js',
		'js/modules/password.js',
		'js/modules/multi.js',
		'js/modules/datetime.js',
		'js/modules/toggle.js',
		'js/modules/alias.js',
		'js/modules/order.js',
	];
	protected $css_list = [
		'css/bootstrap.min.css',
		'font-awesome/css/font-awesome.css',
		'css/plugins/dataTables/dataTables.bootstrap.css',
		'css/sb-admin.css',
		'css/dev.css',
		'fancybox/jquery.fancybox.css',
		'chosen/chosen.min.css',
		'chosen/bs.css',
		'datetimepicker/bootstrap-datetimepicker.min.css',
	];

	// подготовка фасада, универсального для всех страниц (можно запускать повторно, результат первого прохода будет удален)
	function facade(){
		// root
		$this->root = new HNode(HNode::TypeRoot);
		$this->root()->add(new HNode(HNode::TypeDoctype));
		// html
		$html = HNode::standart('html');
		$this->root()->add($html);
		// head
		$head = HNode::standart('head');
		$html->add($head);
		$head
			->add(HNode::single('meta', ['http-equiv' => 'content-type', 'content' => 'text/html; charset=utf-8']))
			->add(HNode::single('meta', ['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge']))
			->add(HNode::standart('title'));

		foreach ($this->css_list as $url){
			$url = $this->prepare_url($url);
			$head->add(HNode::single('link', ['rel' => 'stylesheet', 'href' => $url]));
		}

		// body
		$html->add(HNode::standart('body'));
	}

	// все урлы абсолютные после преобразования
	function prepare_url($url){
		if (mb_substr($url, 0, 1) == '/') return $url;
		return HELPER_PREFIX.'/'.$url;
	}

	// низ body, вызывается в самом конце
	function basement(){
		$body = $this->root()->find('body');

		// modal
		$body->add(new HNodeCommon(HNodeCommon::TypeModal));

		// standart js
		foreach ($this->js_list as $url){
			$url = $this->prepare_url($url);
			$body->add(HNode::standart('script', ['type' => 'text/javascript', 'src' => $url]));
		}

		$body
			// locale js
			->add(
				HNode::standart('script', [
					'type' => 'text/javascript',
					'src' => $this->prepare_url('locale/'.$this->parent()->language().'.js')
				])
			)
			// custom page js
			->add(
				HNode::standart('script', ['type' => 'text/javascript'])
					->addText('jhelper.setOptions('.json_encode($this->parent()->model()->custom_js()).')')
			);
	}

	// стандартная страница
	function page_standart(){
		$this->facade();

		$this->root()->find('body')
			->add(HNode::standart('div', ['id' => 'wrapper'])
				->add(HNodeCommon::header($this->parent()->auth()->data(), $this->parent()->configuration()->data()))
				->add($this->left_menu())
				->add(HNode::standart('div', ['id' => 'page-wrapper']))
			);

		$this->left_menu_content($this->root()->find('#left_menu'));

		$this->basement();
	}

	const PageWrapper = 1;

	// прицепить контент
	function append($node, $container_type = HBuilder::PageWrapper){
		switch ($container_type){
			case HBuilder::PageWrapper:
				$container = 'div#page-wrapper';
				break;
			default:
				return;
		}

		$this->root()->find($container)->add($node);
	}

	/**
	 * @descr построение левого меню с возможностью обновления
	 * @update
	 * @return HNodeCommon
	 */
	function left_menu(){
		$menu = new HNodeCommon(HNodeCommon::TypeLeft, 'left_menu', ['id' => 'left_menu']);

		return $menu;
	}

	// LM controller
	function left_menu_content($menu, $routes = null){
		if (is_null($routes)) $routes = $this->parent()->router()->data();
		switch ($routes[0]){
			// custom chat menu
			case 'chat':
			case 'forwarded':
			case 'finished':
			case 'all':
				$this->left_menu_chat($menu, $routes);
				break;
			// main page - all the menus
			case '':
				$this->left_menu_standart($menu, $routes);
				$this->left_menu_chat($menu, $routes);
				break;
			default:
				$this->left_menu_standart($menu, $routes);
		}
	}

	/**
	 * @param HNode $menu
	 * @param array|null $routes
	 */
//<li class="active">
//<a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
//<ul class="nav nav-second-level">
//<li>
//<a class="active" href="blank.html">Blank Page</a>
//</li>
//<li>
//<a href="login.html">Login Page</a>
//</li>
//</ul>
//<!-- /.nav-second-level -->
//</li>
	function left_menu_standart($menu, $routes = null){
		if (is_null($routes)) $routes = $this->parent()->router()->data();

		$groups = $this->parent()->configuration()->groups();
		$modules = $this->parent()->configuration()->modules();

		// groups
		foreach ($groups as $group){
			$group_ar = [];
			if (isset($group['icon'])) $group_ar['icon'] = $group['icon'];
			$group_node = new HNodeCommon(HNodeCommon::TypeMenuElement, w($group['name']), $group_ar);
			// modules
			foreach ($modules as $module){
				if (!isset($module['group']) || $module['group'] != $group['class']) continue;
				$module['link'] = 'itemlist/'.$module['id'];
				$module_node = new HNodeCommon(HNodeCommon::TypeMenuElement, w($module['name']), $module);
				if ($module_node->isActive()) $group_node->setActive();
				$group_node->add($module_node);
			}
			$menu->add($group_node);
		}

		// no-group modules
		foreach ($modules as $module){
			if (isset($module['group'])) continue;
			$module['link'] = 'itemlist/'.$module['id'];
			$menu->add(new HNodeCommon(HNodeCommon::TypeMenuElement, w($module['name']), $module));
		}
	}

	/**
	 * @param HNode $menu
	 */
	function left_menu_chat($menu){
		if (!HELPER_CHAT_ENABLED || !$this->parent()->auth()->data('chat_type_ids')) return;

		$menu->setUpdater();	// only custom

		// активные и новые чаты выводим в левое меню
		$active_and_new = $this->parent()->chat()->_list([], ['active_and_new' => 1]);
		// attention - сигнал оператору обратить внимание на чат
		$info = ['attention' => false];
		foreach ($active_and_new as $attributes){
			$menu->add(new HNodeCommon(HNodeCommon::TypeMenuChatElement, '', $attributes));
			if (!$attributes['operator_id']) $info['attention'] = true;
		}
		// завершённые чаты
		$finished = $this->parent()->chat()->_list([], ['status_id' => HStatus::StatusFinished, 'limit' => 1]);
		if (count($finished)){
			$attributes = [
				'link' => 'finished',
				'icon' => 'glyphicon glyphicon-time'
			];
			$menu->add(new HNodeCommon(HNodeCommon::TypeMenuElement, w('finished-chats'), $attributes));
		}
		// переданные чаты
		$forwarded = $this->parent()->chat()->_list([], ['forwarded' => 1, 'limit' => 1]);
		if (count($forwarded)){
			$attributes = [
				'link' => 'forwarded',
				'icon' => 'glyphicon glyphicon-share-alt'
			];
			$menu->add(new HNodeCommon(HNodeCommon::TypeMenuElement, w('forwarded-chats'), $attributes));
		}
		// все чаты (только для админа)
		if ($this->parent()->auth()->admin()){
			$all = $this->parent()->chat()->_list([], ['all' => 1, 'limit' => 1]);
			if (count($all)){
				$attributes = [
					'link' => 'all',
					'icon' => 'glyphicon glyphicon-list-alt'
				];
				$menu->add(new HNodeCommon(HNodeCommon::TypeMenuElement, w('all-chats'), $attributes));
			}
		}

		$menu->attr('info', $info);
	}

	function update($params){
		$this->parent()->check()->exist(['routes'], $params);
		if (!isset($params['element']) || !in_array($params['element'],
				// array of callable update methods (method = node)
				['left_menu']
			)) return '';

		$method = $params['element'];
		unset($params['element']);
		/**
		 * @descr HNode позволяет сразу отобразить элемент, либо использовать его в дереве
		 * @var HNode $node
		 */
		$node = $this->$method($params);
		// set up content of element
		$method .= '_content';
		$this->$method($node, $params['routes']);
		// получаем верстку и дополнительные данные
		$out = [
			// при апдейте отдаем только потомков
			'out' => $node->show_children(),
			'info' => $node->attr('info')
		];

		return $out;
	}

	// форма авторизации
	function page_authorization(){
		$this->facade();
		$this->root()->find('body')->add(new HNodeAuth());
		$this->set_title(w('authorization'));
		$this->basement();
	}

	// страница с ошибкой
	function page_exception($error){
		$this->facade();
		$this->root()->find('body')->add(HNodeError::error($error));
		$this->set_title(w('error-occured'));
		$this->basement();
	}

	// заголовок
	function set_title($title){
		$this->root()->find('title')->addText($title);
	}

	/** @return HNode */
	function root(){
		return $this->root;
	}
}