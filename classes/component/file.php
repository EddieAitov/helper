<?php
class HComponentFile extends HComponentStandart {
	function api(){
		return ['upload'];
	}

	function filename($folder){
		foreach (new DirectoryIterator($folder) as $fileInfo) {
			if ($fileInfo->isDot()) continue;
			return $fileInfo->getFilename();
		}

		throw new HException('folder-is-empty');
	}

	function saveRow($params, $id, $action_type){
		// possible rename
		$folderBase = $this->folderBase($id);
		// filename field is "filename"
		if (isset($params['field'][$this->module->id()][$id]['filename'])){
			$newFilename = $params['field'][$this->module->id()][$id]['filename'];
			$oldFilename = $this->filename($folderBase);
			if ($newFilename != $oldFilename){
				rename($folderBase.'/'.$oldFilename, $folderBase.'/'.$newFilename);
			}
		}
	}

	function cloneRow($row, $new_id){
		$old_id = $row[$this->module->field_id()];

		$old_folder = $this->folderBase($old_id);
		$new_folder = $this->folderBase($new_id, true);

		$filename = $this->filename($old_folder);
		copy($old_folder.'/'.$filename, $new_folder.'/'.$filename);

	}

	function removeRow($id){
		if ($this->module->data('do_not_remove_files')) return;

		// remove file and folders
		$folder = $this->folderBase($id);
		if (!file_exists($folder)) throw new HException('error-folder-not-found');
		$file = $folder.'/'.$this->filename($folder);
		if (!file_exists($file) || !unlink($file)) throw new HException('error-removing-file');
		if (!rmdir($folder)) throw new HException('error-removing-folder');
	}

	function tableCell($parent_id){
		$query = HQuery::c($this)
			->select('*')
			->from($this->module->table())
			->order('orderby');

		$this->parent()->lists()->timeCondition($this->module, $query);
		$this->parent()->lists()->parentCondition($this->module, $query, $parent_id);
		$this->parent()->lists()->moduleCondition($this->module, $query);

//		$query->debug();
		// show all files in list
		$rows = $query->fetchAll();
		if (!count($rows)) return HNode::text('&nbsp;');

		foreach ($rows as &$row){
			$file_id = $row[$this->module->field_id()];
			$row['src'] = $this->src($file_id);
		}

		return HNodeFile::tableCellMulti($this->module, $rows);
	}

	function rearrangeFiles($in){
		$out = [];
		foreach($in as $key1 => $value1)
			foreach($value1 as $key2 => $value2)
				$out[$key2][$key1] = $value2;
		return $out;
	}

	function upload(){
		if (!isset($_FILES['upload_file']) || !is_array($_FILES['upload_file']['name'])) throw new HException('file-upload-error');

		$files = $this->rearrangeFiles($_FILES['upload_file']);
		// info about uploaded files
		$result = [];
		// nodes
		$out = '';
		foreach ($files as $file){
			// do upload, get file_id, filename
			$res = $this->upload_file($file);
			// saving data for return
			$result[] = $res;
			// creating HNodeFile with new data
			$node = HNodeFile::listitem($this->module, $res);
			// adding children for new node
			$node->addChildren($this->parent()->manager()->nodeTree($this->module, $res));
			// view node
			$out .= $node->show();
		}

		return [
			'result' => $result,
			'out' => $out
		];
	}

	// /{upload folder from config}/{upload folder from config modules}/{file_id div 1000}/{file_id}/{filename}
	function src($file_id){
		$src = HELPER_UPLOAD_FOLDER;
		if ($this->module->data('folder')) $src .= '/'.$this->module->data('folder');
		$src .= '/'.floor($file_id / 1000);
		$src .= '/'.$file_id;
		$src .= '/'.$this->filename($this->folderBase($file_id));

		return $src;
	}

	function folderBase($image_id, $init = false){
		$folderBase = __DIR__.'/../../..'.HELPER_UPLOAD_FOLDER;
		if ($this->module->data('folder')) $folderBase .= '/'.$this->module->data('folder');
		// max 1000 folders in one top level folder
		$folderBase .= '/'.floor($image_id / 1000);
		// this folder may not exist

		if ($init && !file_exists($folderBase)){
			if (!mkdir($folderBase)) throw new HException('cannot-create-top-dir-for-upload');
		}
		// {base}/{id div 1000}/{id}/{image_index}/{image_name}
		$folderBase .= '/'.$image_id;
		if ($init){
			if (file_exists($folderBase)) throw new HException('upload-dir-error');
			// folder is empty at init moment
			if (!mkdir($folderBase)) throw new HException('cannot-create-dir-for-upload');
		}
		return $folderBase;
	}

	/**
	 * @descr загрузка изображения на сервер. создание копий для всех указанных размеров. без загрузки в базу.
	 * @param array $file
	 * @return array
	 * @throws HException
	 */
	protected function upload_file($file){
		if (preg_match('/^('.HELPER_DENIED_EXTS.')$/i', pathinfo($file['name'], PATHINFO_EXTENSION)))
			throw new HException('extension-is-not-allowed');

		// id
		$file_id = $this->parent()->manager()->reserveId($this->module);
		// folder
		$folderBase = $this->folderBase($file_id, true);
		// name
		$filename = mb_strtolower($file['name']);
		// move file
		move_uploaded_file($file['tmp_name'], $folderBase.'/'.$filename);

		return ['file_id' => $file_id, 'filename' => $filename];
	}
}