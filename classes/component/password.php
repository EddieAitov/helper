<?php
class HComponentPassword extends HComponentAbstract {
	function api(){
		return ['reset'];
	}

	function reset($params){
//		$check = $this->parent->check();
//		$check->exist(['partner_id'], $params);
//		$check->numeric(['partner_id'], $params);

		// id родительского модуля, содержащего данные пользователя
		$module_id = $this->module->id();
		$parent_module = HModule::c($this->parent()->configuration()->find_parent($this->module->id()));
		if (!$parent_module->find(['field' => 'email'])) throw new HException('email-field-not-defined');
		$parent_module_id = $parent_module->id();
		$parent_row_id = array_keys($params['field'][$parent_module_id])[0];

		if (!$params['field'][$parent_module_id][$parent_row_id]['email'] || !isset($params['field'][$module_id]['by_email']))
			throw new HException('email-is-needed');

		$current_user_id = $this->parent->auth()->id();

		// попытка одного партнера сбросить пароль другому партнеру
		if ($current_user_id != $parent_row_id && !$this->parent()->auth()->admin())
			throw new HException('cannot-reset-password-for-another-user');

		$too_frequent = HQuery::c($this)
			->select(1)
			->from('helper_reset_password_notifications')
			->where_not_older('create_time', 2, 'minute')
			->where_equal('user_id', $parent_row_id)
			->fetchColumn();

		if ($too_frequent) throw new HException('cannot-reset-password-too-recent');

		HQuery::c($this)
			->insert_into('helper_reset_password_notifications')
			->set('create_time', HQuery::Now)
			->set('user_id', $parent_row_id)
			->set('initiator_id', $current_user_id)
			->set('email', $params['field'][$parent_module_id][$parent_row_id]['email'])
			->ex();

		$hash = $this->createAndSendPassword($params['field'][$parent_module_id][$parent_row_id]['email']);

		HQuery::c($this)
			->update('helper_operators')
			->set('password', $hash)
			->where_equal('operator_id', $parent_row_id)
			->ex();

		return w('password-successfully-sent');
	}

	function generatePassword($length = 8){
		$chars = '';
		for ($i = 0; $i < $length; $i++) $chars .= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return substr(str_shuffle($chars), 0, $length);
	}

	// установка пароля в первый раз, либо сброс существующего пароля
	function createAndSendPassword($email, $password = false){
		if (!$password) $password = $this->generatePassword();
		if ($email){
			$text = str_replace(['%password%', '%url%'], [$password, HELPER_SITE_URL.HELPER_PREFIX], w('send-password-to-email'));
			if (!$this->parent()->model()->mail($email, w('send-password-to-email-title'), $text))
				throw new HException('error-while-sending-mail');
		}

		// пароль в открытом виде существует только внутри данной функции - вне нее только хэш
		return md5($password);
	}

	function saveModule($params, $parent_id, $action_type){
		if ($action_type == HQuery::TypeInsert) $this->reset($params);
	}

	function getRows($parent_id){
		$out = [];

		$out['notification'] = HQuery::c($this)
			->select_time('n.create_time', HQuery::FormatDateTime, 'create_time')
			->select('n.initiator_id')
			->select('o.name as user_name')
			->from('helper_reset_password_notifications', 'n')
			->join('inner join helper_operators as o on o.operator_id = n.initiator_id')
			->where_equal('n.user_id', $parent_id)
			->order('n.create_time desc')
			->limit(1)
			->fetch();

		if ($out['notification'] && $this->parent()->auth()->admin()){
			$module_id = HModule::c($this->parent()->configuration()->module('helper_operators'))->id();
			$out['notification']['link'] = $this->parent()->router()->link('manage/'.$module_id.'/'.$out['notification']['initiator_id']);
		}

		// emulating array of elements. this module always has 1 element.
		return [$out];
	}
}