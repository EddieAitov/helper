<?php
class HComponentStandart extends HComponentAbstract {
	function getRow($id){
		$query = HQuery::c($this)
			->select('*')
			->from($this->module->table());
		$this->parent()->lists()->idCondition($this->module, $query, $id);
		$this->parent()->lists()->timeCondition($this->module, $query);
		$row = $query->fetch();
		return $row;
	}

	function getRows($parent_id){
		$query = HQuery::c($this)
			->select('*')
			->from($this->module->table());
		$this->parent()->lists()->parentCondition($this->module, $query, $parent_id);
		$this->parent()->lists()->timeCondition($this->module, $query);
		$this->parent()->lists()->moduleCondition($this->module, $query);
		// order by orderby
		if ($this->parent()->lists()->fieldExists($this->module->table(), HNodeField::TypeOrderby)) $query->order(HNodeField::TypeOrderby);
		$rows = $query->fetchAll();
		return $rows;
	}
}