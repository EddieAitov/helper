<?php
class HComponentImage extends HComponentStandart {
	function api(){
		return ['upload'];
	}

	function filename($folder){
		foreach (new DirectoryIterator($folder) as $fileInfo) {
			if ($fileInfo->isDot()) continue;
			return $fileInfo->getFilename();
		}

		throw new HException('folder-is-empty', ['folder' => $folder]);
	}

	function saveRow($params, $id, $action_type){
		// possible rename
		$folderBase = $this->folderBase($id);
		// filename field is "filename"
		if (isset($params['field'][$this->module->id()][$id]['filename'])){
			$newFilename = $params['field'][$this->module->id()][$id]['filename'];
			$oldFilename = $this->filename($folderBase.'/0');
			if ($newFilename != $oldFilename){
				for ($i = 0; $i < count($this->module->data('sizes')); $i++){
					rename($folderBase.'/'.$i.'/'.$oldFilename, $folderBase.'/'.$i.'/'.$newFilename);
				}
			}
		}
	}

	function cloneRow($row, $new_id){
		$old_id = $row[$this->module->field_id()];

		$old_folder = $this->folderBase($old_id);
		$new_folder = $this->folderBase($new_id, true);

		for ($i = 0; $i < count($this->module->data('sizes')); $i++){
			$of = $old_folder.'/'.$i;
			$nf = $new_folder.'/'.$i;
			if (!mkdir($nf)) throw new HException('error-creating-folder-for-cloning-file');
			$filename = $this->filename($of);
			copy($of.'/'.$filename, $nf.'/'.$filename);
		}
	}

	function removeRow($id){
		if ($this->module->data('do_not_remove_files')) return;

		// remove files and folders
		$f = $this->folderBase($id);
		for ($i = 0; $i < count($this->module->data('sizes')); $i++){
			$dn = $f.'/'.$i;
			if (!file_exists($dn)) throw new HException('error-folder-not-found');
			$fn = $dn.'/'.$this->filename($dn);
			if (!file_exists($fn) || !unlink($fn)) throw new HException('error-removing-file');
			if (!rmdir($dn)) throw new HException('error-removing-folder');
		}
		if (!rmdir($f)) throw new HException('error-removing-top-folder');
	}

	function tableCell($parent_id){
		$query = HQuery::c($this)
			->select('*')
			->from($this->module->table())
			->order('orderby')
			->limit(1);

		$this->parent()->lists()->timeCondition($this->module, $query);
		$this->parent()->lists()->parentCondition($this->module, $query, $parent_id);
		$this->parent()->lists()->moduleCondition($this->module, $query);

//		$query->debug();
		// show only primary image in list
		$row = $query->fetch();
		if (!$row) return HNode::text('&nbsp;');

		$id = $row[$this->module->field_id()];
		$row['src'] = $this->src($id, (int)$this->module->data('preview_index'));
		$row['big'] = $this->src($id, count($this->module->data('sizes')) - 1);

		return HNodeImage::tableCell($this->module, $row);
	}

	function rearrangeFiles($in){
		$out = [];
		foreach($in as $key1 => $value1)
			foreach($value1 as $key2 => $value2)
				$out[$key2][$key1] = $value2;
		return $out;
	}

	function upload(){
		if (!isset($_FILES['upload_file']) || !is_array($_FILES['upload_file']['name'])) throw new HException('file-upload-error');

		$files = $this->rearrangeFiles($_FILES['upload_file']);
		// info about uploaded files
		$result = [];
		// nodes
		$out = '';
		foreach ($files as $file){
			// do upload, get image_id, filename
			$res = $this->upload_file($file);
			// saving data for return
			$result[] = $res;
			// creating HNodeImage with new data
			$node = HNodeImage::listitem($this->module, $res);
			// adding children for new node
			$node->addChildren($this->parent()->manager()->nodeTree($this->module, $res));
			// view node
			$out .= $node->show();
		}

		return [
			'result' => $result,
			'out' => $out
		];
	}

	// тип уменьшения изображения
	// вписать в прямоугольник
	const SizingInner = 0;
	// описать вокруг прямоугольника
	const SizingOuter = 1;
	// описать вокруг прямоугольника и обрезать по границам прямоугольника
	const SizingOuterAndCrop = 2;

	// порядок элементов в массиве конфигурации
	const Width = 0;
	const Height = 1;
	const Sizing = 2;

	function src($image_id, $preview_index){
		$src = HELPER_UPLOAD_FOLDER;
		if ($this->module->data('folder')) $src .= '/'.$this->module->data('folder');
		$src .= '/'.floor($image_id / 1000);
		$src .= '/'.$image_id;
		$src .= '/'.$preview_index;
		$src .= '/'.$this->filename($this->folderBase($image_id).'/'.$preview_index);

		return $src;
	}

	function folderBase($image_id, $init = false){
		$folderBase = __DIR__.'/../../..'.HELPER_UPLOAD_FOLDER;
		if ($this->module->data('folder')) $folderBase .= '/'.$this->module->data('folder');
		// max 1000 folders in one top level folder
		$folderBase .= '/'.floor($image_id / 1000);
		// this folder may not exist

		if ($init && !file_exists($folderBase)){
			if (!mkdir($folderBase)) throw new HException('cannot-create-top-dir-for-upload', $folderBase);
		}
		// {base}/{id div 1000}/{id}/{image_index}/{image_name}
		$folderBase .= '/'.$image_id;
		if ($init){
			if (file_exists($folderBase)) throw new HException('upload-dir-error');
			// folder is empty at init moment
			if (!mkdir($folderBase)) throw new HException('cannot-create-dir-for-upload');
		}
		return $folderBase;
	}

	/**
	 * @descr загрузка изображения на сервер. создание копий для всех указанных размеров. без загрузки в базу.
	 * @param array $file
	 * @return array
	 * @throws HException
	 */
	protected function upload_file($file){
		$filename = $file['tmp_name'];
		$imageInfo = @getimagesize($filename);
		if (!$imageInfo) throw new HException('failed-to-open-image-file');
		list($width, $height, $ext) = $imageInfo;

		switch ($ext){
			case IMAGETYPE_GIF:
				$source = imagecreatefromgif($filename);
				break;
			case IMAGETYPE_JPEG:
				$source = imagecreatefromjpeg($filename);
				break;
			case IMAGETYPE_PNG:
				$source = imagecreatefrompng($filename);
				break;
			default:
				throw new HException('unsupported-image-format');
		}

		if ($source === false) throw new HException('error-creating-image');

		// id
		$image_id = $this->parent()->manager()->reserveId($this->module);
		// folder
		$folderBase = $this->folderBase($image_id, true);
		// name
		$filename = mb_strtolower($file['name']);
		if (preg_match('/^('.HELPER_DENIED_EXTS.')/', pathinfo($filename, PATHINFO_EXTENSION)))
			throw new HException('extension-is-not-allowed');

		// process all sizes
		for ($i = 0; $i < count($this->module->data('sizes')); $i++){
			// final folder name
			$folder = $folderBase.'/'.$i;
			mkdir($folder);

			$size = $this->module->data('sizes')[$i];
			$scale = 1;

			if (!isset($size[self::Sizing])) $size[self::Sizing] = self::SizingInner;

			// нужно описать указанный прямоугольник
			if ($size[self::Sizing] == $this::SizingInner){
				// ниодна из сторон не должна заходить за границы (вписываем в заданный прямоугольник)
				if ($width / $size[self::Width] > $scale) $scale = $width / $size[self::Width];
				if ($height / $size[self::Height] > $scale) $scale = $height / $size[self::Height];
			} else {
				// уменьшаем до первой попавшейся границы (описываем заданный прямоугольник)
				$scaleX = $width / $size[self::Width];
				$scaleY = $height / $size[self::Height];
				if ($scaleX <= 1 || $scaleY <= 1) $scale = 1;
				else $scale = min($scaleX, $scaleY);
			}

			$scaledWidth = $width / $scale;
			$scaledHeight = $height / $scale;

			// задан строгий размер для конечного изображения
			if ($size[self::Sizing] == $this::SizingOuterAndCrop){
				$newWidth = $size[self::Width];
				$newHeight = $size[self::Height];
				$x = -($scaledWidth - $newWidth) / 2;
				$y = -($scaledHeight - $newHeight) / 2;
			} else {
				$newWidth = $scaledWidth;
				$newHeight = $scaledHeight;
				$x = 0;
				$y = 0;
			}

			// создание нового изображения с выбранными размерами
			$result = imagecreatetruecolor($newWidth, $newHeight);
			// сохраняем альфу
			imagealphablending($result, false);
			imagesavealpha($result, true);
			$transparent = imagecolorallocatealpha($result, 255, 255, 255, 127);
			imagefilledrectangle($result, 0, 0, $newWidth, $newHeight, $transparent);
			// перенос из старой картинки в новый размер
			imagecopyresampled($result, $source, $x, $y, 0, 0, $scaledWidth, $scaledHeight, $width, $height);

			$path = $folder.'/'.$filename;

			switch ($ext){
				case IMAGETYPE_GIF:
					imagegif($result, $path);
					break;
				case IMAGETYPE_JPEG:
					imagejpeg($result, $path, 95);
					break;
				case IMAGETYPE_PNG:
					imagepng($result, $path, 3);
					break;
			}

			imagedestroy($result);
		}

		imagedestroy($source);
		return ['image_id' => $image_id, 'filename' => $filename];
	}
}