<?php
abstract class HComponentAbstract extends HBase {
	/** @var HModule */
	protected $module;

	/**
	 * @param HBase $base
	 * @param HModule $module
	 */
	function __construct($base, $module){
		parent::__construct($base->parent());
		$this->module = $module;
	}

	/* custom module model methods */

	// get top row with id
	function getRow($id){ return false; }
	// get array of rows for parent_id
	function getRows($parent_id){ return []; }
	// save method for class (called once with id of parent module's object - no existing rows required)
	function saveModule($params, $parent_id, $action_type){}
	// save method for each row with id
	function saveRow($params, $id, $action_type){}
	// extra remove behavior before standart removing (from db)
	function removeRow($id){}
	// clone for module
	function cloneModule($parent_id, $new_parent_id){}
	// clone for row
	function cloneRow($row, $new_id){}

	/**
	 * @param $parent_id
	 * @return HNodeAbstract
	 */
	function tableCell($parent_id){}
}