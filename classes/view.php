<?php
// View-Controller для вьюх из папки view
class HView extends HBase {
	// обработка запроса нужным модулем
	protected function dynamic_module($module){
		$this->init($module);
		return $this->$module;
	}

	protected function init($module){
		if (!isset($this->$module) || is_null($this->$module)){
			$class = 'HView'.ucfirst($module);
			// инициализируем субмодуль view
			$this->$module = new $class($this->parent);
		}
	}

	/* getters */

	/** @return HViewMain */
	function main(){ return $this->dynamic_module(__FUNCTION__); }
	/** @return HViewPopup */
	function popup(){ return $this->dynamic_module(__FUNCTION__); }
}