<?php
class HAuth extends HBase {
	const api = 1;
	const free_api = 1;
	protected $permissions = [];

	function check(){
		return isset($_SESSION['operator']) ? 1 : 0;
	}

	function permission($key){
		$permissions = isset($_SESSION['operator']['permissions']) ? $_SESSION['operator']['permissions'] : [];
		if (isset($permissions[$key])) $out = $permissions[$key][0];
		elseif (isset($permissions['*'])) $out = '*';
		else $out = '';

		if ($out == '*') $out = 'cruda';

		return strtolower($out);
	}

	// called immediatelly after signin and actually stored in session
	function setPermissions($permissions){
		$permissions = explode('|', $permissions);
		$out = [];

		foreach ($permissions as $permission){
			$permission = explode(':', $permission);
			$key = $permission[0];
			array_splice($permission, 0, 1);
			// value for every key is array
			$out[$key] = $permission;
		}

		return $out;
	}

	function _refresh_data(){
		if (!$this->check()) return;

		$operator = HQuery::c($this)
			->select(['h_o.*', 'helper_groups.permissions', 'helper_groups.chat_type_ids', 'helper_groups.languages'])
			->from('helper_operators', 'h_o')
			->join_using('helper_groups', 'group_id')
			->where_equal('h_o.operator_id', $this->id())
			->fetch();

		$operator['permissions'] = $this->setPermissions($operator['permissions']);
		$_SESSION['operator'] = $operator;
	}

	function signin($params){
		$this->parent()->check()->is_not_empty(['login', 'hash', 'timestamp'], $params);

		$params['login'] = str_replace('%', '', $params['login']);

		$operator = HQuery::c($this)
			->select(['h_o.*', 'helper_groups.permissions', 'helper_groups.chat_type_ids', 'helper_groups.languages'])
			->from('helper_operators', 'h_o')
			->join_using('helper_groups', 'group_id')
			->where_ilike('h_o.login', $params['login'])
			->fetch();

		if (!$operator) throw new HException('user-not-found');
		if ($operator['blocked']) throw new HException('account-is-blocked');

		$hash = md5($operator['password'].':'.$params['timestamp']);
		if ($hash != $params['hash']) throw new HException('hash-error');

		$operator['permissions'] = $this->setPermissions($operator['permissions']);

		$_SESSION['operator'] = $operator;
	}

	function signout(){
		if (isset($_SESSION['operator'])) unset($_SESSION['operator']);
	}

	function id(){
		if (!$this->check()) return 0;
		return $this->data('operator_id');
	}

	function name(){
		return trim($this->data('first_name', '').' '.$this->data('last_name', ''));
	}

	function admin(){
		if (!$this->check()) return false;
		return (bool)$this->data('admin');
	}

	function language(){
		$language = $this->data('language');
		if (!$language) $language = HLang::helper_language();

		return $language;
	}

	function languages(){
		if (!$this->check()) return [];

		if (!$this->data('languages')) return HLang::get_list();
		return explode(',', $this->data('languages'));
	}

	/**
	 * @param bool $key
	 * @param bool $default
	 * @return mixed
	 */
	function data($key = false, $default = false){
		if (!$this->check()) return $default;
		if ($key === false) return $_SESSION['operator'];
		if (isset($_SESSION['operator'][$key])) return $_SESSION['operator'][$key];
		return $default;
	}
}