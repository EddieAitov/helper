<?php
class HLog extends HBase {
	const TypeSendConversation = 1;

	function insert($type, $data){
		HQuery::c($this)
			->insert_into('helper_log')
			->set('date', HQuery::Now)
			->set('ip', $_SERVER['REMOTE_ADDR'])
			->set('type', $type)
			->set('data', json_encode($data))
			->ex();
	}
}