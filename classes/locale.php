<?php
// short alias
function w($key, $n = false){
	return HLocale::getInstance()->word($key, $n);
}

class HLocale {
	private static $instance;
	private function __construct(){}
	private function __clone(){}
	private function __wakeup(){}

	public static function getInstance(){
		if (empty(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}

	const LocaleTypeSiteBackend = 1;
	const LocaleTypeSiteFrontend = 2;
	const LocaleTypeAdminBackend = 3;
	const LocaleTypeAdminFrontend = 4;
	const LocaleTypeHelperBackend = 5;
	const LocaleTypeHelperFrontend = 6;

	function localeTypes(){
		return [
			self::LocaleTypeSiteBackend,
			self::LocaleTypeSiteFrontend,
			self::LocaleTypeAdminBackend,
			self::LocaleTypeAdminFrontend,
			self::LocaleTypeHelperBackend,
			self::LocaleTypeHelperFrontend,
		];
	}

	function localeBackend(){
		return [
			self::LocaleTypeSiteBackend,
			self::LocaleTypeAdminBackend,
			self::LocaleTypeHelperBackend,
		];
	}

	function localeFrontend(){
		return [
			self::LocaleTypeSiteFrontend,
			self::LocaleTypeAdminFrontend,
			self::LocaleTypeHelperFrontend,
		];
	}

	/**
	 * @param $locale_type
	 * @param $language
	 * @return string
	 * @throws Exception
	 */
	function getLocaleFilename($locale_type, $language){
		$out = [
			self::LocaleTypeSiteBackend => __DIR__.'/../../locale/'.$language.'.php',
			self::LocaleTypeSiteFrontend => __DIR__.'/../../locale/'.$language.'.js',
			self::LocaleTypeAdminBackend => __DIR__.'/../../pcp/locale/'.$language.'.php',
			self::LocaleTypeAdminFrontend => __DIR__.'/../../pcp/locale/'.$language.'.js',
			self::LocaleTypeHelperBackend => __DIR__.'/../../helper/locale/'.$language.'.php',
			self::LocaleTypeHelperFrontend => __DIR__.'/../../helper/locale/'.$language.'.js'
		];
		return $out[$locale_type];
	}

	private $locale = null;
	function word($key, $n = false){
		if (!function_exists('locale')) return $key;
		if (is_null($this->locale)) $this->locale = locale();
		if (!isset($this->locale[$key])) return $key;

		$value = $this->locale[$key];

		// fix for ES translator
		if (is_array($value) && count($value) == 1) $value = $value[0];

		// если нужно учитывать кол-во
		if (is_array($value)){
			// нужно учесть пол - 1 муж, 2 жен
			if (count($value) == 2){
				if (!$n) $n = 1;
				if (!in_array($n, [1, 2])) throw new Exception('locale-error');
				return $value[$n - 1];
				// нужно учесть число (1, 2, 5)
			} elseif (count($value) != 3) return $value;
			if ($n === false) $n = 1;
			$i = $n % 10 == 1 && $n % 100 != 11 ? 0 :
				($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? 1 : 2);
			return $value[$i];
		} else {
			return $value;
		}
	}
}