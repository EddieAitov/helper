<?php
class HModel extends HBase {
	// headers
	function headers(){
		// redirect after signin
		if (isset($_POST['signin'])){
			header('location: '.$_SERVER['REQUEST_URI']);
			exit;
		}

		if (session_status() == PHP_SESSION_NONE) session_start();
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		mb_internal_encoding('utf-8');

		if (!ini_get('date.timezone')) date_default_timezone_set('GMT');
		if (!headers_sent()) header('content-type: text/html; charset=utf-8');
	}

	// language init
	function define_language(){
		$language = $this->parent()->language();

		if (!file_exists(__DIR__.'/../locale/'.$language.'.php'))
			throw new HException('Language file cannot be found!');
		/** @noinspection PhpIncludeInspection */
		require_once(__DIR__.'/../locale/'.$language.'.php');
	}

	// db init
	function define_db(){
		try {
			switch (DB_TYPE){
				case DB_POSTGRE:
					$db = new HPDO('pgsql:host='.DB_HOST.';port=5432;dbname='.DB_NAME.';user='.DB_USER.';password='.DB_PASSWORD);
					break;
				case DB_MYSQL:
					$db = new HPDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);
					$db->query("set names utf8");
					break;
				default:
					throw new HException();
			}
		// any exception = db error
		} catch (Exception $e){
			exit('Database error');
		}
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		return $db;
	}

	function clear_disposable_session(){
		if (isset($_SESSION['disposable'])) unset($_SESSION['disposable']);
	}

	const TypeHelper = 'H';
	const TypeMain = 'M';
	const TypeCore = 'C';
	const TypeInterface = 'I';
	const TypeParent = 'P';

	// lazy-load
	function autoload(){
		spl_autoload_register(function($class){
			// 0: exploding class for first letter and class name
			$type = substr($class, 0, 1);
			$class = substr($class, 1);

			// first: choosing source folder
			switch ($type){
				case self::TypeHelper: $prefix = ''; break;
				case self::TypeMain: $prefix = '/../../classes'; break;
				case self::TypeCore: $prefix = '/../../core/classes'; break;
				case self::TypeInterface: $prefix = '/../../core/interfaces'; break;
				case self::TypeParent: $prefix = '/../../core/parents'; break;
				default: return;
			}

			// second: checking for special class
			$specials = ['View', 'Node', 'Component', 'Tool'];
			$filename = null;
			foreach ($specials as $special){
				if (mb_substr($class, 0, mb_strlen($special)) == $special && mb_strlen($class) > mb_strlen($special)){
					$filename = __DIR__.$prefix.'/'.mb_strtolower($special).'/'.mb_strtolower(substr($class, mb_strlen($special))).'.php';
					break;
				}
			}

			// standart class type
			if (is_null($filename)) $filename = __DIR__.$prefix.'/'.strtolower($class).'.php';

			if (!file_exists($filename)) throw new HException('module-not-found');
			/** @noinspection PhpIncludeInspection */
			require($filename);
		});
	}

	/**
	 * @return HBase
	 * @throws HException
	 */
	function api_object(){
		$module = $this->parent()->router()->data(0);
		$method = $this->parent()->router()->data(1);

		// standart module (from tree) api call
		if (is_numeric($module)){
			// get module from tree
			$object = $this->parent()->manager()->getModule($module);
			$object->setDelegate($this);
			// custom api calls on module are executed in its delegate
			$object = $object->delegate();
		} elseif ($module == 'tool'){
			$object = $this->parent()->manager()->getTool($method);
			$method = 'execute';	// 1-click tool (1 method available)
		// popup call: $module = popup
		} elseif ($module == 'popup') {
			$object = $this->parent()->view()->popup();
		// standart call
		} else {
			$object = $this->parent()->dynamic_module($module);
		}

		$rc = new ReflectionClass($object);
		// проверка, что к классу  можно обращаться через api - либо задана константа api
		if (!$rc->hasConstant('api')){
			// либо функция api возвращает список допустимых методов
			/** @noinspection PhpUndefinedMethodInspection */
			if (!$rc->hasMethod('api') || !in_array($method, $object->api()))
				throw new HException('module-is-not-callable');
		}
		// проверка, существует ли нужный метод
		if (!$rc->hasMethod($method)) throw new HException('method-not-found');
		// проверка, не закрыт ли выбранный метод от вызовов из вне
		if (mb_substr($method, 0, 1) == '_') throw new HException('method-is-not-callable');

		// если пользователь не авторизован, проверка, можно ли вызывать выбранный метод
		if (!$this->parent()->auth()->check()){
			// все методы доступны для неавторизованного вызова
			if (!$rc->hasConstant('free_api')){
				// доступен ограниченный список методов для неавторизованного вызова
				if (!$rc->hasMethod('free_api')) throw new HException('authorization-required');
				/** @noinspection PhpUndefinedMethodInspection */
				if (!in_array($method, $object->free_api())) throw new HException('authorization-required');
			}
		}

		return [$object, $method];
	}

	function custom_js(){
		return [
			'routes' => $this->parent()->router()->data(),
//			'session_id' => session_id(),
			'helper_prefix' => HELPER_PREFIX,
			'helper_user_friendly_urls' => HELPER_USER_FRIENDLY_URLS,
			'langs' => HLang::get_list()
		];
	}

	function mail($to, $subject, $body){
		$headers = [];
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=utf-8';
		$headers[] = 'Content-Transfer-Encoding: 7bit';
		$headers[] = 'From: '.HELPER_RETURN_NAME.' <'.HELPER_RETURN_ADDRESS.'>';

		return mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=', $body, implode("\r\n", $headers));
	}

	// для передачи в json
	static function pack($text){
		return str_replace(["\r", "\n", "\t"], ['', '', ''], $text);
	}
}