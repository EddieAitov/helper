<?php
// обертка для массива конфига модуля
class HModule {
	protected $data, $c, $r, $u, $d, $a;
	/** @var HModule[] */
	protected $children = [];
	/** @var HComponentAbstract */
	protected $delegate;

	static function c($array){
		return new self($array);
	}

	// set delegate object
	function setDelegate($base){
		$delegate = $this->data('class');
		if (!$delegate) $delegate = 'standart';

		$delegate = 'HComponent'.ucfirst($delegate);
		$delegate = new $delegate($base, $this);
		$this->delegate = $delegate;
	}

	// set delegate objects for all composite children modules
	function setDelegateAll($base){
		$this->setDelegate($base);
		foreach ($this->composites() as $composite) $composite->setDelegateAll($base);
	}

	// delegated functions

	/**
	 * @param $id
	 * @return array|bool
	 */
	function getRow($id){
		return $this->delegate()->getRow($id);
	}

	function getRows($parent_id){
		return $this->delegate()->getRows($parent_id);
	}

	function saveModule($params, $parent_id, $action_type){
		$this->delegate()->saveModule($params, $parent_id, $action_type);
	}

	function saveRow($params, $id, $action_type){
		$this->delegate()->saveRow($params, $id, $action_type);
	}

	function removeRow($id){
		$this->delegate()->removeRow($id);
	}

	function tableCell($parent_id){
		return $this->delegate()->tableCell($parent_id);
	}

	function cloneModule($parent_id, $new_parent_id){
		$this->delegate()->cloneModule($parent_id, $new_parent_id);
	}

	function cloneRow($row, $new_id){
		$this->delegate()->cloneRow($row, $new_id);
	}

	/**
	 * @descr for api access
	 * @return HComponentAbstract
	 */
	function delegate(){
		return $this->delegate;
	}

	/**
	 * @descr Построение объекта из массива конфигурации для дальнейшего использования во View
	 * @param [] $array
	 */
	function __construct($array){
		// for dynamic init
		if (!isset($array['permissions'])) $array['permissions'] = '*';
		foreach (['c', 'r', 'u', 'd', 'a'] as $term)
			$this->$term = $array['permissions'] == '*' || stripos($array['permissions'], $term);
		// for dynamic init
		if (!isset($array['children'])) $array['children'] = [];
		foreach ($array['children'] as $child) $this->add($child);

		unset($array['permissions'], $array['children']);

		$this->data = $array;
	}

	function children(){
		return $this->children;
	}

	function child($index){
		return isset($this->children[$index]) ? $this->children[$index] : null;
	}

	function add($array){
		$this->children[] = HModule::c($array);
	}

	// permissions
	function canCreate(){
		return $this->a;
	}

	function canRead(){
		return $this->r;
	}

	function canUpdate(){
		return $this->u;
	}

	function canDelete(){
		return $this->d;
	}

	function canApprove(){
		return $this->a;
	}

	function needApprove(){
		return false;
	}

	function isEditableList(){
		return (!$this->needApprove() || $this->canApprove()) &&
			(is_null($this->data('editable_list')) || $this->data('editable_list'));
	}

	// можно при необходимости добавить проверку возможности сортировки внутри модуля
	function isSortable(){
		return true;
	}

	function isFilterable(){
		return true;
	}

	function isField(){
		return $this->field() ? true : false;
	}

	/**
	 * @descr COLUMN is used in lists. it could be field, or any composite which supports list_mode (currently: image)
	 * @return bool
	 */
	function isColumn(){
		// highest priority. possibility for composite objects to show in list.
		if ($this->data('show_in_list')) return true;

		return $this->field() &&
			in_array($this->type(), [
				HNodeField::TypeText,
				HNodeField::TypeId,
				HNodeField::TypeLinked,
				HNodeField::TypeTextarea,
				HNodeField::TypeCheckbox,
				HNodeField::TypeMultiSelect,
				HNodeField::TypeDate,
				HNodeField::TypeDatetime
			]) &&
			!$this->data('hide_in_list');
	}

	function isLinkedStandart(){
		return $this->data('linked') && is_string($this->data('linked'));
	}

	function name(){
		if ($this->type() == HNodeField::TypeId) return 'Id';
		return w($this->data(__FUNCTION__));
	}

	function field(){
		return $this->data(__FUNCTION__);
	}

	function request_field($table_alias){
		return $table_alias.'.'.$this->field();
	}

	function request_field_as($table_alias, $as){
		return $table_alias.'.'.$this->field().' as '.$as;
	}

	function request_field_linked($suffix = '', $tableSuffix = ''){
		return "{$this->linked()}{$tableSuffix}.name as {$this->field()}{$suffix}";
	}

	function itemlist_field($table_alias){
		if ($this->data('linked')) return $this->request_field_linked();
		return $this->request_field($table_alias);
	}

	function type(){
		return $this->data(__FUNCTION__);
	}

	function id(){
		return $this->data(__FUNCTION__);
	}

	function linked(){
		return $this->data(__FUNCTION__);
	}

	function table(){
		return $this->data(__FUNCTION__);
	}

	function data($key = null, $value = null){
		if (!is_null($value)){
			$this->data[$key] = $value;
			return null;
		}
		if (is_null($key)) return $this->data;
		if (isset($this->data[$key])) return $this->data[$key];
		return null;
	}

	/**
	 * @descr получить массив потомков, для использования в качестве колонок в списке (рекурсивный проход по дереву потомков)
	 * @return HModule[]
	 */
	function columns(){
		$out = [];
		foreach ($this->children as $child){
			if ($child->isColumn()) $out[] = $child;
		}
		return $out;
	}

	/**
	 * @descr получить массив потомков, являющихся полями
	 * @return HModule[]
	 */
	function fields(){
		$out = [];
		foreach ($this->children as $child){
			if ($child->isField()) $out[] = $child;
		}
		return $out;
	}

	/**
	 * @descr получить массив потомков, являющихся сложными объектами
	 * @return HModule[]
	 */
	function composites(){
		$out = [];
		foreach ($this->children as $child){
			if (!$child->isField()) $out[] = $child;
		}
		return $out;
	}

	const NotEmpty = 'NotEmpty';

	/**
	 * @descr поиск среди потомков по параметрам
	 * @param $params
	 * @return HModule|null
	 */
	function find($params){
		foreach ($this->children() as $child){
			$match = true;
			foreach ($params as $key => $value){
				if ($child->data($key) != $value && ($value != self::NotEmpty || !$child->data($key))){
					$match = false;
					break;
				}
			}
			if ($match) return $child;
		}

		foreach ($this->children() as $child){
			$child_result = $child->find($params);
			if ($child_result) return $child_result;
		}

		return null;
	}

	function findByType($type, $return_type = false){
		$field = $this->find(['type' => $type]);
		if (is_null($field) && $return_type) $field = HModule::c(['field' => $type]);
		return $field;
	}

	// short aliases
	function field_id(){
		return $this->findByType(HNodeField::TypeId)->field();
	}

	function field_parent(){
		return $this->findByType(HNodeField::TypeParentId)->field();
	}

	function field_module(){
		return $this->findByType(HNodeField::TypeModuleId)->field();
	}

	function field_order(){
		return $this->findByType(HNodeField::TypeOrderby)->field();
	}

	function setFieldIfNotExists($type, $row = null){
		if (!is_null($row) && !isset($row[$type])) return;

		if (!$this->findByType($type)){
			$this->add([
				'field' => $type,
				'type' => $type
			]);
		}
	}

	function sequence(){
		$sequence = $this->data('sequence');
		if (!$sequence){
			$sequence = $this->data('table').'_'.$this->find(['type' => HNodeField::TypeId])->field().'_seq';
		}
		return $sequence;
	}
}