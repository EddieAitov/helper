<?php
class HFile extends HBase {
	const api = 1;
	function free_api(){
		return ['user_upload', 'user_remove'];
	}

	function user_upload($params){
		$params['user_mode'] = 1;
		return $this->upload($params);
	}

	function user_remove($params){
		$params['user_mode'] = 1;
		return $this->remove($params);
	}

	// upload for users and operator
	function upload($params){
		// params -> chat_id, message_id
		// $_FILES[filename][name][] $_FILES[filename][tmp_name][]

		// no files - ok
		if (!isset($_FILES['upload_file']) || !is_array($_FILES['upload_file']['name'])) throw new Exception('file_error_1');

		// $files = [[name,tmp_name,...],...]
		$files = array();
		foreach($_FILES['upload_file'] as $key1 => $value1)
			foreach($value1 as $key2 => $value2)
				$files[$key2][$key1] = $value2;

		// check chat
		if (!$this->parent()->chat()->check($params)) throw new Exception('file_error_2');

		// files path
		$base = __DIR__.'/../files';

		$out_files = [];

//		print_r($files);
//		echo end(explode('.', $files[0]	['name']));

		// process all files
		foreach ($files as $file){
			$ar = explode('.', $file['name']);
			if (preg_match('/^(php|phtml)/', end($ar))) throw new Exception('file_error_3');
			$query = HQuery::c($this)
				->insert_into('helper_files')
				->set('start_time', HQuery::Now)
				->set('chat_id', $params['chat_id'])
				->set('file_name', $file['name'])
				->returning('file_id');

			if (isset($params['user_mode'])){
				if (isset($params['user_unid']) && $params['user_unid']) $query->set('user_unid', $params['user_unid']);
				else $query->set('session_id', session_id());
			} else {
				$query->set('operator_id', $this->parent()->auth()->id());
			}

			$file_id = $query->fetchColumn();

			// create folder for file
			$top_level = floor($file_id / 1000);
			$dir = $base.'/'.$top_level;
			if (!file_exists($dir) && !is_dir($dir))
				if (!mkdir($dir)) throw new Exception('file_error_4');
			$dir = $base.'/'.$top_level.'/'.$file_id;
			if (!file_exists($dir) && !is_dir($dir))
				if (!mkdir($dir)) throw new Exception('file_error_5');

			// move file
			if (!move_uploaded_file($file['tmp_name'], $dir.'/'.$file['name'])) throw new Exception('file_error_6');
			// save id for frontend
			$out_files[] = [
				'file_id' => $file_id,
				'file_name' => $file['name'],
				'file_size' => $this->_size($file_id, $file['name'])
			];
		}

		return ['files' => $out_files];
	}

	static function _path($file_id, $file_name){
		return HELPER_SITE_URL.HELPER_PREFIX.'/files/'.floor($file_id / 1000).'/'.$file_id.'/'.$file_name;
	}

	static function _size($file_id, $file_name, $add_measure = true){
		$size = filesize(__DIR__.'/../files/'.floor($file_id / 1000).'/'.$file_id.'/'.$file_name);
		if (!$add_measure) return $size;

		$measure = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
		$i = 0;

		while ($size > 1024){
			$i++;
			$size /= 1024;
		}
		$size = round($size, 1);

		return $size.' '.$measure[$i];
	}

	function remove($params){
		$this->parent()->check()->numeric(['file_id'], $params);

		$query = HQuery::c($this)
			->select('1')
			->from('helper_files')
			->where_not_deleted()
			->where_equal('file_id', $params['file_id']);

		if (isset($params['user_mode'])){
			if (isset($params['user_unid']) && $params['user_unid']) $query->where_equal('user_unid', $params['user_unid']);
			else $query->where_equal('session_id', session_id());
		} else {
			$query->where_equal('operator_id', $this->parent()->auth()->id());
		}

		if ($query->fetchColumn()){
			HQuery::c($this)
				->update('helper_files')
				->set('end_time', HQuery::Now)
				->where_equal('file_id', $params['file_id'])
				->ex();

			return true;
		}

		return false;
	}

	// связывание файла с сообщением
	function _link($file_id, $chat_id, $message_id){
		HQuery::c($this)
			->update('helper_files')
			->set('message_id', $message_id)
			->where_not_deleted()
			->where_equal('file_id', $file_id)
			->where_equal('chat_id', $chat_id)
			->where_null('message_id')
			->ex();
	}

	function _list($params){
		$this->parent()->check()->numeric(['message_id'], $params);

		$data = HQuery::c($this)
			->select(['file_id', 'file_name'])
			->from('helper_files')
			->where_not_deleted()
			->where_equal('message_id', $params['message_id'])
			->fetchAll();

		foreach ($data as &$node){
			$node['file_size'] = $this->_size($node['file_id'], $node['file_name']);
		}

		return $data;
	}
}