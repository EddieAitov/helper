<?php
class HException extends Exception {
	protected $msg;

	function __construct($message = null, $error_info = ''){
		if (is_null($message)){
			$message = [
				'error' => '',
				'error_code' => ''
			];
		} elseif (!is_array($message)){
			$message = [
				'error' => w($message),
				'error_code' => $message,
				'error_info' => $error_info
			];
		}
		$this->msg = $message;
	}

	function message(){
		return $this->msg;
	}
}