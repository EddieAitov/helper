<?php
class HNode {
	const TypeRoot = -1;
	const TypeStandart = 0;
	const TypeSingleTag = 1;
	const TypeText = 2;
	const TypeDoctype = 3;
	const TypeEmpty = 4;

	/** @var HNode[] */
	protected $children = [];
	/** @var array */
	protected $attributes;
	/** @var int */
	protected $type;
	/** @var string */
	protected $name;
	/** @var bool */
	protected $update = false;
	/** @var array */
	protected $list = [];

	function __construct($type = HNode::TypeStandart, $name = '', $attributes = []){
		$this->type = $type;
		$this->name = $name;
		$this->attributes = $attributes;
	}

	protected $enabled = true;

	function disable(){
		$this->enabled = false;
	}

	static function c($type = HNode::TypeStandart, $name = '', $attributes = []){
		return new static($type, $name, $attributes);
	}

	static function withtype($type, $attributes){
		return new static($type, '', $attributes);
	}

	// TypeStandart может быть переопределен в потомках
	static function standart($name = '', $attributes = []){
		return new static(static::TypeStandart, $name, $attributes);
	}

	static function div($attributes = []){
		return HNode::standart('div', $attributes);
	}

	static function span($attributes = []){
		return HNode::standart('span', $attributes);
	}

	static function a($attributes = []){
		return HNode::standart('a', $attributes);
	}

	static function single($name = '', $attributes = []){
		return new static(HNode::TypeSingleTag, $name, $attributes);
	}

	static function text($string){
		return new self(HNode::TypeText, $string);
	}

	function setUpdater(){
		$this->update = true;
	}

	/**
	 * @param $child
	 * @return HNode
	 */
	function add($child){
		$this->children[] = $child;
		return $this;	// chaining
	}

	/**
	 * @descr div chain with text in the last one
	 * @param $classes
	 * @param $text
	 * @return HNode[]
	 */
	function chain($classes, $text = ''){
		$out = [];
		$current = $this;
		foreach ($classes as $class){
			$div = HNode::div(['class' => $class]);
			$out[] = $div;
			$current->add($div);
			$current = $div;
		}
		if (strlen($text)) $current->addText($text);

		return $out;
	}


	function addChildren($children, $class = null, $type = null){
		if ($class && $type){
			foreach ($children as $child) $this->add(new $class($type, '', $child));
		} else {
			foreach ($children as $child) $this->add($child);
		}
		return $this;
	}

	/**
	 * @param $string
	 * @return HNode
	 */
	function addText($string){
		return $this->add(HNode::text($string));
	}

	function clear(){
		$this->children = [];
		return $this;
	}

	function addSpace(){
		return $this->addText(' ');
	}

	function child($index){
		if (isset($this->children[$index])) return $this->children[$index];
		return false;
	}

	/** @return HNode */
	function firstChild(){
		return count($this->children) ? reset($this->children) : null;
	}

	/** @return HNode */
	function lastChild(){
		return count($this->children) ? end($this->children) : null;
	}

	function children(){
		return $this->children;
	}

	// getter & setter
	function attr($key, $value = null){
		if (!is_null($value)){
			$this->attributes[$key] = $value;
			return null;
		}
		if (isset($this->attributes[$key])) return $this->attributes[$key];
		return null;
	}

	function removeAttr($key){
		if (array_key_exists($key, $this->attributes)) unset($this->attributes[$key]);
		return $this;
	}

	function attributes(){
		return $this->attributes;
	}

	function type(){
		return $this->type;
	}

	function name(){
		return $this->name;
	}

	function find($selector){
		// default
		$class = '';
		$id = '';
		$name = $selector;
		// tag*.class
		if (preg_match('/^[0-9a-z]*\.[0-9a-z_\-]+$/i', $selector)){
			$array = explode('.', $selector);
			$name = $array[0];
			if (isset($array[1])) $class = $array[1];
		}
		// tag*#id
		if (preg_match('/^[0-9a-z]*#[0-9a-z_\-]+$/i', $selector)){
			$array = explode('#', $selector);
			$name = $array[0];
			if (isset($array[1])) $id = $array[1];
		}
		// поиск по прямым потомкам
		foreach ($this->children() as $child){
			if ($child->type() != $this::TypeText &&
				(!$name || $child->name() == $name) &&
				(!$class || $child->attr('class') == $class) &&
				(!$id || $child->attr('id') == $id)
			) return $child;
		}
		// поиск в глубь
		foreach ($this->children() as $child){
			$find = $child->find($selector);
			if (!is_null($find)) return $find;
		}

		return null;
	}

	/**
	 * @param bool $data
	 * @param bool|array $attributes
	 * @return string
	 */
	function show_attributes($data = false, $attributes = false){
		$out = [];
		if ($attributes === false) $attributes = $this->attributes();
		foreach ($attributes as $key => $value){
			if ($data) $key = 'data-'.$key;
			$out[] = $key.'="'.$value.'"';
		}
		$out = implode(' ', $out);
		if ($out) $out = ' '.$out;

		return $out;
	}

	// распечатка аттрибутов в json в data аттрибут
	function show_object_attr($key){
		$attributes = $this->attributes();
		foreach ($attributes as &$a)
			if (is_string($a)) $a = str_replace("'", "\\'", $a);

		$data = json_encode($attributes);
		$out = "data-{$key}='{$data}'";
		return $out;
	}

	/**
	 * @descr визуализация дерева
	 * @var HNode $node
	 * @return string
	 */
	function show(){
		$out = '';
		// open
		switch ($this->type()){
			case HNode::TypeStandart:
				$out .= '<'.$this->name().$this->show_attributes().'>';
				break;
			case HNode::TypeSingleTag:
				$out .= '<'.$this->name().$this->show_attributes().'/>';
				break;
			case HNode::TypeDoctype:
				$out .= '<!DOCTYPE html>'."\r\n";
				break;
			case HNode::TypeText:
				$out .= $this->name();
				break;
		}

		$out .= $this->show_children();

		// close
		switch ($this->type()){
			case HNode::TypeStandart:
				$out .= '</'.$this->name().'>';
				break;
		}

		return $out;
	}

	function show_children(){
		$out = '';
		foreach ($this->children() as $child) $out .= $child->show();
		return $out;
	}
}