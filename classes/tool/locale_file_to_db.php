<?php
class HToolLocale_file_to_db extends HToolAbstract {
	function execute($params = []){
		// обнуление значений перед загрузкой
		HQuery::c($this)
			->update('locale')
			->set('value', '')
			->ex();

		foreach (HLocale::getInstance()->localeTypes() as $locale_type){
			foreach (HLang::get_list() as $language){
				$filename = HLocale::getInstance()->getLocaleFilename($locale_type, $language);
				// файла с языком может еще не существовать - игнорируем и идем дальше
				if (!file_exists($filename)) continue;
				// file data
				$data = file_get_contents($filename);
				// php way
				if (in_array($locale_type, HLocale::getInstance()->localeBackend())){
					$data = preg_replace('/^.*\{\s+return\s+(array\s*\(.*?\))[^\)]*$/s', '$1', $data);
					$data = eval('return '.$data.';');
				// js way
				} else {
					$data = preg_replace('/^[^\{]*(\{.*?\})[^\}]*$/s', '$1', $data);
					$data = json_decode($data, true);
				}

				if (is_array($data)){
					foreach ($data as $key => $value){
						$ex = HQuery::c($this)
							->select('locale_id')
							->from('locale')
							->where_equal('locale.type_id', $locale_type)
							->where_equal('locale.key', $key)
							->where_not_deleted()
							->fetchColumn();

						// многомерные массивы недопустимы в hstore
						if (is_array($value)) $value = serialize($value);
						$value = $this->db()->pack([$language => $value]);
						// запись найдена в базе
						if ($ex){
							HQuery::c($this)
								->update('locale')
								->set('value', $value, true)
								->set('editor_id', 0)
								->set('update_time', HQuery::Now)
								->where_equal('locale.key', $key)
								->where_equal('locale.type_id', $locale_type)
								->ex();
							// новая запись
						} else {
							HQuery::c($this)
								->insert_into('locale')
								->set('type_id', $locale_type)
								->set('key', $key)
								->set('value', $value)
								->set('editor_id', 0)
								->set('update_time', HQuery::Now)
								->ex();
						}
					}
				}
			}
		}

		// remove unused values
		HQuery::c($this)
			->update('locale')
			->set('end_time', HQuery::Now)
			->where_equal('value', '')
			->ex();

		return parent::execute();
	}
}