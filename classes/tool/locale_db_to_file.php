<?php
class HToolLocale_db_to_file extends HToolAbstract {
	function execute($params = []){
		$data = HQuery::c($this)
			->select('*')
			->from('locale')
			->fetchAll();

		// init
		$locale_array = [];
		foreach (HLocale::getInstance()->localeTypes() as $locale_type){
			foreach (HLang::get_list() as $language){
				// locale_array[0][ru]
				$locale_array[$locale_type][$language] = [];
			}
		}

		// составление массивов значений
		foreach ($data as $node){
			$value_array = $this->db()->unpack($node['value']);
			foreach ($value_array as $language => $value){
				if ($value === '') continue;

				$un_value = @unserialize($value);
				$value = $value === 'b:0;' || $un_value !== false ? $un_value : $value;

				// десериализация массива при необходимости
				$locale_array[$node['type_id']][$language][$node['key']] = $value;
			}
		}

		// сохранение массивов в файлы
		foreach (HLocale::getInstance()->localeTypes() as $locale_type){
			foreach (HLang::get_list() as $language){
				// имя файла локализации
				$filename = HLocale::getInstance()->getLocaleFilename($locale_type, $language);
				// php way
				if (in_array($locale_type, HLocale::getInstance()->localeBackend())){
					$content = '<?php function locale(){ return '.var_export($locale_array[$locale_type][$language], true).';}';
				// js way
				} else {
					$content = 'var locale = '.json_encode($locale_array[$locale_type][$language]).';';
				}

				if (!file_put_contents($filename, $content)) throw new HException('unexpected-error: '.$filename);
			}
		}

		return parent::execute();
	}
}