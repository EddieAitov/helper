<?php
// one click tool
abstract class HToolAbstract extends HBase {
	protected $data;

	function __construct($parent, $data){
		parent::__construct($parent);
		$this->data = $data;
	}

	/**
	 * @param HBase $base
	 * @param $data
	 * @return static
	 */
	static function c($base, $data){
		return new static($base->parent(), $data);
	}

	function data($key){
		if (array_key_exists($key, $this->data)) return $this->data[$key];
		return null;
	}

	function disabled(){
		return $this->data('disabled') ? true : false;
	}

	function api(){
		return ['execute'];
	}

	function execute($params = []){
		return ['out' => w('operation-completed-successfully')];
	}
}