<?php
class HOperator extends HBase {
	const api = 1;

	function activity(){
		HQuery::c($this)
			->update('helper_operators')
			->set('activity_time', HQuery::Now)
			->where_equal('operator_id', $this->parent()->auth()->id())
			->ex();
	}
}