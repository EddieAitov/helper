<?php
class HBase {
	/** @var HController */
	protected $parent;

	function __construct($parent){
		$this->parent = $parent;
	}

	function parent(){
		return $this->parent;
	}

	function db(){
		return $this->parent()->db();
	}
}