<?php

/**
 * Класс для формирования модулей (не списков), сохранения и удаления данных
 * Class HManager
 */
class HManager extends HBase {
	function api(){
		return ['save', 'create_clone', 'create_clones', 'remove_entry', 'remove_entries', 'find_dublicate_alias'];
	}

	/**
	 * @param HModule $module
	 */
	function prepareModuleFields($module){
		foreach ($module->fields() as $field) $this->prepareModule($field);
	}

	/**
	 * @descr execute module queries
	 * @param HModule $module
	 */
	function prepareModule($module){
		// locking (prevent multi calling)
		if ($module->data('prepared')) return;
		$module->data('prepared', true);

		foreach ($module->data() as $key => $val){
			switch ($key){
				case 'linked':
					if (is_string($key)){
						$field = $module->data('linked_field') ? $module->data('linked_field') : $module->field();

						$rows = HQuery::c($this)
							->select([$field.' as id', 'name'])
							->from($val)
							->order('name');

						if ($this->parent()->lists()->fieldExists($val, 'end_time')) $rows->where_not_deleted();
						$rows = $rows->fetchAll();


						if ($module->data('linked_locale')){
							foreach ($rows as &$row){
								$row['name'] = $this->db()->unpack($row['name'])[HLang::helper_language()];
							}
						}
					} else { // complex linked
						$rows = [];
					}

					$module->data('rows', $rows);
					break;
				case 'query':
					$query = HQuery::c($this)
						->select([$val['id'].' as id', $val['name'].' as name'])
						->from($val['table'])
						->order('id');
					$rows = $query->fetchAll();

					$module->data('rows', $rows);
					break;
			}
		}
	}

	/**
	 * @descr module -> nodeField adapter for non-root modules
	 * @param HModule $column
	 * @param $value
	 * @return array
	 */
	function module2node($column, $value = null){
		$this->prepareModule($column);
		$out = [];
		foreach ($column->data() as $key => $val){
			switch ($key){
				// major params are set outside of attributes (->name, ->type), name is taken from m->name()
				case 'field':
				case 'type':
				case 'name':
				case 'query':
					break;
				default:
					$out[$key] = $val;
			}
		}

		$out['value'] = is_null($value) ? '' : $this->htmlValue($value);

		$out['label'] = $column->name();
		$out['allowed_languages'] = $this->parent()->auth()->languages();
		$out['current_language'] = $this->parent()->auth()->language();

		return $out;
	}

	function htmlValue($value){
		if (is_array($value)){
			foreach ($value as &$v) $v = htmlspecialchars($v);
		} else {
			$value = htmlspecialchars($value);
		}

		return $value;
	}

	/**
	 * @descr распаковка pg:hstore / mysql:json переменных в пхп
	 * @param HModule[] $columns
	 * @param array &$row
	 */
	function unpackLocale($columns, &$row){
		foreach ($columns as $column){
			if (!$column->data('locale')) continue;

			$row[$column->field()] = $this->db()->unpack($row[$column->field()]);
			foreach (HLang::get_list() as $language){
				if (!isset($row[$column->field()][$language])) $row[$column->field()][$language] = '';
			}
		}
	}

	/**
	 * @descr проверка условий, наложенных на раздел - можно ли редактировать выбранную запись
	 * @param HModule $module
	 * @param $row
	 * @return bool
	 */
	function checkRowEnabled($module, $row){
		// блокирующие условия для раздела
		$block = $module->data('block');
		// если заданы, нужно проверить текущую строку данных - блокировка на этом этапе имеет высший приоритет
		if ($block){
			foreach ($block as $field => $blocked_values){
				if (isset($row[$field]) && in_array($row[$field], $blocked_values)) return false;
			}
		}

		return true;
	}

	/**
	 * @param HModule $column
	 * @param bool $row_enabled
	 * @param bool $id_allowed
	 * @return bool
	 */
	function checkCellEnabled($column, $row_enabled, $id_allowed = false){
		// заблокирована вся строка и на данной колонке нет свойства unblockable
		if (!$row_enabled && !$column->data('unblockable')) return false;
		// колонка только для чтения и не случился спецслучай исключения для админа
		if ($column->data('readonly') && $this->action_type == HQuery::TypeUpdate &&
			(!$this->parent()->auth()->admin() || !$column->data('disable_readonly_for_admin'))
		) return false;
		// редактирование колонки доступно только для админа
		if ($column->data('admin_edit') && !$this->parent()->auth()->admin()) return false;
		// невозможно редактировать поле определенных типов
		if (!$id_allowed && in_array($column->type(), [HNodeField::TypeId])) return false;
		// во всех остальных случаях ячейку можно редактировать
		return true;
	}

	function checkAnyCellEnabled($columns, $row_enabled){
		foreach ($columns as $column){
			if ($this->checkCellEnabled($column, $row_enabled)) return true;
		}

		return false;
	}

	/**
	 * @descr reserve id for new record
	 * @param HModule $module
	 * @return int
	 */
	function reserveId($module){
		if (DB_TYPE == DB_POSTGRE){
			return $this->db()->fetchColumn("select nextval('{$module->sequence()}')");
		} else {
			$id = $this->db()->query("show table status like '{$module->table()}'")->fetch()['Auto_increment'];
			$this->db()->query("alter table `{$module->table()}` auto_increment = ".($id + 1));
			return $id;
		}
	}

	/**
	 * @param int $module_id
	 * @param int $id
	 * @param HModule $child
	 * @param $row
	 * @return HNodeField
	 */
	function nodeField($module_id, $id, $child, $row){
		$value = isset($row[$child->field()]) ? $row[$child->field()] : '';
		// using array for field name in this (default) method
		$node = new HNodeField($child->type(), [$module_id, $id, $child->field()], $this->module2node($child, $value));

		return $node;
	}

	// return_to_list
	function entryUpdated(){
		return isset($_SESSION['disposable']['entry_updated']);
	}

	// return_to_list
	function entryCloned(){
		return isset($_SESSION['disposable']['entry_cloned']);
	}

	function setEntryUpdated(){
		$_SESSION['disposable']['entry_updated'] = true;
	}

	function setEntryCloned(){
		$_SESSION['disposable']['entry_cloned'] = true;
	}

	/**
	 * @param int $module_id
	 * @return HModule
	 * @throws HException
	 */
	function getModule($module_id){
		$array = $this->parent()->configuration()->module($module_id);
		if (is_null($array)) throw new HException('module-not-found');
		return HModule::c($array);
	}

	/**
	 * @param int $tool_id
	 * @return HToolAbstract
	 * @throws HException
	 */
	function getTool($tool_id){
		$array = $this->parent()->configuration()->tool($tool_id);
		if (is_null($array)) throw new HException('module-not-found');
		$class = 'HTool'.ucfirst($array['class']);
		/** @noinspection PhpUndefinedMethodInspection */
		return $class::c($this, $array);
	}

	function getProfileModule(){
		return HModule::c($this->parent()->configuration()->profile());
	}

	/**
	 * @descr analog of "create"
	 * @return HNodeForm
	 */
	function create_profile(){
		$module = $this->getProfileModule();
		$this->action_type = HQuery::TypeUpdate;

		$form = HNodeForm::standart($module->name(), $module->data());
		$form->setButtons([HNodeForm::ButtonApply]);
		$form->attr('action_type', $this->action_type);
		$form->attr('entry_updated', $this->entryUpdated());

		// passing extra params in form (no field prefix)
		$form->add(HNodeField::hidden('action_type', $this->action_type));
		$form->add(HNodeField::hidden('profile', 1));

		$row = HQuery::c($this)
			->select('*')
			->from($module->table())
			->where_equal($module->findByType(HNodeField::TypeId)->field(), $this->parent()->auth()->id())
			->where_not_deleted()
			->fetch();

		// $module - root model
		// $form  - root view
		// $row - data
		$form->addChildren($this->nodeTree($module, $row));

		return $form;
	}

	/**
	 * @descr create manager node
	 * @return HNodeForm
	 * @throws HException
	 */
	protected $action_type;

	function create(){
		$module_id = (int)$this->parent()->router()->data(1);
		$module = $this->getModule($module_id);

		$id = (int)$this->parent()->router()->data(2);

		if ($id){
			$this->action_type = HQuery::TypeUpdate;
			if (!$module->canUpdate()) throw new HException('permission-denied');
			$module->setDelegate($this);
			$row = $module->getRow($id);
			if (!$row) throw new HException(404);
			// unpack before using in node
			$this->unpackLocale($module->children(), $row);
		} else {
			$this->action_type = HQuery::TypeInsert;
			if (!$module->canCreate()) throw new HException('permission-denied');
			$id = $this->reserveId($module);

			// setup id field
			$row = [$module->findByType(HNodeField::TypeId)->field() => $id];
		}

		$form = HNodeForm::standart($module->name(), $module->data());
		$form->toggleButton(HNodeForm::ButtonClone, $module->canCreate() && $this->action_type == HQuery::TypeUpdate);
		$form->toggleButton(HNodeForm::ButtonRemove, $module->canDelete() && $this->action_type == HQuery::TypeUpdate);
		$form->attr('action_type', $this->action_type);
		$form->attr('entry_updated', $this->entryUpdated());
		$form->attr('entry_cloned', $this->entryCloned());
		$form->attr('row_id', $id);

		// passing extra params in form (no field prefix)
		$form->add(HNodeField::hidden('module_id', $module_id));
		$form->add(HNodeField::hidden('action_type', $this->action_type));

		// $module - root model
		// $form  - root view
		// $row - data
		$form->addChildren($this->nodeTree($module, $row));

		return $form;
	}

	function setActionType($action_type){
		$this->action_type = $action_type;
	}

	/**
	 * @descr сбор потомков
	 * @param HModule $module
	 * @param array $row
	 * @param bool $parent_row_enabled
	 * @return HNode[]
	 */
	function nodeTree($module, $row, $parent_row_enabled = true){
		$row_enabled = $parent_row_enabled && $this->checkRowEnabled($module, $row);

		// create array of field nodes of this level
		$field_nodes = [];
		$composite_wrappers = [];

		// create nodes for all childs of module
		foreach ($module->children() as $child){
			// field
			if ($child->isField()){
				// create FIELD node
				$field_node = $this->nodeField($module->id(), $row[$module->findByType(HNodeField::TypeId)->field()], $child, $row);
				if (!$this->checkCellEnabled($child, $row_enabled)) $field_node->disable();
				$field_nodes[] = $field_node;
				// composite
			} else {
				$child_class = 'HNode'.ucfirst($child->data('class'));    // e.g. HNodeImage, not HNode
				/** @var HNodeAbstract $composite_wrapper */
				/** @noinspection PhpUndefinedMethodInspection */
				$composite_wrapper = $child_class::wrapper($child);    // wrapper is only VIEW
//				$composite_wrapper->attr('action_type', $this->action_type);	// insert/update difference for view
//				print_r($composite_wrapper);	// action_type во враппере!

				$child->setDelegate($this);    // e.g. HComponentImage
				// delegate executes method getRows (MODEL)
				$child_rows = $child->getRows($row[$module->field_id()]);

				if (count($child_rows)){
					foreach ($child_rows as $child_row){
						/** @var HNodeAbstract $child_node */
						/** @noinspection PhpUndefinedMethodInspection standart */
						// create COMPOSITE node
						$child_node = $child_class::listitem($child, $child_row);
						$cell_enabled = $this->checkCellEnabled($child, $row_enabled);
						if (!$cell_enabled) $child_node->disable();
						// action_type is set no listitem, not to wrapper
						$child_node->attr('action_type', $this->action_type);
						// RECURSION for composites
						$child_node->addChildren($this->nodeTree($child, $child_row, $cell_enabled));
						// add each node to the wrapper
						$composite_wrapper->add($child_node);
					}
				}

				// wrapper array
				$composite_wrappers[] = $composite_wrapper;
			}
		}

		// wrapper for fields
		if (count($composite_wrappers)){
			$field_wrapper = HNodeWrapper::field(w('information'));
			$field_wrapper->addChildren($field_nodes);
			// array of wrappers
			$out = array_merge([$field_wrapper], $composite_wrappers);
		} else {
			$field_wrapper = HNode::standart('div', ['class' => 'col-lg-6']);
			$field_wrapper->addChildren($field_nodes);
			// array of wrappers
			$out = [$field_wrapper];
		}

		return $out;
	}

	/**
	 * @descr batch cloning
	 * @param $params
	 * @return array
	 * @throws HException
	 */
	function create_clones($params){
		$this->parent()->check()->exist(['module_id', 'row_ids'], $params);
		$row_ids = explode(',', $params['row_ids']);
		$new_ids = [];

		foreach ($row_ids as $row_id){
			$new_ids[] = $this->create_clone([
				'module_id' => $params['module_id'],
				'row_id' => $row_id,
				'no_alert' => 1
			]);
		}

		return ['new_ids' => $new_ids];
	}

	/**
	 * @descr global entry point for clone. called once during request.
	 * @param $params
	 * @throws HException
	 * @return int
	 */
	function create_clone($params){
		$this->parent()->check()->exist(['module_id', 'row_id'], $params);
		// standart root module
		$module = $this->getModule((int)$params['module_id']);
		// setup all delegates for saving/removing data
		$module->setDelegateAll($this);
		// clone top row
		$new_id = $this->cloneRow($module, (int)$params['row_id'], null);
		// visual message
		if (!isset($params['no_alert'])) $this->setEntryCloned();

		return $new_id;
	}

	/**
	 * @param HModule $module
	 * @param $parent_id
	 * @param $new_parent_id
	 */
	protected function cloneModule($module, $parent_id, $new_parent_id){
		$module->cloneModule($parent_id, $new_parent_id);

		if (!$module->table()) return;

		$query = HQuery::c($this)
			->select($module->field_id())
			->from($module->table());
		$this->parent()->lists()->timeCondition($module, $query);
		$this->parent()->lists()->parentCondition($module, $query, $parent_id);
		$this->parent()->lists()->moduleCondition($module, $query);

		$ids = $query->fetchAllColumns();

		// cloning sub objects
		foreach ($ids as $id) $this->cloneRow($module, $id, $new_parent_id);
	}

	/**
	 * @param HModule $module
	 * @param $id
	 * @param $new_parent_id
	 * @throws HException
	 * @return int
	 */
	protected function cloneRow($module, $id, $new_parent_id){
		// operation possible if table is defined for module
		if (!$module->table()) return 0;

		$query = HQuery::c($this)
			->select('*')
			->from($module->table());
		$this->parent()->lists()->idCondition($module, $query, $id);
		$this->parent()->lists()->timeCondition($module, $query);
		$row = $query->fetch();
		if (!$row) throw new HException('cannot-find-data-for-clone');

		$query = HQuery::c($this);

		$module->setFieldIfNotExists(HNodeField::TypeModuleId, $row);
		$module->setFieldIfNotExists(HNodeField::TypeParentId, $row);
		$module->setFieldIfNotExists(HNodeField::TypeOrderby, $row);
		$module->setFieldIfNotExists(HNodeField::TypeUserId, $row);

		// doctrine compatibility. id should be pregenerated.
		$query->set($module->field_id(), $this->reserveId($module));

		// key->value
		foreach ($module->fields() as $field){
			// checking field
			if (!$this->checkCellEnabled($field, true)) continue;

			// replacing special values
			switch ($field->type()){
				case HNodeField::TypeUserId:
					$value = $this->parent()->auth()->id();
					break;
				case HNodeField::TypeParentId:
					$value = is_null($new_parent_id) ? $row[$field->field()] : $new_parent_id;
					break;
				default:
					$value = $row[$field->field()];
			}

			if ($field->type() == 'checkbox') $value = $value ? 't' : 'f';
			$query->set($field->field(), $value);
		}

		$query->insert_into($module->table());
		$this->setStartTime($module, $query);
		$query->returning($module->field_id());

//		$query->debug();
		$new_id = $query->fetchColumn();

		try {
			// module specific data saving using delegate
			$module->cloneRow($row, $new_id);
			// if an error occured in module we should remove main entry from db
		} catch (HException $e){
			HQuery::c($this)
				->delete_from($module->table())
				->where_equal($module->field_id(), $new_id)
				->ex();
			throw new HException($e->message());
		}

		// cloning children
		foreach ($module->composites() as $composite){
			$this->cloneModule($composite, $id, $new_id);
		}

		return $new_id;
	}

	/**
	 * @descr global entry point for save. called once during request.
	 * @param $params
	 * @throws HException
	 */
	function save($params){
		// get module for work and entry's id
		if (isset($params['profile']) && $params['profile']){
			// profile module
			$module = HModule::c($this->parent()->configuration()->profile());
			// user id
			$id = $this->parent()->auth()->id();
		} else {
			$this->parent()->check()->exist(['module_id'], $params);
			// standart root module
			$module = $this->getModule((int)$params['module_id']);
			// searching for id
			if (!count(array_keys($params['field'][$params['module_id']]))) throw new HException('row-not-found');
			$id = (int)array_keys($params['field'][$params['module_id']])[0];
		}

		// update by default
		if (!isset($params['action_type']) || $params['action_type'] != HQuery::TypeInsert) $params['action_type'] = HQuery::TypeUpdate;
		$this->setActionType($params['action_type']);

		// setup all delegates for saving/removing data
		$module->setDelegateAll($this);
		// start with row (not module) save
		$this->saveRow($module, $params, $id, $params['action_type']);
		// update profile cache
		if (isset($params['profile']) && $params['profile']) $this->parent()->auth()->_refresh_data();
	}

	/**
	 * @descr middle point (controller) for children composites
	 * @param HModule $module composite (not root)
	 * @param array $params global params
	 * @param int $parent_id
	 * @param int $action_type
	 */
	protected function saveModule($module, $params, $parent_id, $action_type){
		// custom behavior for class (available with non-table components)
		$module->saveModule($params, $parent_id, $action_type);

		// no table - do not continue
		if (!$module->table()) return;

		// get all existing rows for this parent_id
		$query = HQuery::c($this)
			->select($module->field_id())
			->from($module->table());

		$this->parent()->lists()->parentCondition($module, $query, $parent_id);
		$this->parent()->lists()->moduleCondition($module, $query);
		$this->parent()->lists()->timeCondition($module, $query);

		$existing_ids = $query->fetchAllColumns();

		// found row ids
		$found_ids = [];

		// add linking for submodules. parent_id and module_id fields are required for every submodule.
		$module->setFieldIfNotExists(HNodeField::TypeModuleId);
		$module->setFieldIfNotExists(HNodeField::TypeParentId);

		// add priority field
		if ($this->parent()->lists()->fieldExists($module->table(), HNodeField::TypeOrderby)){
			$orderby = 1;
			// add orderby field
			$module->setFieldIfNotExists(HNodeField::TypeOrderby);
		} else {
			$orderby = 0;
		}

		// processing all module rows
		if (isset($params['field'][$module->id()])){
			foreach (array_keys($params['field'][$module->id()]) as $row_id){
				// passing required params
				$params['field'][$module->id()][$row_id][$module->field_parent()] = $parent_id;
				$params['field'][$module->id()][$row_id][$module->field_module()] = $module->id();
				if ($orderby) $params['field'][$module->id()][$row_id][$module->field_order()] = $orderby++;

				// this entry was not deleted, update it
				if (in_array($row_id, $existing_ids)){
					$found_ids[] = $row_id;
					$this->saveRow($module, $params, $row_id, HQuery::TypeUpdate);
					// new entry - create it
				} else {
					$this->saveRow($module, $params, $row_id, HQuery::TypeInsert);
				}
			}
		}

		// deleted rows
		$diff = array_diff($existing_ids, $found_ids);
		foreach ($diff as $row_id) $this->removeRow($module, $row_id);
	}

	/**
	 * @descr saving one row
	 * @param HModule $module
	 * @param array $params
	 * @param int $id
	 * @param int $action_type
	 * @throws Exception
	 */
	protected function saveRow($module, $params, $id, $action_type){
		// operation possible if table is defined for module
		if (!$module->table()) return;

		// check
		if ($action_type == HQuery::TypeUpdate){
			// check requested entry exists
			$query = HQuery::c($this)
				->select('*')
				->from($module->table());

			$this->parent()->lists()->ownerCondition($module, $query);
			$this->parent()->lists()->idCondition($module, $query, $id);
			$this->parent()->lists()->timeCondition($module, $query);
			$this->parent()->lists()->staticConditions($module, $query);

			$existing_row = $query->fetch();
			if (!$existing_row) throw new HException('cannot-update-selected-entry');
			$row_enabled = $this->checkRowEnabled($module, $existing_row);
		} else {
			$row_enabled = true;
			// check if exists entry with the same id
			$query = HQuery::c($this)
				->select(1)
				->from($module->table());
			$this->parent()->lists()->idCondition($module, $query, $id);
			$existing_row = $query->fetch();
			if ($existing_row) throw new HException('cannot-insert-entry-with-this-id');
		}

		// main query
		$query = HQuery::c($this);

		// local data
		$row = $params['field'][$module->id()][$id];

		// key->value
		foreach ($module->fields() as $field){
			// checking field. id field is allowed (for insert)!
			if (!$this->checkCellEnabled($field, $row_enabled, true)) continue;
			// restrictions for update
			if ($action_type == HQuery::TypeUpdate){
				if (in_array($field->type(), [HNodeField::TypeUserId, HNodeField::TypeId])) continue;
				// not all fields may be passed during quick update from list
				if (isset($params['list_mode']) && $params['list_mode'] && !$field->isColumn()) continue;
			}

			switch ($field->type()){
				case HNodeField::TypeId:
					$value = $id;
					break;
				case HNodeField::TypeUserId:
					$value = $this->parent()->auth()->id();
					break;
				case HNodeField::TypeCheckbox:
					$value = isset($row[$field->field()]) && $row[$field->field()] ? 1 : 0;
					break;
				case HNodeField::TypeMultiSelect:
					if (isset($row[$field->field()]) && is_array($row[$field->field()])) $value = implode(',', $row[$field->field()]);
					else $value = '';
					break;
				case HNodeField::TypeDatetime:
					$value = $row[$field->field()];
					if (preg_match(HNodeField::dmYHi, $value)) $value .= ':00';
					$value = preg_replace(HNodeField::dmYHis, '$3-$2-$1 $4:$5:$6', $value);
					if (!$value) continue 2;
					if (!preg_match(HNodeField::YmdHis, $value)) throw new HException('bad-datetime-format'.$value);
					break;
				case HNodeField::TypeDate:
					$value = $row[$field->field()];
					$value = preg_replace(HNodeField::dmY, '$3-$2-$1', $value);
					if (!$value) continue 2;
					if (!preg_match(HNodeField::Ymd, $value)) throw new HException('bad-date-format'.$value);
					break;
				default:
					$value = $row[$field->field()];
			}

			if ($field->data('locale')) $value = $this->db()->pack($value);

			$dbtype = $this->parent()->lists()->fieldExists($module->table(), $field->field());

			// empty2zero fix
			if ($this->db()->isInt($dbtype) || $this->db()->isFloat($dbtype)){
				if (!$value) $value = 0;
			}

			if ($this->db()->isInt($dbtype)) $value = (int)$value;
			if ($this->db()->isFloat($dbtype)) $value = (float)$value;

			$query->set($field->field(), $value, $field->data('locale'));
		}

		// insert
		if ($action_type == HQuery::TypeInsert){
			$query->insert_into($module->table());
			$this->setStartTime($module, $query);
			// update
		} else {
			$query->update($module->table());
			$this->setUpdateTime($module, $query);
			$this->setEditorId($module, $query);
			$this->parent()->lists()->idCondition($module, $query, $id);
		}

//		$query->debug();
		// EXECUTE INSERT/UPDATE QUERY
		$query->ex();
		// module specific data saving using delegate
		$module->saveRow($params, $id, $action_type);

		// saving children (not available for quick (in list) save)
		if (!isset($params['list_mode']) || !$params['list_mode']){
			foreach ($module->composites() as $composite) $this->saveModule($composite, $params, $id, $action_type);
		}

//		// save для всех потомков
//		foreach ($composites as $composite){
//			if ($enabled || $composite->property('unblockable')) $composite->save($params, $this);
//		}

		if (isset($params['return']) && $params['return'] == 'item') $this->setEntryUpdated();
	}

	/**
	 * @descr при необходимости можно расширить функционал и использовать кастомные названия полей
	 * @param HModule $module
	 * @param HQuery &$query
	 */
	protected function setUpdateTime($module, $query){
		if ($this->parent()->lists()->fieldExists($module->table(), HNodeField::TypeUpdateTime))
			$query->set(HNodeField::TypeUpdateTime, HQuery::Now);
	}


	/**
	 * @param HModule $module
	 * @param HQuery &$query
	 */
	protected function setStartTime($module, $query){
		if ($this->parent()->lists()->fieldExists($module->table(), HNodeField::TypeStartTime))
			$query->set(HNodeField::TypeStartTime, HQuery::Now);
	}

	/**
	 * @param HModule $module
	 * @param HQuery &$query
	 */
	protected function setEditorId($module, $query){
		if ($this->parent()->lists()->fieldExists($module->table(), HNodeField::TypeEditorId))
			$query->set(HNodeField::TypeEditorId, $this->parent()->auth()->id());
	}

	/**
	 * @descr protected method with no checks
	 * @param HModule $module
	 * @param $row_id
	 */
	protected function removeRow($module, $row_id){
		// remove from delegate
		$module->removeRow($row_id);

		// remove children
		foreach ($module->composites() as $composite){
			$query = HQuery::c($this)
				->select($composite->findByType(HNodeField::TypeId)->field())
				->from($composite->table());
			$this->parent()->lists()->parentCondition($composite, $query, $row_id);
			$this->parent()->lists()->moduleCondition($composite, $query);
			$this->parent()->lists()->timeCondition($module, $query);

			$child_row_ids = $query->fetchAllColumns();
			foreach ($child_row_ids as $child_row_id) $this->removeRow($composite, $child_row_id);
		}

		// logical
		if ($this->parent()->lists()->fieldExists($module->table(), 'end_time')){
			HQuery::c($this)
				->update($module->table())
				->set('end_time', HQuery::Now)
				->where_equal($module->findByType(HNodeField::TypeId)->field(), $row_id)
//				->debug();
				->ex();
			// physical
		} else {
			HQuery::c($this)
				->delete_from($module->table())
				->where_equal($module->findByType(HNodeField::TypeId)->field(), $row_id)
				->ex();
		}
	}

	function remove_entry($params){
		$this->parent->check()->exist(['row_id', 'module_id'], $params);

		$module = $this->getModule((int)$params['module_id']);
		$module->setDelegateAll($this);

		$query = HQuery::c($this)
			->select(1)
			->from($module->table());
		$this->parent()->lists()->idCondition($module, $query, (int)$params['row_id']);
		$this->parent()->lists()->ownerCondition($module, $query);
		$this->parent()->lists()->timeCondition($module, $query);

		if (!$query->fetchColumn()) throw new HException('entry-not-found');

		$this->removeRow($module, (int)$params['row_id']);
	}

	/**
	 * @descr batch removing
	 * @param $params
	 * @return array
	 * @throws HException
	 */
	function remove_entries($params){
		$this->parent()->check()->exist(['module_id', 'row_ids'], $params);
		$row_ids = explode(',', $params['row_ids']);

		foreach ($row_ids as $row_id){
			$this->remove_entry([
				'module_id' => $params['module_id'],
				'row_id' => $row_id
			]);
		}
	}

	function find_dublicate_alias($params){
		// params[field_name], module_id, row_id
		$module = $this->getModule($params['module_id']);

		$alias_field = $module->find(['alias' => HModule::NotEmpty])->field();

		$query = HQuery::c($this)
			->select('1')
			->from($module->table())
			->where_equal($alias_field, $params['alias'])
			->where_not_equal($module->field_id(), $params['row_id']);
		$this->parent()->lists()->timeCondition($module, $query);
		$exists = $query->fetchColumn();

		return $exists;
	}
}