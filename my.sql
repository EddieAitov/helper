SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone="+00:00";

DROP TABLE IF EXISTS `helper_chats`;
CREATE TABLE `helper_chats` (
`chat_id` int NOT NULL auto_increment PRIMARY KEY,
`start_time` timestamp NULL default NULL,
`end_time` timestamp NULL default NULL,
`user_name` text,
`user_email` text,
`user_phone` text,
`operator_id` int,
`activity_time` timestamp NULL default NULL,
`type_id` int,
`ip` text NOT NULL,
`session_id` varchar(32) NOT NULL,
`user_unid` text,
`messenger_type_id` int,
`status_id` int
);

-- ----------------------------
-- Table structure for helper_groups
-- ----------------------------
DROP TABLE IF EXISTS `helper_groups`;
CREATE TABLE `helper_groups` (
`group_id` int NOT NULL auto_increment PRIMARY KEY,
`name` text NOT NULL,
`chat_type_ids` text
);

INSERT INTO `helper_groups` (
`group_id` ,
`name` ,
`chat_type_ids`
)
VALUES (
NULL ,  'Все типы', NULL
);


-- ----------------------------
-- Table structure for helper_messages
-- ----------------------------
DROP TABLE IF EXISTS `helper_messages`;
CREATE TABLE `helper_messages` (
`message_id` int NOT NULL auto_increment PRIMARY KEY,
`chat_id` int NOT NULL,
`start_time` timestamp NULL default NULL,
`end_time` timestamp NULL default NULL,
`operator_id` int,
`text` text NOT NULL,
`user_email` text,
`viewed` tinyint(1) DEFAULT 0 NOT NULL,
`delivered` tinyint(1) DEFAULT 0 NOT NULL
);

-- ----------------------------
-- Table structure for helper_operators
-- ----------------------------
DROP TABLE IF EXISTS `helper_operators`;
CREATE TABLE `helper_operators` (
`operator_id` int NOT NULL auto_increment PRIMARY KEY,
`name` text NOT NULL,
`login` varchar(32) NOT NULL,
`password` varchar(32) NOT NULL,
`start_time` timestamp NULL default NULL,
`end_time` timestamp NULL default NULL,
`blocked` tinyint(1) DEFAULT 0 NOT NULL,
`image_id` int,
`group_id` int,
`first_name` text,
`last_name` text,
`activity_time` timestamp NULL default NULL,
`admin` tinyint(1) DEFAULT 0 NOT NULL
);

-- ----------------------------
-- Table structure for helper_types
-- ----------------------------
DROP TABLE IF EXISTS `helper_types`;
CREATE TABLE `helper_types` (
`type_id` int NOT NULL auto_increment PRIMARY KEY,
`name` text NOT NULL
);
