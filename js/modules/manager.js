jhelper.manager = {
	// редактирование, добавление записи
	save: function(obj, callback){
		obj = $(obj);
		// form may be not DOM form element (tr, div)
		var form = obj.closest('.manager-form'),
			ret = $(obj).data('return');
		if (!callback){
			callback = function(){
				obj.find('i').attr('class', 'fa fa-check');

				// profile editing
				if (form.find('input[name=profile]').val() === '1'){
					switch (ret){
						case 'item':
							jhelper.navigation.profile();
							break;
					}
					// standart saving
				} else {
					var id = form.data('row_id'),
						module_id = form.find('input[name=module_id]').val();
					switch (ret){
						case 'list':
							jhelper.navigation.itemlist(module_id);
							break;
						case 'item':
							jhelper.navigation.manage(module_id, id);
							break;
						case 'new':
							jhelper.navigation.manage(module_id);
							break;
					}
				}
			};
		}

		if (CKEDITOR){
			for (var instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
		}

		obj.find('i').attr('class', 'fa fa-cog fa-spin');

		jhelper.ajax('manager/save', form.find('input, select, textarea').serialize() + '&return=' + ret, callback);
	},

	save_tr: function(obj){
		var button = $(obj).closest('.btn'),
			button_i = button.find('i'),
			callback = function(){
				button.removeClass('btn-warning').addClass('btn-success');
				button_i.attr('class', 'fa fa-check fa-fw');

				setTimeout(function(){
					button.removeClass('btn-success');
					button_i.attr('class', 'fa fa-save fa-fw');
				}, 1000);
				jhelper.alias.init();
			};

		button_i.attr('class', 'fa fa-cog fa-spin fa-fw');

		jhelper.manager.save(obj, callback);
	},

	row_ids: function(){
		var checkboxes = $('.dataTable td:first-of-type input[type=checkbox]:checked'),
			row_ids = [];
		for (var i = 0; i < checkboxes.length; i++){
			row_ids.push($(checkboxes[i]).closest('.manager-form').data('row_id'));
		}

		return row_ids.join(',');
	},

	remove_entry: function(obj){
		if (!confirm(w('confirm-remove'))) return;

		var form = $(obj).closest('.manager-form'),
			data = {
				row_id: form.data('row_id'),
				module_id: form.find('input[name=module_id]').val()
			};

		var callback = function(){
			jhelper.navigation.itemlist(data.module_id);
		};

		jhelper.ajax('manager/remove_entry', data, callback);
	},

	remove_entries: function(){
		if (!confirm(w('confirm-remove'))) return;

		var callback = function(){
			var dataTable = $('.dataTable').DataTable();
			$('.dataTable td:first-of-type input[type=checkbox]:checked').closest('tr').each(function(){
				dataTable.row(this).remove();
			});
			dataTable.draw();
			jhelper.table.reset_checkboxes();
		};

		jhelper.ajax('manager/remove_entries', 'row_ids=' + jhelper.manager.row_ids() + '&module_id=' + jhelper.options.routes[1], callback);
	},

	create_clone: function(obj){
		var module_id = jhelper.options.routes[1],
			row_id = $(obj).closest('.manager-form').data('row_id'),
			callback = function(response){
				jhelper.navigation.manage(module_id, response.out);
			};

		jhelper.ajax('manager/create_clone', 'module_id=' + module_id + '&row_id=' + row_id, callback);
	},

	create_clones: function(){
		var callback = function(){
			$('.dataTable').DataTable().ajax.reload();
			jhelper.table.reset_checkboxes();
		};

		jhelper.ajax('manager/create_clones', 'row_ids=' + jhelper.manager.row_ids() + '&module_id=' + jhelper.options.routes[1], callback);
	}
};