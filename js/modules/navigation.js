jhelper.navigation = {
	home: function(){
		location.href = jhelper.options.frontend_url;
	},

	itemlist: function(module_id){
		location.href = jhelper.options.frontend_url + 'itemlist/' + module_id;
	},

	manage: function(module_id, id){
		if (id == undefined) id = '';
		location.href = jhelper.options.frontend_url + 'manage/' + module_id + '/' + id;
	},

	profile: function(){
		location.href = jhelper.options.frontend_url + 'profile';
	},

	api: function(method){
		return jhelper.options.api_url + method;
	},

	reload: function(){
		location.reload();
	}
};