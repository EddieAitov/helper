jhelper.alias = {
	init: function(){
		$('*[data-alias]').each(function(){
			var alias = $(this),
				form = alias.closest('.manager-form'),
				row_id = form.data('row_id'),
				module_id = form.find('input[name=module_id]').val(),
				source_name = 'field[' + module_id + '][' + row_id + '][' + alias.data('alias') + ']',
				source = form.find('input[name="' + source_name + '"]'),
				action_type = form.find('input[name=action_type]').val();

			source.off('.alias').on('keyup.alias change.alias', function(){
				if (action_type == 2 || alias.data('changed')) return;

				var alias_value = jhelper.ru2en.translit($(this).val()).toLowerCase();
				alias_value = alias_value.replace(/[^a-z0-9]+/g, '-');
				alias_value = alias_value.replace(/^\-+/, '');
				alias_value = alias_value.replace(/\-+$/, '');
				alias.val(alias_value).trigger('change_source');
			});

			alias.off('.alias').on('keyup.alias change.alias change_source.alias', function(event){
				if (event.type != 'change_source') $(this).data('changed', 1);
				var callback = function(response){
					alias
						.parent().removeClass('has-warning').removeClass('has-feedback').attr('title', '')
						.find('.glyphicon-warning-sign').remove();

					if (response.out){
						alias
							.parent().addClass('has-warning').addClass('has-feedback').attr('title', w('alias-is-used'))
							.append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>');
					}
				};
				jhelper.ajax('manager/find_dublicate_alias', 'row_id=' + row_id + '&module_id=' + module_id + '&alias=' + $(this).val(), callback);
			}).trigger('change_source');
		});
	}
};

jhelper.register(jhelper.alias.init);