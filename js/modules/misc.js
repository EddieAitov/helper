$.extend(jhelper, {
	arrayUnique: function (array) {
		var a = array.concat();
		for (var i = 0; i < a.length; ++i) {
			for (var j = i + 1; j < a.length; ++j) {
				if (a[i] === a[j])
					a.splice(j--, 1);
			}
		}

		return a;
	},

	func: function (functionName) {
		var context = window;
		var namespaces = functionName.split('.');
		var func = namespaces.pop();
		for (var i = 0; i < namespaces.length; i++) {
			context = context[namespaces[i]];
		}
		return context[func];
	},

	ex: function (functionName, target) {
		return jhelper.func(functionName)(target);
	},

	message: function(text, type, callback){
		if (!type) type = 'message';
		jhelper.popup.set_data_and_show(w(type), text, callback);
	},

	timestamp: function(){
		var pad2 = function(n){ return n < 10 ? '0' + n : n },
			date = new Date();

		return date.getFullYear().toString() +
		pad2(date.getMonth() + 1) +
		pad2(date.getDate()) +
		pad2(date.getHours()) +
		pad2(date.getMinutes()) +
		pad2(date.getSeconds());
	},

	callback: {
		error: function(data){
			jhelper.message(data.error, 'error');
		},
		success: function(data){
			jhelper.message(data.out, 'message');
		}
	},

	scrollBottom: function(){
		$("html, body").animate({ scrollTop: $(document).height() }, 300);
	},

	getCookie: function(c_name) {
		if (document.cookie.length > 0) {
			var c_start = document.cookie.indexOf(c_name + "=");
			if (c_start != -1) {
				c_start = c_start + c_name.length + 1;
				var c_end = document.cookie.indexOf(";", c_start);
				if (c_end == -1) {
					c_end = document.cookie.length;
				}
				return decodeURIComponent(document.cookie.substring(c_start, c_end));
			}
		}
		return "";
	},

	ru2en: {
		ru_str : "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя",
		en_str : ['A','B','V','G','D','E','E','ZH','Z','I','Y','K','L','M','N','O','P','R','S','T',
			'U','F','H','TS','CH','SH','SCH','','Y','','E','YU','YA',
			'a','b','v','g','d','e','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u','f',
			'h','ts','ch','sh','sch','','y','','e','yu','ya'],
		translit : function(org_str) {
			var tmp_str = "";
			for(var i = 0, l = org_str.length; i < l; i++) {
				var s = org_str.charAt(i), n = this.ru_str.indexOf(s);
				if(n >= 0) { tmp_str += this.en_str[n]; }
				else { tmp_str += s; }
			}
			return tmp_str;
		}
	},

	init_misc: function(){
		setTimeout(function(){
			$('.entry-updated').slideUp(500);
		}, 4000);
	}
});

jhelper.register(jhelper.init_misc);