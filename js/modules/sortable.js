jhelper.sortable = {
	init: function(){
		$(function(){
			$('.sortable').sortable({
				distance: 5
			});
		});
	}
};

jhelper.register(jhelper.sortable.init);