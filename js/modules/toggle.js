jhelper.toggle = {
	init: function(){
		var v = 'toggle_' + jhelper.options.routes[1] + '_' + jhelper.options.routes[2] + '_';

		$('*[data-panel_toggle]').on('click', function(){
			var obj = $(this),
				i = obj.find('i'),
				panel = obj.closest('.panel'),
				module_id = panel.data('module_id'),
				elements = panel.children(':not(.panel-heading)'),
				hide;

			if (i.hasClass('fa-chevron-up')){
				i.removeClass('fa-chevron-up').addClass('fa-chevron-down');
				elements.hide();
				hide = 1;
			} else {
				i.removeClass('fa-chevron-down').addClass('fa-chevron-up');
				elements.show();
				hide = 0;
			}

			//noinspection JSValidateTypes
			document.cookie = v + module_id + '=' + hide;
		}).each(function(){
			var obj = $(this),
				panel = obj.closest('.panel'),
				module_id = panel.data('module_id');

			if (jhelper.getCookie(v + module_id) == 1) obj.click();
		});
	}
};

jhelper.register(jhelper.toggle.init);