jhelper.locale = {
	toggle_field_language: function(obj){
		obj = $(obj);
		var wrapper = obj.closest('.locale-container'),
			field = obj.data('field'),
			language = obj.data('language');

		// show/hide inputs
		wrapper.find('*[name^="' + field + '"]').hide();
		wrapper.find('*[name="' + field + '[' + language + ']"]').show().focus();

		// проход по всем переключателям, установка состояния
		$(obj).parent().children().each(function(){
			obj = $(this);
			// reset
			obj.attr('class', 'btn btn-xs');
			if (obj.data('language') == language) obj.addClass('btn-primary');
			else if (wrapper.find('*[name="' + obj.data('field') + '[' + obj.data('language') + ']"]').val().length) obj.addClass('btn-gray');
			else obj.addClass('btn-default');
		});
	},

	translate: function w(key, n){
		if (typeof locale == 'undefined' || locale[key] == undefined) return key;
		if (n == undefined) n = 1;
		var value = locale[key];
		if ($.isArray(value)){
			if (value.length == 2){
				return value[n - 1];
			} else if (value.length != 3){
				console.log('l10n error');
				return '';
			}
			var i = n % 10 == 1 && n % 100 != 11 ? 0 :
				(n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
			return value[i];
		} else {
			return value;
		}
	}
};

// short alias
var w = jhelper.locale.translate;