jhelper.table = (function(){
	var t = {
		init: function(){
			var data_table = $('.data-table');
			data_table.each(t.init_table);
			t.init_row_manager();
			t.init_checkboxes();
		},

		init_checkboxes: function(){
			var checkboxes = function(obj){
				return $(obj).closest('.dataTable').find('td:first-of-type input[type=checkbox]');
				},
				allChecked = function(obj){
					var c = checkboxes(obj), res = true;

					for (var i = 0; i < c.length; i++){
						if (!$(c[i]).is(':checked')){
							res = false;
							break;
						}
					}

					return res;
				},
				anyChecked = function(obj){
					var c = checkboxes(obj), res = false;

					for (var i = 0; i < c.length; i++){
						if ($(c[i]).is(':checked')){
							res = true;
							break;
						}
					}

					return res;
				},
				masterCheckbox = function(obj){
					return $(obj).closest('.dataTable').find('th:first-of-type input[type=checkbox]');
				},
				toggleActions = function(obj){
					if (anyChecked(obj)) $('.checkbox-dependent').removeAttr('disabled');
					else $('.checkbox-dependent').attr('disabled', 'disabled');
				},
				masterCheck = function(){
					$(this).add(checkboxes(this)).prop('checked', !allChecked(this));
					toggleActions(this);
				},
				slaveCheck = function(){
					masterCheckbox(this).prop('checked', allChecked(this));
					toggleActions(this);
				};

			$(document).on('change', '.dataTable th:first-of-type input[type=checkbox]', masterCheck);
			$(document).on('change', '.dataTable td:first-of-type input[type=checkbox]', slaveCheck);
		},

		reset_checkboxes: function(){
			$('.dataTable th:first-of-type input[type=checkbox], .dataTable td:first-of-type input[type=checkbox]').prop('checked', false);
			$('.checkbox-dependent').attr('disabled', 'disabled');
		},

		init_table: function(index, obj){
			var data_table = $(obj),
				wrapper = false;

			data_table.dataTable({
				deferRender: true,
				bProcessing: true,
				bServerSide: true,
				bStateSave: true,
				aoColumnDefs: [
					{
						bSortable: false,
						aTargets: ['nosort']
					}
				],
				// default order: id desc. 0-column is for checkboxes
				aaSorting: [[1, 'desc']],
				// data tables locale file
				language: {
					url: jhelper.options.helper_prefix + "/js/datatable.ru.json",
					processing: '<img src="' + jhelper.options.helper_prefix + '/images/circle-preloader.gif"/>'
				},
				// before send request
				fnServerData: function(sSource, aoData, fnCallback){
					// adapter
					var _aoData = {};

					// keys are passed incorrect - changing
					for (var key in aoData){
						_aoData[aoData[key].name] = aoData[key].value;
					}


					// adding missing_lang after reorder
					if (wrapper){
						var missing_lang = wrapper.find('.missing_lang');
						if (missing_lang.length && missing_lang.val().length) _aoData.missing_lang = missing_lang.val();
					}

					if (data_table.data('cache_missing_lang')) _aoData.missing_lang = data_table.data('cache_missing_lang');

					var callback = function (response) {
						if (response.error) {
							jhelper.callback.error(response);
						} else {
							fnCallback(response);
						}
					};

					jhelper.ajax('lists/itemlist/' + jhelper.options.routes[1], _aoData, callback);
				},
				// custom view elements
				fnDrawCallback: function(o){
					// init wrapper
					if (!wrapper) wrapper = data_table.closest('.dataTables_wrapper');

					var per_page = o._iDisplayLength,
						start = o._iDisplayStart,
						total = o._iRecordsDisplay,
						page = Math.ceil(start / per_page) + 1,
						page_count = Math.max(Math.ceil(total / per_page), 1);

					if (data_table.data('noscroll')) data_table.data('noscroll', 0);
					else $('html, body').animate({scrollTop: 0}, 300);

					var p_wrapper = wrapper.find('.dataTables_paginate'),
						p_div = $('<div/>', {class: "paginator-selector"}),
						p_select = $('<select/>', {class: "form-control", "data-change": "jhelper.table.change_page"});

					// adding page selector
					for (var i = 1; i <= page_count; i++){
						p_select.append($('<option/>', {value: i, selected: i == page}).append(i));
					}

					p_wrapper.append(p_div.append(p_select));

					//$('input[type=search]').prop('required', true);	// @fixed in jquery.dataTables.js

					// post draw actions
					jhelper.multi.init();
					jhelper.resize.update();
					// editable list tr2form fix
					data_table.find('tr').each(function(){
						var tr = $(this);
						tr.addClass('manager-form').data('row_id', tr.find('input[name=row_id]').val());
					});
					jhelper.alias.init();
					jhelper.datetime.init();
					data_table.find('td:last-of-type').addClass('actions-td').addClass('btn-toolbar');
					data_table.find('td:first-of-type').addClass('checkbox-td');
				},
				// after initialization. called once for each table (not called after redraw)
				fnInitComplete: function(){
					if (data_table.data('missing_lang')){
						var ml_wrapper = $('<label/>', {class: "missing_lang-wrapper"}).append(w('missing_lang-search') + ': '),
							ml_select = $('<select/>', {class: "form-control input-sm missing_lang", "data-change": "jhelper.table.redraw"});

						ml_select.append($('<option/>', {value: ""}).append('-'));

						$.each(jhelper.options.langs, function(k, v){
							ml_select.append($('<option/>', {
								value: v,
								selected: data_table.data('cache_missing_lang') == v
							}).append(v.toUpperCase()));
						});

						ml_wrapper.append(ml_select);
						wrapper.find('.dataTables_length').parent().append(ml_wrapper);
						data_table.data('cache_missing_lang', false);
					}
				},

				fnStateLoadParams: function(oSettings, oData){
					// caching param because visuals are not initialized
					if (oData.missing_lang) data_table.data('cache_missing_lang', oData.missing_lang);
				},
				fnStateSaveParams: function(oSettings, oData){
					// if wrapper is loaded - checking select's value
					if (wrapper) oData.missing_lang = wrapper.find('.missing_lang').val();
					// else getting cached value from fnStateLoadParams
					else oData.missing_lang = data_table.data('cache_missing_lang');
				}
			});

			//$(document).on('click', '.data-table tbody tr', function (){
			//	var tag = event.target.tagName.toLowerCase();
			//	if (tag != 'td' && tag != 'div') return;
			//	var row_id = $(this).find('input[name=row_id]').val();
			//	if (!row_id) return;
			//	jhelper.navigation.manage(jhelper.options.routes[1], row_id);
			//});

			// DataTable
			var table = data_table.DataTable();

			// apply the filter
			data_table.find('tfoot input, tfoot select').on('keyup change', function(){
				data_table.data('noscroll', 1);
				table
					.column($(this).parent().index() + ':visible')
					.search(this.value)
					.draw();
			});

			// default filter values
			for (var i = 0; i < table.columns().search().length; i++) {
				$(data_table.find('tfoot input, tfoot select')[i]).val(table.columns().search()[i + 1]);
			}
		},

		// инициализация для редактируемых списков
		init_row_manager: function (){
			$(document).on('input change', '.data-table input[name], .data-table select[name], .data-table textarea[name]', function(){
				$(this).closest('tr').find('*[data-action=save]').addClass('btn-warning');
			});
		},

		change_page: function(obj){
			$(obj).closest('.dataTables_wrapper').find('.dataTable').DataTable().page($(obj).val() - 1).draw(false);
		},

		redraw: function(obj){
			$(obj).closest('.dataTables_wrapper').find('.dataTable').DataTable().draw(false);
		}
	};

	return {
		init: t.init,
		reset_checkboxes: t.reset_checkboxes,
		change_page: t.change_page,
		redraw: t.redraw
	};
})();

jhelper.register(jhelper.table.init);