jhelper.ajax = function(method, data, callbackSuccess, callbackError){
	$.ajax({
		url: jhelper.navigation.api(method),
		data: data,
		type: 'post',
		dataType: 'json',
		success: function(response){
			if (response.error != undefined){
				if (callbackError != undefined){
					callbackError(response);
				} else {
					jhelper.callback.error(response);
				}
			} else {
				if (callbackSuccess != undefined){
					callbackSuccess(response);
				} else {
					jhelper.callback.success(response);
				}
			}
		}
	});
};