jhelper.fast_answer = {
	select: function(obj){
		obj = $(obj);
		var textarea = obj.closest('.answer').find('textarea[name=text]'),
			answer_id = obj.val();
		if (!parseInt(answer_id)) return;

		textarea.val(obj.find('option:selected').html());
		obj.val(0);

		jhelper.ajax('answer/inc_fast', 'answer_id=' + answer_id, function(){});
	}
};