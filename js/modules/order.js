jhelper.order = {
	state: 0,

	toggle: function(){
		if (jhelper.order.state == 1) jhelper.order.unload();
		else if (jhelper.order.state == 0) jhelper.order.init();
		// -1 for locking
	},

	init: function(){
		jhelper.order.state = -1;
		$('.checkbox-dependent').hide();
		$('.dynamic > .dataTables_wrapper').hide();
		$('.dynamic').append($('<img/>', {class: 'preloader', src: jhelper.options.helper_prefix + '/images/circle-preloader.gif'}));
		$('.order-toggle').removeClass('btn-default').addClass('btn-warning');
		var callback = function(response){
			$('.preloader').remove();
			$('.dynamic').append(response.out);
			$('.order-table').tableDnD({
				onDragClass: 'tr-drag',
				onDrop: function(table){
					jhelper.ajax('order/save/' + jhelper.options.routes[1], $(table).find('input[name="ids[]"]').serialize(), function(){});
				}
			});
		};
		jhelper.ajax('order/get/' + jhelper.options.routes[1], '', callback);
		jhelper.order.state = 1;
	},

	unload: function(){
		jhelper.order.state = -1;
		$('.checkbox-dependent').show();
		$('.dynamic > .dataTables_wrapper').show();
		$('.order-toggle').removeClass('btn-warning').addClass('btn-default');
		$('.order-table').remove();
		jhelper.order.state = 0;
	}
};