jhelper.blink_title = (function(){
	var bt = {
		original: "",
		phase: false,
		kill_timer: false,
		active: false,

		init: function(){
			bt.original = document.title;
		},

		setOriginal: function(text){
			bt.original = text;
		},

		start: function(timer_call){
			// блокировка новых внешних вызовов
			if (!timer_call && bt.active) return;
			bt.active = true;

			// получен сигнал остановки
			if (bt.kill_timer){
				bt.active = false;
				bt.kill_timer = false;
				document.title = bt.original;
				bt.phase = false;
				return;
			}

			document.title = (bt.phase ? '' : '[' + w('new-message') + '] ') + bt.original;
			bt.phase = !bt.phase;

			setTimeout(function(){
				bt.start(true);
			}, 700);
		},

		stop: function(){
			bt.active = false;
			bt.kill_timer = true;
		}
	};

	return {
		init: bt.init,
		start: bt.start,
		stop: bt.stop,
		setOriginal: bt.setOriginal
	};
})();

jhelper.register(jhelper.blink_title.init);