jhelper.password = {
	reset: function(obj){
		obj = $(obj);
		var wrapper = obj.closest('*[data-module_id]'),
			form = wrapper.closest('form');

		jhelper.ajax(wrapper.data('module_id') + '/reset', form.serialize());
	}
};