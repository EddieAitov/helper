jhelper.chat = {
	init: function(){
		if (jhelper.options.routes[0] != 'chat') return;

		$('*[data-chat]').each(function(){
			var wrapper = $(this),
				dynamic = wrapper.find('.dynamic');

			jhelper.chat.update(wrapper);
			jhelper.chat.scroll_bottom(dynamic);
			// подгрузка сообщений сверху
			dynamic.on('mousewheel DOMMouseScroll', jhelper.chat.scroll);
			// обновление сообщений на статус "просмотренные"
			$(document).on('mousemove', function(){
				jhelper.chat.update_to_viewed(wrapper);
			});
		});
	},

	// функция может быть вызвана по специальному событию
	update_to_viewed: function(wrapper){
		if (!wrapper.data('chat').not_viewed.length) return;
		// отправка и затирание текущего значения not_viewed
		jhelper.ajax('message/operator_viewed',
			'message_ids=' + JSON.stringify(wrapper.data('chat').not_viewed) +
			'&chat_id=' + wrapper.data('chat').chat_id, function(){});
		wrapper.data('chat').not_viewed = [];
	},

	start: function(obj){
		var wrapper = $(obj).closest('*[data-chat]'),
			callback = function(){
				$(obj).closest('.answer_overlay').fadeOut(300);
				wrapper.find('.change_operator, .change_status').fadeIn(300);
				wrapper.find('textarea[name=text]').focus();
			};
		jhelper.ajax('chat/set_operator', 'chat_id=' + wrapper.data('chat').chat_id, callback);
	},

	change_operator: function(obj){
		var wrapper = $(obj).closest('*[data-chat]'),
			operator_id = parseInt($('#new_operator_id').val());

		if (!operator_id){
			alert(w('operator-is-not-set'));
			return;
		}

		jhelper.ajax('chat/change_operator',
			'chat_id=' + wrapper.data('chat').chat_id +
			'&operator_id=' + operator_id,
			jhelper.navigation.home);
	},

	change_status: function(obj){
		var wrapper = $(obj).closest('*[data-chat]'),
			status_id = parseInt($('#new_status_id').val());

		if (!status_id){
			alert(w('status-is-not-set'));
			return;
		}

		jhelper.ajax('chat/change_status',
			'chat_id=' + wrapper.data('chat').chat_id +
			'&status_id=' + status_id,
			jhelper.navigation.reload);
	},

	// event handler
	scroll: function(event){
		var dynamic = $(event.target).closest('.dynamic'),
			wrapper = dynamic.closest('*[data-chat]'),
			scrollTop = dynamic[0].scrollTop,
			wheelDelta = event.originalEvent.wheelDelta;
		// когда происходит скролл выше верхнего предела
		if (scrollTop == 0 && wheelDelta > 0) jhelper.chat.update(wrapper, true);
	},

	update_top_lock: 0,
	update: function(wrapper, top){
		var chat_data = wrapper.data('chat');

		// при апдейте "сверху" не нужно зацикливать функцию
		if (top){
			// блокировка при частых запросах от пользователя
			if (jhelper.chat.update_top_lock) return;
			jhelper.chat.update_top_lock = 1;
		} else {
			// зацикливание для подгрузки сообщений снизу
			setTimeout(function(){
				jhelper.chat.update(wrapper);
			}, 2000);
		}

		var callback = function(response){
			// убираем лок в самом начале колбэка
			jhelper.chat.update_top_lock = 0;

			// обновляем данные чата (data -> data attrs)
			for (var key in response.data){
				var v = response.data[key];
				// специальные значения
				switch (key){
					case 'first_id':
						if (v && v < chat_data.first_id) chat_data.first_id = v;
						break;
					case 'last_id':
						if (v && v > chat_data.last_id) chat_data.last_id = v;
						break;
					case 'not_viewed':
						chat_data.not_viewed = jhelper.arrayUnique(chat_data.not_viewed.concat(v));
						break;
					default:
						chat_data[key] = v;
				}
			}

			var icon = wrapper.find('.status-icon');
			if (icon.hasClass('green') && !response.data.active) icon.removeClass('green').addClass('gray');
			else if (icon.hasClass('gray') && response.data.active) icon.removeClass('gray').addClass('green');

			// если передано обновленное название диалога, обновляем его и title
			if (wrapper.data('name') != response.name){
				wrapper.data('name', response.name); // сохраняем для проверки, чтобы лишний раз не обновлять DOM
				// header - полная версия, name - без тэгов для title
				wrapper.find('.chat-name').html(response.name);
				document.title = response.name;
				jhelper.blink_title.setOriginal(response.name);
			}

			// не было новых записей - дальнейшие действия не требуются
			if (!response.out.length) return;

			// динамический слой сообщений
			var dynamic = wrapper.find('.dynamic');
			// новые объекты
			var new_content = $(response.out).hide().fadeIn(100);
			// подгрузка сообщений сверху
			if (top) dynamic.prepend(new_content);
			// подгрузка новых сообщений
			else dynamic.append(new_content);
			// перелистывание к новым сообщениям (не работает, если страница не активна)
			if (!top) jhelper.chat.scroll_bottom(dynamic);
		};

		var callbackError = function(response){
			if (response.error_code == 'chat-not-found') jhelper.navigation.home();
			else jhelper.callback.error(response);
		};

		jhelper.ajax(
			'lists/operator_chat',
			'first_id=' + chat_data.first_id +
			// для обновления сверху будет запрошена другая сторона списка
			(top ? '' : '&last_id=' + chat_data.last_id) +
			'&chat_id=' + chat_data.chat_id,
			callback,
			callbackError
		);
	},

	scroll_bottom: function(dynamic){
		setTimeout(function(){
			dynamic[0].scrollTop = 99999;
		}, 150);
	},

	send: function(obj){
		var wrapper = $(obj).closest('*[data-chat]'),
			form = wrapper.find('form.answer-form'),
			textarea = form.find('textarea'),
			file_ids = form.find('input[name="file_ids[]"]'),
			chat_data = wrapper.data('chat');

		// не введен текст, не загружены файлы
		if (!textarea.val().length && !file_ids.length) return;

		var data = form.serialize() + '&chat_id=' + chat_data.chat_id;

		var clear = function(){
			textarea.val('');
			wrapper.find('.answer-files').empty();
			$(window).resize();
		};

		// пользователь находится на сайте, либо речь о мобильном приложении - СТАНДАРТНАЯ отправка
		if (chat_data.active || chat_data.messenger_type_id == 3){
			clear();
			jhelper.ajax('message/operator_send', data, function(){});
		// пользователь ушел с сайта и не указал e-mail - ОТМЕНА отправки
		} else if (!chat_data.user_email){
			jhelper.message(w('user-left-without-email'), 'error');
		// пользователь ушел с сайта и указал e-mail - отправка НА E-MAIL
		} else {
			// инициализация отправки e-mail
			var lock = 0,
				callback = function(){
					if (lock) return;
					lock = 1;

					clear();

					var send_callback = function(){
						jhelper.message(w('message-successfully-sent-to-email'));
					};

					jhelper.ajax('message/operator_send_email', data, send_callback);
				};
			jhelper.message(w('confirm-email-message'), 'confirm', callback);
		}
	}
};

jhelper.register(jhelper.chat.init);