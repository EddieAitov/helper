jhelper.resize = {
	init: function(){
		$(window).resize(jhelper.resize.update);
		jhelper.resize.update();
	},

	update: function(){
		var total_height = document.body.scrollHeight ? document.body.scrollHeight : $(window).height(),
			top_height = 51,
			header_height = $('.page-header').height() + 50;

		$('#page-wrapper').css('height', (total_height - top_height) + 'px');
		// only for chat
		if (jhelper.options.routes[0] == 'chat'){
			$('.dynamic').css('height', (total_height - top_height - ($('.answer').height() + 1) - header_height) + 'px');
		}
	}
};

jhelper.register(jhelper.resize.init);