jhelper.multi = {
	init: function(){
		$('select[multiple]').chosen({
			no_results_text: w('no-results-text'),
			placeholder_text_multiple: w('placeholder-text-multiple'),
			placeholder_text_single: w('placeholder-text-single'),
			width: "100%"
		});
	}
};

jhelper.register(jhelper.multi.init);