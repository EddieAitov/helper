jhelper.listeners = {
	enter: function(){
		$(document).on('keyup change', '*[data-enter]', function(event){
			if (event.keyCode == 13) jhelper.ex($(this).data('enter'), event.target);
		});
	},

	click: function(){
		$(document).on('click', '*[data-click]', function(event){
			jhelper.ex($(this).data('click'), event.target);
		});
	},

	change: function(){
		$(document).on('change', '*[data-change]', function(event){
			jhelper.ex($(this).data('change'), event.target);
		});
	},

	ctrlenter: function(){
		$(document).on('keyup change', '*[data-ctrlenter]', function(event){
			if (event.keyCode == 13 && event.ctrlKey) jhelper.ex($(this).data('ctrlenter'), event.target);
		});
	}
};

jhelper.register(function(){
	for (var a in jhelper.listeners) jhelper.listeners[a]();
});