jhelper.datetime = {
	init: function(){
		$(function(){
			$('input[data-datetime]').datetimepicker({
				language: 'ru'
			});
			$('input[data-date]').datetimepicker({
				language: 'ru',
				pickTime: false
			});
		});
	}
};

jhelper.register(jhelper.datetime.init);