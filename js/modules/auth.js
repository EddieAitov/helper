jhelper.auth = {
	signin: function(){
		var timestamp = jhelper.timestamp(),
			login = $('#login').val(),
			password = $('#password').val();

		password = jhelper.md5(password);
		var hash = jhelper.md5(password + ':' + timestamp);

		var callback = function(){
			$('#auth-form').submit();
		};

		jhelper.ajax('auth/signin', 'login=' + login + '&hash=' + hash + '&timestamp=' + timestamp, callback);
	},

	signout: function(){
		jhelper.ajax('auth/signout', '', jhelper.navigation.home);
	}
};