jhelper.file = (function(){
	var f = {
		// main init method
		init_upload: function(obj, bar, method, callback){
			var bar_progress = bar.find('.progress-bar');

			if (obj.prop("tagName").toLowerCase() != 'input'){
				obj.append($('<input/>', {
					type: "file",
					name: "upload_file[]",
					multiple: "multiple",
					class: "pseudo-hidden"
				}));
			}

			obj.fileupload({
				url: jhelper.navigation.api(method),
				dataType: 'json',
				// called after selecting files
				add: function (e, data) {
					//bar.show();
					data.submit();
				},
				// uploading error - extended error information
				error: function(data){
					jhelper.message(w('error-while-uploading') + '<br/><b>' + data.status + ': ' + data.statusText + '</b>', 'error');
				},
				// summary progress
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					bar_progress.css('width', progress + '%');
					// finished - add delayed fadeOut
					if (progress == 100){
						//bar.delay(1000).fadeOut(500);
						// reset progress while invisible
						setTimeout(function(){
							bar_progress.addClass('notransition');
							bar_progress.css('width', 0);
							//noinspection BadExpressionStatementJS
							bar_progress[0].offsetHeight;
							bar_progress.removeClass('notransition');
						}, 1500);
					}
				},
				// finish
				done: function (e, data) {
					if (data.result.error) {
						jhelper.callback.error(data.result);
					}

					if (parseInt(data.result.out) > 0){
						jhelper.message(w('error-occured-error-code') + data.result.out, 'error');
						return;
					}

					// custom callback
					callback(data.result);
				}
			});
		},

		// chat
		init_chat: function(){
			$('.file').each(function(){
				var obj = $(this),
					button = obj.closest('.fileinput-button'),
					bar = button.closest('.tbl').find('.progress');//.hide();

				var callback = function(response){
					var container = $('.answer-files');
					// add file into DOM
					for (var i in response.files) {
						var file = response.files[i],
							li = $('<li/>')
								.append(file.file_name + ' (' + file.file_size + ') ')
								.append($('<i/>', {
									class: "glyphicon glyphicon-remove-sign pointer",
									"data-file_id": file.file_id,
									"data-method": "file/remove",
									"data-click": "jhelper.file.remove"
								}))
								.append($('<input/>', {
									type: "hidden",
									name: "file_ids[]",
									value: file.file_id
								}));
						container.append(li);
					}
					// trigger event
					$(window).trigger('resize');
					jhelper.scrollBottom();
				};

				f.init_upload(obj, bar, 'file/upload', callback);
			});
		},

		// main upload initialization
		init_standart: function(){
			$('.file-upload').each(function(){
				var obj = $(this),
					wrapper = obj.closest('.panel'),
					content = wrapper.find('.wrapper-content'),
					bar = obj.closest('.tbl').find('.progress');//.hide();

				var callback = function(response){
					// add elements into DOM
					content.append(response.out);
					$(window).trigger('resize');
				};

				// todo: select-all, remove-selected

				f.init_upload(obj, bar, obj.data('module_id') + '/upload', callback);
			});
		},

		init: function() {
			$(function(){
				f.init_chat();
				f.init_standart();
				f.init_selectable();
			});
		},

		init_selectable: function(){
			$('.sortable').each(function(){
				var elements_wrapper = $(this),
					wrapper = elements_wrapper.closest('*[data-module_id]'),
					select_all = wrapper.find('.select-all'),
					remove_selected = wrapper.find('.remove-selected');

				var any = function(){
					return elements_wrapper.find('.thumbnail>input[type=checkbox].selected').length ? true : false;
				};

				var anySelected = function(){
					return elements_wrapper.find('.thumbnail>input[type=checkbox].selected:checked').length ? true : false;
				};

				var allSelected = function(){
					return elements_wrapper.find('.thumbnail>input[type=checkbox].selected:not(:checked)').length ? false : true;
				};

				elements_wrapper.on('change', 'input.selected', function(){
					remove_selected.prop('disabled', !anySelected());
				});

				select_all.on('click', function(){
					elements_wrapper.find('.thumbnail>input[type=checkbox]').prop('checked', !allSelected()).change();
				});

				remove_selected.on('click', function(){
					var i = 0;
					elements_wrapper.find('.thumbnail>input[type=checkbox].selected:checked').each(function(){
						f.remove_dom(this, i++);
					});
				});
			});
		},

		// only for custom modules. standart remove is 2-stepped.
		remove: function(obj){
			if (!confirm(w('confirm-deleting-file'))) return;

			obj = $(obj);
			var callback = function(data){
				if (data.out){
					obj.parent().fadeOut(500);
					setTimeout(function(){
						obj.parent().remove();
						$(window).trigger('resize');
					}, 500);
				} else {
					jhelper.message(w('error-deleting-file'), 'error');
				}
			};

			jhelper.ajax(obj.data('method'), 'file_id=' + obj.data('file_id'), callback)
		},

		// remove dom only (for second step)
		remove_dom: function(obj, do_not_ask){
			if (!do_not_ask && !confirm(w('confirm-deleting-file'))) return;

			obj = $(obj);
			var wrapper = obj.closest('.element-wrapper');
			wrapper.find('input.selected').prop('checked', false).change();
			wrapper.fadeOut(500);
			setTimeout(function(){
				wrapper.remove();
				$(window).trigger('resize');
			}, 500);
		}
	};

	return {
		init: f.init,
		remove: f.remove,
		remove_dom: f.remove_dom
	};
})();

jhelper.register(jhelper.file.init);