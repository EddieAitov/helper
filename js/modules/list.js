jhelper.list = {
	more: function(obj){
		obj = $(obj);
		var wrapper = obj.closest('.row').find('.list-wrapper'),
			callback = function(response){
				var more = true;
				// если передается first_id, работаем с ним, иначе с last_id
				if (obj.data('first_id')){
					if (response.data.first_id) obj.data('first_id', response.data.first_id);
					else more = false;
				} else if (obj.data('last_id')){
					if (response.data.last_id) obj.data('last_id', response.data.last_id);
					else more = false;
				}
				if (!more) obj.fadeOut(500);
				wrapper.append(response.out);
			},
			data = '';

		if (obj.data('first_id')) data += '&first_id=' + obj.data('first_id');
		else if (obj.data('last_id')) data += '&last_id=' + obj.data('last_id');

		jhelper.ajax('lists/' + obj.data('list'), data, callback);
	}
};