jhelper.popup = {
	init: function(){
		$(document).on('click', '*[data-popup]', function(){
			jhelper.popup.open($(this).data('popup'), this, $(this).data('params') ? $(this).data('params') : '');
		});
	},

	set_data_and_show: function(title, text, callback){
		$('#modal-title').text(title);
		$('#modal-body').html(text);
		// callback для кнопки "подтвердить"
		if (callback){
			$('#modal-accept')
				.show()
				.off()
				.on('click', callback);
		} else {
			$('#modal-accept').hide();
		}
		$('#modal').modal('show');
	},

	open: function(method, sender, params){
		var callback = function(response){
			// если задан колбек, получаем функцию из строки, оборачиваем вызов в анонимную функцию, чтобы передать sender'а
			var callback = response.callback ? function(){ jhelper.func(response.callback)(sender); } : false;
			jhelper.popup.set_data_and_show(response.title, response.text, callback);
		};
		jhelper.ajax('popup/' + method, params, callback);
	},

	close: function(){
		$('#modal').modal('hide');
	}
};

jhelper.register(jhelper.popup.init);