// динамически обновляемые стандартные элементы (без специальных событий)
jhelper.updater = {
	init: function(){
		$('*[data-update]').each(function(){
			var obj = $(this),
				html = false,
				callback = function(response){
					if (html != response.out) obj.html(response.out);
					html = response.out;
					// дополнительный функционал при обновлении
					if (response.info.attention) jhelper.blink_title.start();
					else jhelper.blink_title.stop();
				},
				update = function(){
					setTimeout(update, 4000);
					jhelper.ajax('builder/update', {
						element: obj.data('update'),
						// при запросе обязательно нужно знать текущую страницу, эмуляция frontend режима
						routes: jhelper.options.routes
					}, callback);
				};
			update();
		});
	}
};

jhelper.register(jhelper.updater.init);