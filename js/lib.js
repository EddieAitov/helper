(function(main){
	var registry = []; // collection of module

	// adds module to collection
	main.register = function(moduleDeclaration){
		registry.push(moduleDeclaration);
	};

	// called from DOM
	main.setOptions = function(options){
		main.options = $.extend({
			routes: [],
			helper_prefix: "",
			helper_user_friendly_urls: 0,
			langs: []
		}, options);

		main.options.api_url = main.options.helper_user_friendly_urls ?
			main.options.helper_prefix + '/api/' :
			main.options.helper_prefix + '/index.php?r=api/';

		main.options.frontend_url = main.options.helper_user_friendly_urls ?
			main.options.helper_prefix + '/' :
			main.options.helper_prefix + '/index.php?r=';
	};

	// called after DOM is loaded
	main.init = function(){
		// loop through each module and execute
		for(var i = 0; i < registry.length; i++){
			// "this" is context for registry[i]
			registry[i].call(this);
		}
	};
}(window.jhelper = window.jhelper || {}));

$(document).ready(jhelper.init);