/*
Navicat PGSQL Data Transfer

Source Server         : snub-main
Source Server Version : 90304
Source Host           : localhost:5432
Source Database       : snub-eu
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90304
File Encoding         : 65001

Date: 2014-10-27 10:15:10
*/


-- ----------------------------
-- Sequence structure for helper_chats_chat_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_chats_chat_id_seq";
CREATE SEQUENCE "helper_chats_chat_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_chats_chat_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_fast_answers_answer_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_fast_answers_answer_id_seq";
CREATE SEQUENCE "helper_fast_answers_answer_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_fast_answers_answer_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_files_file_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_files_file_id_seq";
CREATE SEQUENCE "helper_files_file_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_files_file_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_forwards_forward_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_forwards_forward_id_seq";
CREATE SEQUENCE "helper_forwards_forward_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_forwards_forward_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_groups_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_groups_group_id_seq";
CREATE SEQUENCE "helper_groups_group_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_groups_group_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_images_image_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_images_image_id_seq";
CREATE SEQUENCE "helper_images_image_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_images_image_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_log_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_log_log_id_seq";
CREATE SEQUENCE "helper_log_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_log_log_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_messages_message_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_messages_message_id_seq";
CREATE SEQUENCE "helper_messages_message_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_messages_message_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_operators_operator_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_operators_operator_id_seq";
CREATE SEQUENCE "helper_operators_operator_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_operators_operator_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_reset_password_notifications_notification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_reset_password_notifications_notification_id_seq";
CREATE SEQUENCE "helper_reset_password_notifications_notification_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_reset_password_notifications_notification_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for helper_types_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "helper_types_type_id_seq";
CREATE SEQUENCE "helper_types_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."helper_types_type_id_seq"', 1, true);

-- ----------------------------
-- Table structure for helper_chats
-- ----------------------------
DROP TABLE IF EXISTS "helper_chats";
CREATE TABLE "helper_chats" (
"chat_id" int4 DEFAULT nextval('helper_chats_chat_id_seq'::regclass) NOT NULL,
"start_time" timestamp(6),
"end_time" timestamp(6),
"user_name" text,
"user_email" text,
"user_phone" text,
"operator_id" int4,
"activity_time" timestamp(6),
"type_id" int4,
"ip" text NOT NULL,
"session_id" varchar(32) NOT NULL,
"user_unid" text,
"messenger_type_id" int4,
"status_id" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_fast_answers
-- ----------------------------
DROP TABLE IF EXISTS "helper_fast_answers";
CREATE TABLE "helper_fast_answers" (
"answer_id" int4 DEFAULT nextval('helper_fast_answers_answer_id_seq'::regclass) NOT NULL,
"text" text NOT NULL,
"short_text" text,
"start_time" timestamp(6),
"end_time" timestamp(6),
"use_count" int4 DEFAULT 0 NOT NULL,
"a" text,
"b" bool
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_files
-- ----------------------------
DROP TABLE IF EXISTS "helper_files";
CREATE TABLE "helper_files" (
"file_id" int4 DEFAULT nextval('helper_files_file_id_seq'::regclass) NOT NULL,
"file_name" text NOT NULL,
"chat_id" int4 NOT NULL,
"message_id" int4,
"operator_id" int4,
"user_unid" text,
"session_id" text,
"start_time" timestamp(6) NOT NULL,
"end_time" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_forwards
-- ----------------------------
DROP TABLE IF EXISTS "helper_forwards";
CREATE TABLE "helper_forwards" (
"forward_id" int4 DEFAULT nextval('helper_forwards_forward_id_seq'::regclass) NOT NULL,
"chat_id" int4 NOT NULL,
"from_operator_id" int4 NOT NULL,
"operator_id" int4 NOT NULL,
"time" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_groups
-- ----------------------------
DROP TABLE IF EXISTS "helper_groups";
CREATE TABLE "helper_groups" (
"group_id" int4 DEFAULT nextval('helper_groups_group_id_seq'::regclass) NOT NULL,
"name" text NOT NULL,
"chat_type_ids" text,
"permissions" text,
"languages" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_images
-- ----------------------------
DROP TABLE IF EXISTS "helper_images";
CREATE TABLE "helper_images" (
"image_id" int4 DEFAULT nextval('helper_images_image_id_seq'::regclass) NOT NULL,
"module_id" int4 NOT NULL,
"parent_id" int4 NOT NULL,
"start_time" timestamp(6) NOT NULL,
"end_time" timestamp(6),
"filename" text NOT NULL,
"orderby" int2 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_log
-- ----------------------------
DROP TABLE IF EXISTS "helper_log";
CREATE TABLE "helper_log" (
"log_id" int4 DEFAULT nextval('helper_log_log_id_seq'::regclass) NOT NULL,
"date" timestamp(6) NOT NULL,
"ip" text NOT NULL,
"type" int2 NOT NULL,
"data" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_messages
-- ----------------------------
DROP TABLE IF EXISTS "helper_messages";
CREATE TABLE "helper_messages" (
"message_id" int4 DEFAULT nextval('helper_messages_message_id_seq'::regclass) NOT NULL,
"chat_id" int4 NOT NULL,
"start_time" timestamp(6),
"end_time" timestamp(6),
"operator_id" int4,
"text" text NOT NULL,
"user_email" text,
"viewed" bool DEFAULT false NOT NULL,
"delivered" bool DEFAULT false NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_operators
-- ----------------------------
DROP TABLE IF EXISTS "helper_operators";
CREATE TABLE "helper_operators" (
"operator_id" int4 DEFAULT nextval('helper_operators_operator_id_seq'::regclass) NOT NULL,
"login" varchar(32) NOT NULL,
"password" varchar(32) NOT NULL,
"start_time" timestamp(6),
"end_time" timestamp(6),
"blocked" bool DEFAULT false NOT NULL,
"image_id" int4,
"group_id" int4,
"first_name" text,
"last_name" text,
"activity_time" timestamp(6),
"admin" bool DEFAULT false NOT NULL,
"email" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_reset_password_notifications
-- ----------------------------
DROP TABLE IF EXISTS "helper_reset_password_notifications";
CREATE TABLE "helper_reset_password_notifications" (
"notification_id" int4 DEFAULT nextval('helper_reset_password_notifications_notification_id_seq'::regclass) NOT NULL,
"create_time" timestamp(6) NOT NULL,
"user_id" int4 NOT NULL,
"initiator_id" int4 NOT NULL,
"email" text
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for helper_types
-- ----------------------------
DROP TABLE IF EXISTS "helper_types";
CREATE TABLE "helper_types" (
"type_id" int4 DEFAULT nextval('helper_types_type_id_seq'::regclass) NOT NULL,
"name" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "helper_chats_chat_id_seq" OWNED BY "helper_chats"."chat_id";
ALTER SEQUENCE "helper_fast_answers_answer_id_seq" OWNED BY "helper_fast_answers"."answer_id";
ALTER SEQUENCE "helper_files_file_id_seq" OWNED BY "helper_files"."file_id";
ALTER SEQUENCE "helper_forwards_forward_id_seq" OWNED BY "helper_forwards"."forward_id";
ALTER SEQUENCE "helper_groups_group_id_seq" OWNED BY "helper_groups"."group_id";
ALTER SEQUENCE "helper_images_image_id_seq" OWNED BY "helper_images"."image_id";
ALTER SEQUENCE "helper_log_log_id_seq" OWNED BY "helper_log"."log_id";
ALTER SEQUENCE "helper_messages_message_id_seq" OWNED BY "helper_messages"."message_id";
ALTER SEQUENCE "helper_operators_operator_id_seq" OWNED BY "helper_operators"."operator_id";
ALTER SEQUENCE "helper_reset_password_notifications_notification_id_seq" OWNED BY "helper_reset_password_notifications"."notification_id";
ALTER SEQUENCE "helper_types_type_id_seq" OWNED BY "helper_types"."type_id";

-- ----------------------------
-- Primary Key structure for table helper_chats
-- ----------------------------
ALTER TABLE "helper_chats" ADD PRIMARY KEY ("chat_id");

-- ----------------------------
-- Primary Key structure for table helper_fast_answers
-- ----------------------------
ALTER TABLE "helper_fast_answers" ADD PRIMARY KEY ("answer_id");

-- ----------------------------
-- Primary Key structure for table helper_files
-- ----------------------------
ALTER TABLE "helper_files" ADD PRIMARY KEY ("file_id");

-- ----------------------------
-- Primary Key structure for table helper_forwards
-- ----------------------------
ALTER TABLE "helper_forwards" ADD PRIMARY KEY ("forward_id");

-- ----------------------------
-- Primary Key structure for table helper_groups
-- ----------------------------
ALTER TABLE "helper_groups" ADD PRIMARY KEY ("group_id");

-- ----------------------------
-- Primary Key structure for table helper_images
-- ----------------------------
ALTER TABLE "helper_images" ADD PRIMARY KEY ("image_id");

-- ----------------------------
-- Primary Key structure for table helper_log
-- ----------------------------
ALTER TABLE "helper_log" ADD PRIMARY KEY ("log_id");

-- ----------------------------
-- Primary Key structure for table helper_messages
-- ----------------------------
ALTER TABLE "helper_messages" ADD PRIMARY KEY ("message_id");

-- ----------------------------
-- Primary Key structure for table helper_operators
-- ----------------------------
ALTER TABLE "helper_operators" ADD PRIMARY KEY ("operator_id");

-- ----------------------------
-- Primary Key structure for table helper_reset_password_notifications
-- ----------------------------
ALTER TABLE "helper_reset_password_notifications" ADD PRIMARY KEY ("notification_id");

-- ----------------------------
-- Primary Key structure for table helper_types
-- ----------------------------
ALTER TABLE "helper_types" ADD PRIMARY KEY ("type_id");
