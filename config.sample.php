<?php
// remove .sample and store file named config.php in helper's root directory with these settings.
// config.sample.php is not used anywhere

// set to true if chat module is needed
define('HELPER_CHAT_ENABLED', 0);
// extra debug information will be outputted in helper if option is set to true
define('HELPER_DEBUG', 1);
// messages loaded in chat by default
define('HELPER_MESSAGES_COUNT', 30);
// chat count by default
define('HELPER_CHATLIST_COUNT', 30);
// username in email from chat operator to user
define('HELPER_RETURN_NAME', 'HELPER');
// email address in email from chat operator to user
define('HELPER_RETURN_ADDRESS', 'no-reply@example.com');
// database settings
define('DB_HOST', '');
define('DB_USER', '');
define('DB_PASSWORD', '');
define('DB_NAME', '');

// helper DB types - do not change these 2 defined values
define('DB_POSTGRE', 1);
define('DB_MYSQL', 2);
// set current db type - DB_POSTGRE or DB_MYSQL. some features (multilanguage support) is not available in mysql.
define('DB_TYPE', DB_POSTGRE);

// sms
define('SMS_URL', 'http://smsc.ru/sys/send.php?login=<login>&psw=<password>&phones=<phones>&mes=<message>&sender=<sender>&charset=utf-8');
define('SMS_LOGIN', '');
define('SMS_PASSWORD', '');
define('SMS_SENDER', '');

// helper path from document (www) root
define('HELPER_PREFIX', '/helper');
// path for file uploads - do not forget to create these folder and folder "redactor" inside it!
define('HELPER_UPLOAD_FOLDER', '/files');

// web site url without last slash (important)
define('HELPER_SITE_URL', 'http://www.example.com');

// denied from uploading file extensions
define('HELPER_DENIED_EXTS', 'exe|com|msi|bat|cgi|pl|php|phps|phtml|php3|php4|php5|php6|py|pyc|pyo|pcgi|pcgi3|pcgi4|pcgi5|pchi6');

/** user-friendly urls - e.g. /helper/manage/1/2 vs /helper/index.php?r=manage/1/2
 * Enable ONLY when .htaccess has:

RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*) index.php

 * or nginx equivalent (try_location) so all requests are handled by index.php by default
 */
define('HELPER_USER_FRIENDLY_URLS', 1);

// interval after which the user is considered inactive
define('HELPER_ACTIVITY_INTERVAL', 60);