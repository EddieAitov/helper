<?php
// this is sample configuration for modules
// file should return an array with configuration
// rename file to config.modules.php and store in helper's folder
// config.modules.sample.php is not used anywhere
return [
	// global groups for menu
	'groups' => [
		[
			// class is used for linking with modules and tools
			'class' => 'modules',
			// name of group (locale)
			'name' => 'modules',
			// classes for icon
			'icon' => 'fa fa-list-alt fa-fw'
		],
		[
			// use this option to set the group available only for admin users
			'admin' => 1,
			'class' => 'tools',
			'name' => 'tools',
			'icon' => 'fa fa-cog fa-fw'
		],
		[
			'admin' => 1,
			'class' => 'locale',
			'name' => 'locale',
			'icon' => 'fa fa-font fa-fw'
		]
	],
	// main module list - standart itemlist/manage/remove
	'modules' => [
		[
			// module id. unique parameter. if not set will be set dynamically. careful with dynamic ids.
			'id' => 1,
			// module table
			'table' => 'helper_fast_answers',
			// module group (group.class)
			'group' => 'modules',
			// module name (locale)
			'name' => 'fast-answers',
			// module icon classes
			'icon' => '',
			// submodules
			'children' => [
				// field with type "id" is required for standart module. name of this field is overriten with "Id".
				['field' => 'answer_id', 'type' => 'id'],
				// standart name is taken from locale (or if there is no name in locale the key is outputted)
				['field' => 'text', 'name' => 'text'],
				// most of field types are shown in list by default (no need in show_in_list option)
				['field' => 'short_text', 'name' => 'short-text'],
				['field' => 'use_count', 'name' => 'use-count'],
				['field' => 'a', 'name' => 'a', 'type' => 'multi',
					// query is available for "multi" type. it defines table, id field and name field for linking with.
					'query' => ['table' => 'helper_operators', 'id' => 'operator_id', 'name' => 'login']
				],
				// field can be readonly
				['field' => 'b', 'type' => 'checkbox', 'name' => 'Test', 'readonly' => 1],
				[
					// composite modules are not shown in list by default - if it is possible to show em, use "show_in_list"
					'show_in_list' => 1,
					// id is very important for submodules to be set! do not leave it empty for submodules as it used in db linking
					'id' => 2,
					// class is component's type. image -> HComponentImage (model) & HNodeImage (view)
					'class' => 'image',
					// table for component (can be empty for e.g. password module)
					'table' => 'helper_images',
					// [images] subfolder in main files folder. do not forget to create this folder.
					'folder' => 'images',
					// name from locale
					'name' => 'images',
					// submodule can also have its submodules
					'children' => [
						// id field is required for modules with tables
						['field' => 'image_id', 'type' => 'id'],
						['field' => 'filename', 'name' => 'filename']
					],
					'sizes' => [
						// [image] image copies sizes. 3rd parameter is scaling mode (0 - inner/1 - outer/2 - crop)
						[50, 50, 0],
						[100, 100, 1],
						[200, 200, 2]
					],
					// [image] which size of image to show for preview in helper
					'preview_index' => 1
				]
			]
		],
		[
			// module is available only for admin users
			'admin' => 1,
			'table' => 'helper_operators',
			'group' => 'modules',
			'name' => 'operators',
			'icon' => '',
			'children' => [
				['field' => 'operator_id', 'type' => 'id'],
				['field' => 'login', 'name' => 'login'],
				// linked field. inner join <linked> using <field>. field "name" is used from linked table.
				['field' => 'group_id', 'name' => 'group', 'linked' => 'helper_groups'],
				['field' => 'email', 'name' => 'email'],
				[
					// password set/change module. no table is required.
					'class' => 'password',
					'name' => 'password'
				]
			]
		],
		[
			'table' => 'locale',
			'group' => 'locale',
			'name' => 'locale',
			// adding filter for missing language for field "value"
			'missing_lang' => 'value',
			'children' => [
				['field' => 'locale_id', 'type' => 'id'],
				// field is linked for localized table - setting linked_locale (so "name->ru" will be used, not "name")
				['field' => 'type_id', 'linked' => 'locale_types', 'linked_locale' => 1, 'name' => 'type'],
				['field' => 'key', 'name' => 'key'],
				['field' => 'value', 'name' => 'value', 'locale' => 1]
			]
		]
	],
	// 1 click tools. have only one method execute and can be executed by one click in menu. have no page (view).
	'tools' => [
		[
			// id is shared with modules
			'id' => 3,
			'group' => 'tools',
			'name' => 'locale-db-to-file',
			// class is tool's type. e.g. locale_db_to_file -> HToolLocale_db_to_file (model)
			'class' => 'locale_db_to_file'
		],
		[
			'id' => 4,
			'group' => 'tools',
			'name' => 'locale-file-to-db',
			'class' => 'locale_file_to_db',
			// tool can be disabled for security reasons and quickly returned back
			'disabled' => 1
		]
	]
];